<?php

/**
 * ProcessWire Init File
 *
 * This file is included during ProcessWire's boot initialization, immediately
 * after autoload modules have been loaded and had their init() methods called.
 * Anything you do in here will behave the same as an init() method on a module.
 * When this file is called, the current $page has not yet been determined.
 * This is an excellent place to attach hooks that don't need to know anything
 * about the current page.
 *
 */