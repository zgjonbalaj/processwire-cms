ProcessWire ProDrafts
=====================

This is a commercially licensed and supported module. Do not distribute. 
Copyright (C) 2016 by Ryan Cramer


ABOUT PRODRAFTS
===============

ProDrafts enables you to maintain separate draft and live versions of
any page. You can make edits to a draft version of the page without 
affecting the live version until you are ready to publish your draft.
ProDrafts provides a comparison tool so that you can compare draft
values to live values and view a "diff" for text changes. Furthermore,
you can preview both the draft and live versions on the front-end of
your site. 

ProDrafts provides automatic save capability, ensuring that your work 
is never lost, even if you step away from the computer or otherwise
forget to save. Edits are automatically saved to a draft of your page.

ProDrafts provides live preview capability, enabling you to edit a 
page while seeing it update on the front-end in real time, side-by-side.


BETA TEST NOTES
===============

This is an initial beta test version of ProDrafts. Expect potential bugs
and backup data before using this version. Please report any issues in
the ProDrafts board at processwire.com/talk/.


HOW TO INSTALL
==============

Please note: ProDrafts requires ProcessWire 2.7.0 or newer. 

1. Unzip the ProDrafts[version].zip file and place the files in:
   /site/modules/ProDrafts/
   
2. In the ProcessWire admin, go to Modules > Refresh. Then go to 
   Modules > Site and click "Install" next to ProDrafts. 
   
3. Next you will see the ProDrafts configuration screen. Configure
   ProDrafts and you are ready to go! 


DOCUMENTATION
=============
Please see our full online documentation at:
https://processwire.com/api/modules/prodrafts/
 

PRODRAFTS VIP SUPPORT
=====================

Your ProDrafts service includes 1-year of VIP support through the ProcessWire
ProDrafts forum at https://processwire.com/talk/. Support may optionally be 
renewed every year. 

Please send a private message (PM) to 'ryan' or contact us at:
https://processwire.com/contact/ 

VIP support is also available by email: ryan@processwire.com


TERMS AND CONDITIONS
====================

You may not copy or distribute ProDrafts, except on site(s) you have registered it
for with Ryan Cramer Design, LLC. It is okay to make copies for use on staging
or development servers specific to the site you registered for.

This service/software includes 1-year of support through the ProcessWire ProDrafts
Support forum and/or email. 

In no event shall Ryan Cramer Design, LLC or ProcessWire be liable for any special,
indirect, consequential, exemplary, or incidental damages whatsoever, including,
without limitation, damage for loss of business profits, business interruption,
loss of business information, loss of goodwill, or other pecuniary loss whether
based in contract, tort, negligence, strict liability, or otherwise, arising out of
the use or inability to use ProcessWire ProDrafts, even if Ryan Cramer Design, LLC /
ProcessWire has been advised of the possibility of such damages.

ProDrafts is provided "as-is" without warranty of any kind, either expressed or
implied, including, but not limited to, the implied warranties of merchantability and
fitness for a particular purpose. The entire risk as to the quality and performance
of the program is with you. Should the program prove defective, you assume the cost
of all necessary servicing, repair or correction. If within 7 days of purchase, you
may request a full refund. Should you run into any trouble with ProDrafts, please
email for support or visit the ProDrafts Support forum at:
https://processwire.com/talk/.


Thank you for using ProDrafts!
