function ProcessProDraftsEdit() {
	
}


jQuery(document).ready(function($) {
	var $diffs;

	// select or unselect all in main ProDrafts list
	$(document).on('click', 'input.select_all', function() {
		var checked = $(this).is(':checked');
		$(this).closest('table').find('input[type=checkbox]:not(.select_all)').each(function() {
			if(checked) {
				$(this).attr('checked', 'checked');
			} else {
				$(this).removeAttr('checked');
			}
		});
	});

	// diff comparison in changes action
	$diffs = $('.ProDrafts-diffs');
	if($diffs.length) $diffs.prettyTextDiff({});
});