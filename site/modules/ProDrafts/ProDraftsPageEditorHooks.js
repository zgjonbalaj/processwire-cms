/**
 * ProcessWire Pro Drafts: Page Editor Hooks javascript
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * https://processwire.com/ProDrafts/
 * 
 * @constructor
 * @param jQuery $
 *
 */

function ProDraftsPageEditorHooks($) {

	// ProDrafts configuration vars
	var cfg = typeof ProcessWire != "undefined" ? ProcessWire.config.ProDrafts : config.ProDrafts;
	
	// whether debug mode is enabled or not
	var debug = cfg.debug;
	
	// last event recorded by autosave's change event handler
	var autosaveLastEvent = { id: '', type: '' }
	
	// how often autosave should look for changes 
	var autosaveInterval = cfg.autosave == 1 ? cfg.autosaveInterval : (cfg.autosave * 1000);
	
	// milliseconds to wait between user interactions before beginning autosave
	var autosaveDelay = cfg.autosave == 1 ? cfg.autosaveDelay : 750;
	
	/**
	 * Get the text label for an Inputfield
	 *
	 * @param $inputfield
	 * @returns string
	 *
	 */
	function getInputfieldLabel($inputfield) {
		var label = $inputfield.children('.InputfieldHeader').text();
		return label;
	}

	/**
	 * Initialize an in page editor for ProDrafts
	 * 
	 * This primarily focuses on adding the ProDrafts icon to the Inputfield header
	 * 
	 * @param $inputfield having class "Inputfield"
	 * 
	 */
	function initInputfield($inputfield) {
		
		var fieldName = $inputfield.attr('id').replace('wrap_Inputfield_', '');
		var $header = $inputfield.children('.InputfieldHeader');
		var $icon, $link, $save, $pending;

		// if already initialize, don't re-initialize
		if($inputfield.hasClass('ProDraftsInit')) return;
		
		// indicate this field is initialized
		$inputfield.addClass('ProDraftsInit');
	
		// secondary initialization check
		if($header.find('.ProDraftsIcon').length) return;
		
		if($inputfield.hasClass('InputfieldNoProDraft')) {
			// inputfield that DOES NOT support ProDrafts
			$icon = jQuery("<i class='fa fa-warning fa-fw'></i>");
			$link = jQuery('<a />')
				.addClass('ProDraftsIcon')
				.attr('href', '#')
				.attr('title', cfg.labels.notSupported)
				.append($icon)
				.click(function() { return false; });

		} else {
			// inputfield that DOES support ProDrafts
			$icon = jQuery("<i class='ProDraftsIconNormal fa fa-fw fa-" + cfg.icon + "'></i>");
			$save = jQuery("<span class='ProDraftsIconSaving detail'></span>"); // + cfg.labels.saving + "</span>");
			$save.append("<i class='fa fa-fw fa-spin fa-spinner'></i>");
			$pending = jQuery("<i class='ProDraftsIconPending fa fa-fw fa-dot-circle-o'></i>");
		
			if(cfg.unpublished || $('body').hasClass('ProDraftsLivePreview')) {
				// ProDrafts icon WILL NOT be a compare link
				$link = jQuery('<span />');
			} else {
				// ProDrafts icon WILL be a compare link
				$link = jQuery('<a />').attr('href', cfg.urls.compare + fieldName)
					.attr('title', cfg.labels.hasChanges)
					.addClass('pw-modal pw-modal-large');
			}
			
			$link.addClass('ProDraftsIcon').append($icon).append($save).append($pending);
		}

		// add tooltip to ProDrafts icon
		$link.tooltip({
			position: {
				my: "center bottom", 
				at: "center top"
			}
		});

		$header.append($link);
	}

	/**
	 * Initialize the ProDrafts notice that appears at the top of the page editor
	 * 
	 * This primarily keeps track of whether the notice should be open or closed
	 * 
	 */
	function initNotice() {
		
		var timer = null;
	
		$('#ProDraftsNoticeCloser').click(function() {
			$.cookie('ProDraftsNoticeClosed', 1);
			if($('body').hasClass('AdminThemeReno')) {
				$('.ProDraftsNotice').slideUp('fast', function() {
					$('body').addClass('ProDraftsNoticeClosed');
					$('#ProDraftsNoticeOpener').fadeIn();
				});
			} else {
				$('body').addClass('ProDraftsNoticeClosed');
				$('.ProDraftsNotice').toggleClass('pwpd-notice-closed', 'fast');
			}
			return false;
		});
		
		function clickOpener() {
			if(timer) clearTimeout(timer);
			$.cookie('ProDraftsNoticeClosed', 0);
			$('body').removeClass('ProDraftsNoticeClosed');
			if($('body').hasClass('AdminThemeReno')) {
				$(this).hide();
				$('.ProDraftsNotice').hide();
				$('.ProDraftsNotice').slideDown('fast').fadeIn();
			} else {
				$('.ProDraftsNotice').toggleClass('pwpd-notice-closed', 'fast');
			}
			return false;
		}
		
		if($('body').hasClass('AdminThemeReno')) {
			if($('body').hasClass('modal')) {
				$('#ProDraftsNoticeOpener').click(clickOpener);
			} else {
				$('#ProDraftsNoticeOpener').mouseover(clickOpener);
			}
		} else {
			$(document).on('click', '.pwpd-notice-closed', clickOpener);
			$(document).on('mouseover', '.pwpd-notice-closed', function() {
				if(!$('body').hasClass('ProDraftsNoticeClosed')) return;
				timer = setTimeout(function(){
					clickOpener();
				}, 500);
			}).on('mouseleave', '.pwpd-notice-closed', function() {
				if(timer) clearTimeout(timer);
			});
		}
	}

	/**
	 * Update the ProDrafts notice that appaers at the top of the page editor
	 * 
	 * This updates the change counter that appears in the notice and adds a tooltip
	 * to it that shows the list of changed fields.
	 * 
	 */
	function updateNotice() {
		
		var $notice = $(".ProDraftsNotice");
		var $noticeText = $notice.find(".ProDraftsNoticeText");
		var $inputfields = $(".ProDraftsInit:not(.InputfieldNoProDraft)");
		var numChanges = $inputfields.length;
		var text;
		var changes = [];
		
		if(numChanges == 1) {
			text = cfg.labels.editingDraft;
		} else if(numChanges == 0) {
			text = cfg.labels.editingDraftNone;
		} else {
			text = cfg.labels.editingDraftPlural;
		}
		
		if(numChanges > 0) {
			$notice.removeClass('pwpd-no-changes').addClass('pwpd-changes');
		} else {
			$notice.addClass('pwpd-no-changes').removeClass('pwpd-changes');
		}
		
		text = text.replace('%d', numChanges);
		if(text != $noticeText.text()) {
			$noticeText.parent().fadeOut('fast', function() {
				$noticeText.text(text);
				$noticeText.parent().fadeIn('fast');
			});
		}
		
		if(numChanges > 0) {
			$inputfields.each(function() {
				changes.push(getInputfieldLabel($(this)));
			});
			$noticeText.attr('title', "• " + changes.join("\n• "));
			if(!$noticeText.hasClass('ProDraftsHasTooltip')) {
				$noticeText.tooltip({
					position: {
						my: "right bottom",
						at: "right top"
					}
				}).addClass('ProDraftsHasTooltip');
			}
		} else {
			$noticeText.attr('title', '');
		}
	}

	/**
	 * Event handler for when the "Save Draft" button is clicked
	 * 
	 * This produces a warning/confirm dialog for when user has made changes to a field
	 * that cannot be saved in a ProDraft. 
	 * 
	 * @returns {boolean}
	 * 
	 */
	function saveDraftClick() {
		var changes = [];
		var message, label;

		$(".InputfieldNoProDraft").each(function() {
			if($(this).hasClass('InputfieldStateChanged') || $(this).find('.InputfieldStateChanged').length) {
				label = $.trim($(this).children('.InputfieldHeader').text());
				if($.inArray(label, changes) == -1) changes.push(label);
			}
		});
		if(changes.length) {
			message = cfg.labels.saveDraftWarn + "\n\n• " + changes.join("\n• ");
			if(!confirm(message)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Initialize autosave
	 * 
	 */
	function initAutosave() {
	
		// timer used for autosave's setTimeout() calls
		var autosaveTimer = null;
		
		// is autosave temporarily disabled?
		var autosaveDisabled = false; 
		
		// selector for finding Inputfields that can be used with autosave
		var xselector = ':not(.InputfieldNoProDraft):not(.InputfieldNoAutosave)';
		
		// time of last user interaction, for use with autosaveDelay
		var lastInteractionTime = 0;

		/**
		 * Called when autosave has finished, updates the live preview 'site' window, if applicable
		 * 
		 * @param response
		 * @param $inputfields
		 * @param array fieldNames
		 * 
		 */
		function successAutosave(response, $inputfields, fieldNames) {
			var parent, $parent;
			
			updateNotice();
			
			if(cfg.livePreview > 0) {
				parent = window.parent;
				if(typeof parent == "undefined" || parent == null) return;
				$parent = parent.jQuery;
				if(typeof $parent == "undefined" || $parent == null) return;
				if($parent.find("#pwpd-site").length == 0) return;
				if(cfg.livePreview == 2) {
					parent.ProDraftsLivePreview.reloadSiteAll();
				} else {
					parent.ProDraftsLivePreview.reloadSite(fieldNames);
				}
			}
		}

		/**
		 * Called when autosave has failed
		 *
		 * @param string textStatus
		 * @param string message Message from ProcessWire, if available
		 * @param array fieldNames
		 *
		 */
		function failedAutosave(textStatus, message, fieldNames) {
			if(debug) alert('Autosave failed: ' + textStatus + ' ' + message + ' (' + fieldNames.join(', ') + ')');
		}

		/**
		 * Reset the autosave timer
		 * 
		 * @param ms Number of milliseconds
		 * 
		 */
		function resetAutosaveTimer(ms) {
			if(autosaveTimer !== null) clearTimeout(autosaveTimer);
			autosaveTimer = setTimeout(readyAutosave, ms);
		}

		/**
		 * Update the last interaction time
		 * 
		 */
		function updateInteractionTime() {
			var d = new Date();
			lastInteractionTime = d.getTime();
		}

		/**
		 * Execute the autosave
		 * 
		 * @param array inputfieldIDs ID attributes of Inputfields to be autosaved
		 * 
		 */
		function executeAutosave(inputfieldIDs) {
		
			// the form.InputfieldForm 
			var $form = null;
		
			// array of selectors to use in finding the data that will be sent in the autosave POST request
			var selectors = [ '._post_token', '#Inputfield_id' ];
		
			// array of field names included in the autosave
			var fieldNames = [];
			
			// array of .Inputfield elements included in the autosave
			var inputfields = [];
		
			// string of serialized form data that will be sent in the POST request
			var formData = '';
		
			// additional runtime use vars
			var n, fieldName, $inputfield, inputfieldID, formDataMD5, request, $inputs, test;
		
			// determine and load the inputfields from the inputfieldIDs
			for(n = 0; n < inputfieldIDs.length; n++) {
				inputfieldID = '#' + inputfieldIDs[n];
				$inputfield = $(inputfieldID);
				if(!$inputfield.length) continue;
				if($form === null) $form = $inputfield.closest('form');
				initInputfield($inputfield);
				selectors.push(inputfieldID + ' :input');
				fieldName =	$inputfield.attr('id').replace('wrap_Inputfield_', '');
				fieldNames.push(fieldName);
				inputfields.push($inputfield);
			}

			if($form === null) return;

			// locate all of the <input> elements that will be sent in the POST
			$inputs = $form.find(selectors.join(', '));
			
			// populate the formData that will be sent in the POST request
			formData = 'submit_save_draft=autosave&' + $inputs.serialize();
		
			// make sure that unchecked checkbox and radios are included as blank field names
			$inputs.filter('input[type=checkbox], input[type=radio]').each(function() {
				var $input = $(this);
				if($input.is(":checked")) return;
				var name = $input.attr('name');
				if(name.indexOf('[') > -1) {
					name = name.replace('[', '%5B').replace(']', '%5D');
				}	
				var item = '&' + name + '=';
				if(formData.indexOf(item) > -1) return;
				formData += '&' + name + '='; // blank value
			});
		
			// get a hash of the formData for later comparison
			formDataMD5 = md5(formData);
			
			// get the previous hash of data that was sent
			test = $inputs.first().attr('data-pwpdmd5');
		
			// test if an identical request has already been sent
			if(typeof test != "undefined" && test == formDataMD5) {
				// abort, because this request is identical to one we already submitted
				// this can occur when a request is triggered by a 'keyup' event, and a 
				// follow-up 'change' event occurs when the field is blurred
				setTimeout(function() {
					for(n = 0; n < inputfields.length; n++) {
						$inputfield = inputfields[n];
						// remove classes indicating the inputfield has changed
						$inputfield.removeClass('InputfieldStateChanged ProDraftSaving')
							.find('.InputfieldStateChanged').removeClass('InputfieldStateChanged');
					}
				}, 100);
				if(debug) console.log("Aborting POST because identical to previous");
				return;
			}
	
			// if we get to this point, we have a unique request that will be POST'ed
			// populate our postData hash to the inputs 
			$inputs.attr('data-pwpdmd5', formDataMD5);
		
			// account for itemList fields (files, images, etc.) that need their field name as part of the post data
			// this is to accommodate the current ProcessPageEdit::ajaxSave implementation which may not use $_GET[fields]
			for(n = 0; n < fieldNames.length; n++) {
				fieldName = '&' + fieldNames[n];
				if(formData.indexOf(fieldName + '=') == -1 && formData.indexOf(fieldName + '%5B') == -1) {
					formData += fieldName + '=1';
				}
			}

			// build our ajax POST request
			request = {
				type: 'POST',
				url: $form.attr('action') + '&fields=' + fieldNames.join(','),
				data: formData,
				dataType: 'json',
				success: function(response, textStatus, jqXHR) {
					var $inputfields = $form.find('#' + inputfieldIDs.join(', #'));
					
					$inputfields.each(function() {
						var $inputfield = $(this);
						$inputfield.removeClass('ProDraftSaving');
						// if(response.error == false) $inputfield.removeClass('InputfieldStateChanged');
						// Inputfield in Inputfield (textareas, multiplier, etc.)
						// $inputfield.find('.InputfieldStateChanged').removeClass('InputfieldStateChanged');
					});

					if(response.error) {
						// error occurred
						failedAutosave(textStatus, response.message, fieldNames);
					} else {
						// successful autosave
						successAutosave(response, $inputfields, fieldNames);
					}
					
					if(debug) console.log(response);
				},
				error: function(jqXHR, textStatus) {
					failedAutosave(textStatus, '', fieldNames);
				}
			}

			if(debug) console.log(request);
			
			// send the ajax request
			jQuery.ajax(request);
		}

		/**
		 * Called every second or so to determine if there are changes and autosave should proceed
		 * 
		 */
		function readyAutosave() {
		
			// id attributes of Inputfields that need to be autosaved
			var saveInputfieldIDs = [];
		
			// current interaction time
			var d = new Date();
			var t = d.getTime();
			
			// don't let autosave proceed if user has done some interaction within the last second
			if(t - lastInteractionTime < autosaveDelay) {
				resetAutosaveTimer(autosaveDelay);	
				return;
			}
		
			// attach change event to any CKE inline editors that have shown up in the document
			// this is necessary because inline editors don't trigger change events on textareas
			$('.InputfieldCKEditorInline.InputfieldCKEditorLoaded:not(.ProDraftsCKE)').each(function() {
				var $cke = $(this);
				var editor = CKEDITOR.instances[$cke.attr('id')];
				editor.on('change', inputChangeEvent);
				$cke.addClass('ProDraftsCKE');
			});

			// find changed Inputfields if a save is not already occurring
			if(!autosaveDisabled && $('.ProDraftSaving').length == 0) {
			
				// changed Inputfields that are allowed for autosave
				$('.InputfieldStateChanged' + xselector).each(function() {
					
					var $inputfield = $(this);
					var $parentInputfield; // i.e. like .Inputfield in a Textareas or Multiplier
				
					// if Inputfield has a parent with no drafts or autosave, don't continue
					if($inputfield.closest('.InputfieldNoProDraft').length > 0) return;
					if($inputfield.closest('.InputfieldNoAutosave').length > 0) return;
				
					// find the furthest parent inputfield, for situations like InputfieldTextareas or InputfieldMultiplier
					$parentInputfield = $inputfield.parents('.Inputfield:not(.InputfieldWrapper)').last();
					
					if($parentInputfield.length) {
						if(debug) console.log("Substituting " + $parentInputfield.attr('id') + ' for ' + $inputfield.attr('id'));
						$inputfield.trigger('saveReady');
						$inputfield = $parentInputfield;
						$inputfield.find('.InputfieldStateChanged').removeClass('InputfieldStateChanged');
					}
				
					// mark the field as being saved and trigger 'saveReady' event for Inputfields that look for ie (i.e. CKEditor)
					$inputfield.addClass('ProDraftSaving').trigger('saveReady').removeClass('InputfieldStateChanged');
					saveInputfieldIDs.push($inputfield.attr('id'));
					autosaveDisabled = true;
					
					// ensure that CKEditor is triggering change events for things like bold > unbold > bold
					// by resetting the 'dirty' state from the last change rather than from when the editor loaded
					if($inputfield.hasClass('InputfieldCKEditor')) {
						$inputfield.find('.InputfieldCKEditorLoaded').each(function() {
							var editor = CKEDITOR.instances[$(this).attr('id')];
							editor.resetDirty();
						});
					}
				});
			}
		
			// execute the autosave after a short delay, giving time for any 
			// potential 'saveReady' events to complete their updates
			setTimeout(function() {
				if(saveInputfieldIDs.length) executeAutosave(saveInputfieldIDs);
				autosaveDisabled = false;
				resetAutosaveTimer(autosaveInterval);
			}, 250);
		}

		/**
		 * Handles 'change' and 'keyup' events to determine when autosave should queue
		 * 
		 * @param e
		 * 
		 */
		function inputChangeEvent(e) {
			
			var $input = $(this);
			if($input.closest('.InputfieldNoProDraft, .InputfieldNoAutosave').length) return;
			
			var $inputfield = $input.closest('.Inputfield');
			var code = (e.keyCode || e.which);
			
			updateInteractionTime();
			
			if(typeof code != "undefined") {
				// keyup event
				if(code == 37 || code == 38 || code == 39 || code == 40) {
					// exclude arrow keys
					return;
				}
				autosaveLastEvent.id = $input.prop('id');
				autosaveLastEvent.type = 'keyup';
				
			} else if(autosaveLastEvent.type == 'keyup' && autosaveLastEvent.id == $input.prop('id')) {
				// skip the change event because it was already recorded as a keyup event
				autosaveLastEvent.type = 'change';	
				setTimeout(function() {
					$inputfield.removeClass('InputfieldStateChanged');
				}, 100);
				return false;
				
			} else {
				// regular change event
				autosaveLastEvent.type = 'change';
				autosaveLastEvent.id = $input.prop('id');
			}

			$inputfield.addClass('InputfieldStateChanged');
			
			if(debug) {
				var name = $input.attr('name');
				if(typeof name == "undefined") name = getInputfieldLabel($inputfield);
				console.log('change: ' + $input.attr('name'));
			}
		}

		/**
		 * Main autosave initialization
		 * 
		 */
		updateInteractionTime();
	
		// assign event handlers to monitor change and upload events
		$(document).on('change', ':input', inputChangeEvent);
		$(document).on('AjaxUploadDone', '.InputfieldFileList', inputChangeEvent);
		
		// monitor keyup events only if aggressive autosave mode is active
		if(cfg.autosave == 1) $(document).on('keyup', 'input, textarea', inputChangeEvent);

		// start the autosave timer
		resetAutosaveTimer(autosaveInterval);
	}

	/**
	 * Initialize links used in admin part of live preview
	 * 
	 */
	function initLivePreview() {
		
		$('a.pwpd-exit').click(function() {
			/*
			// PW's inputfields.js already catches this, so commented out unless/until we find a situation where it doesn't
			var $inputfields = $('.InputfieldStateChanged'); 
			var exitNow = true;
			var message = cfg.labels.confirmExit + "\n";
			if($inputfields.length) {
				$inputfields.each(function() {
					message += "\n• " + getInputfieldLabel($(this));
				});
				if(!confirm(message)) exitNow = false;	
			}
			*/
			parent.window.location = $(this).attr('data-url');
			return false;
		});
		
		$('a.pwpd-refresh').click(function() {
			parent.ProDraftsLivePreview.reloadSiteAll();	
			return false;
		});
	}

	/**
	 * Initialize versions support (for future use when versions support active)
	 * 
	 */
	function initVersions() {
		var $compareButton = $("#ProDraftsCompareButton");

		$(".ProDraftsCompare1, .ProDraftsCompare2").on('change', function() {
			var $compare1 = $(".ProDraftsCompare1:checked");
			var $compare2 = $(".ProDraftsCompare2:checked");
			var href = $compareButton.attr('data-basehref') + $compare1.val() + '.' + $compare2.val();
			$compareButton.attr('data-href', href);
		});
		
		$(".ProDraftsCompare1:eq(0)").change();
	}
	
	function initConfirmPublish() {
		$('#submit_save').click(function() {
			if($(this).attr('id') != 'submit_save') return true;
			if(confirm(cfg.labels.confirmPublish)) {
				return true;
			} else {
				return false;
			}
		});
	}

	/**
	 * Initialize
	 * 
	 */
	function init() {
		
		$(".InputfieldHasProDraft, .InputfieldNoProDraft").each(function() {
			initInputfield($(this));
		});

		$(document).on('reloaded', '.InputfieldHasProDraft, .InputfieldNoProDraft', function() {
			initInputfield($(this));
		});

		$("#submit_save_draft").click(saveDraftClick);
		if($('body').hasClass('AdminThemeReno')) {
			if($('body').hasClass('modal')) {
				$("#content .ProDraftsNotice").css('margin-bottom', '1em').prependTo($("#ProcessPageEdit")); 
				$("#ProDraftsNoticeOpener").css({
					position: 'relative',
					zIndex: 100
				});
			} else {
				$("#content .ProDraftsNotice").appendTo($("#breadcrumbs")); 
			}
		}
		if($(".ProDraftsNoticeText").text().length == 0) updateNotice();
		
		if(cfg.autosave > 0) initAutosave();
		if($('body').hasClass('ProDraftsLivePreview')) initLivePreview();
		initNotice();
		// initVersions();
		if(cfg.confirmPublish > 0) initConfirmPublish();
	}
	
	init();
}

/**
 * Spinner that appears in top left corner of ProDraftsNotice
 * 
 * As used from ProcessProDraftsEdit.js admin window calls
 * 
 * @type {{start: Function, stop: Function}}
 * 
 */
var ProDraftsSpinner = {
	start: function() {
		var $icon = $('.ProDraftsNotice i').first();
		$icon.attr('data-class', $icon.attr('class'));
		$icon.attr('class', 'fa fa-fw fa-spin fa-spinner');
	},

	stop: function() {
		var $icon = $('.ProDraftsNotice i').first();
		$icon.attr('class', $icon.attr('data-class'));
		$icon.removeAttr('data-class');
	}
}

/**
 * jQuery document.ready
 * 
 */
jQuery(document).ready(function($) {
	ProDraftsPageEditorHooks($);
});