<?php

/**
 * ProcessWire Pro Drafts: Hooks
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 *
 */

class ProDraftsHooks extends Wire {

	/**
	 * @var ProDrafts
	 * 
	 */
	protected $drafts;

	/**
	 * @var ProDraftsPageEditorHooks
	 *
	 */
	protected $pageEditorHooks = null;

	/**
	 * @var ProDraftsFrontEndHooks
	 *
	 */
	protected $frontEndHooks = null;

	/**
	 * Construct
	 * 
	 * @param ProDrafts $drafts
	 * 
	 */
	public function __construct(ProDrafts $drafts) {
		$this->drafts = $drafts;
	}

	/**
	 * Initialize hooks
	 * 
	 */
	public function init() {
		
		$this->addHook('Page::draft', $this, 'hookPageDraft');
		$this->addHook('Page::isDraft', $this, 'hookPageIsDraft');
		$this->addHook('Page::hasDraft', $this, 'hookPageHasDraft');
		$this->addHook('Page::draftPage', $this, 'hookPageDraftPage');
		$this->addHook('Page::livePage', $this, 'hookPageLivePage');
		$this->addHook('Page::draftChanges', $this, 'hookPageDraftChanges');
		$this->addHook('Page::saveDraft', $this, 'hookPageSaveDraft');
		$this->addHook('Page::publishDraft', $this, 'hookPagePublishDraft');
		$this->addHook('Page::deleteDraft', $this, 'hookPageDeleteDraft');
		$this->addHookBefore('Pages::save', $this, 'hookBeforePageSave');
		$this->addHookBefore('Pages::saveField', $this, 'hookBeforePageSaveField');
		$this->addHook('Pages::saved', $this, 'hookPageSaved');
		$this->addHook('Pages::savedField', $this, 'hookPageFieldSaved');
		$this->addHook('Pages::deleted', $this, 'hookPageDeleted');
		$this->addHookBefore('Fieldtype::savePageField', $this, 'hookFieldtypeSavePageField');
		$this->addHookAfter('Fieldtype::sleepValue', $this, 'hookFieldtypeSleepValue');
		$this->addHook('Fields::saved', $this, 'hookFieldSaved');
	
		if($this->drafts->maxVersions > 0) {
			$this->addHook('Page::version', $this, 'hookPageVersion');
			$this->addHook('Page::versions', $this, 'hookPageVersions');
		}
		
		if(!empty($_GET['draft'])) {
			require_once(dirname(__FILE__) . '/ProDraftsFrontEndHooks.php');
			$this->frontEndHooks = new ProDraftsFrontEndHooks($this->drafts);
		}
	}

	/**
	 * API ready
	 * 
	 */
	public function ready() {
		
		$user = $this->wire('user');
		if(!$user->isLoggedin()) return;	
		
		$superuser = $user->isSuperuser();
		$page = $this->wire('page');
		$admin = $page->template == 'admin';	
		
		if($admin) {
			require_once(dirname(__FILE__) . '/ProDraftsPageEditorHooks.php');
			$this->pageEditorHooks = new ProDraftsPageEditorHooks($this->drafts, $page);
		}

		if($admin || (($superuser || $page->editable()) && $this->drafts->hasDraft($page))) {
			$this->addHookAfter('PagefilesManager::path', $this, 'hookPagefilesManagerPath');
			$this->addHookAfter('PagefilesManager::url', $this, 'hookPagefilesManagerUrl');
		}
		
	}

	/**
	 * Get cache of ProDraft objects for $page indexed by version number
	 * 
	 * @param Page $page
	 * @param int|null $version Specify a version number if you only want to retrieve the ProDraft for that version
	 * @return array|ProDraft|null Array of ProDraft objects indexed by version number, or null|ProDraft if version number specified.
	 * 
	 */
	public function getPageDraftsCache(Page $page, $version = null) {
		$a = $page->get('_ProDrafts');
		if(!is_array($a)) $a = array();
		$b = array();
		foreach($a as $draft) {
			$b[$draft->version] = $draft;		
		}
		if(!is_null($version)) {
			return isset($b[$version]) ? $b[$version] : null;
		}
		return $b;	
	}

	/**
	 * Add ProDraft to the cache for $page
	 * 
	 * @param Page $page
	 * @param ProDraft $draft
	 * 
	 */
	public function addToPageDraftsCache(Page $page, ProDraft $draft) {
		$a = $this->getPageDraftsCache($page);
		$a[$draft->version] = $draft;
		$page->setQuietly('_ProDrafts', $a);
	}

	/**
	 * Remove ProDraft from the cache for $page
	 * 
	 * @param Page $page
	 * @param ProDraft $draft
	 * 
	 */
	public function removeFromPageDraftsCache(Page $page, ProDraft $draft) {
		$a = $this->getPageDraftsCache($page);
		unset($a[$draft->version]); 
		$page->setQuietly('_ProDrafts', $a);
	}

	/*********************************************************************************************
	 * DRAFTS IMPLEMENTATION HOOKS
	 *
	 */

	/**
	 * Hook after Page::editable when logged-in, page-publish installed, non-superuser front-end page and $_GET[draft] == 1
	 * 
	 * @param HookEvent $event
	 * 
	public function hookPageEditable(HookEvent $event) {
		if($event->return) return;
		$page = $this->wire('page');
		$draft = $page->isDraft();
		if($draft && $this->wire('user')->hasPermission('page-edit', $page)) {
			// enable draft to be viewed if user has standard page-edit permission to it
			$event->return = true;
		}
	}
	 */

	/**
	 * Adds a Page::isDraft() method which returns a ProDraft if the page is a populated draft or version
	 *
	 * Returns instance of ProDraft if page is a draft or version, or boolean false if it's not.
	 * Alternatively, specify an argument of false if not a draft, or ProDraft if it is a draft, and the
	 * value will be set. 
	 * 
	 * USAGE
	 * =====
	 * $draft = $page->isDraft(); // returns ProDraft object if draft/version or boolean false if not
	 * $page->isDraft($draft); // Provide a ProDraft object to set the current value...
	 * $page->isDraft(false); // ...or specify boolean false to unset it 
	 *
	 * @param HookEvent $e
	 * @throws WireException if given invalid argument
	 *
	 */
	public function hookPageIsDraft(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		if(!$this->drafts->allowDraft($page)) {
			$e->return = false;
			return;
		}
		$arg = $e->arguments(0);
		if($arg !== null) {
			if($arg === false) {
				$page->__unset('_isDraft');
			} else if($arg instanceof ProDraft) {
				$draft = $arg;
				$page->set('_isDraft', $draft);
				$this->addToPageDraftsCache($page, $draft);
			} else {
				throw new WireException("Page::isDraft(\$arg) called with unrecognized \$arg (must be false or ProDraft)");
			}
			$e->return = $arg;
			return;
		}	
		$draft = $page->get('_isDraft');
		if($draft && $draft->exists()) {
			$e->return = $draft;
		} else {
			$e->return = false;
		}
	}

	/**
	 * Hook for Page::hasDraft()
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookPageHasDraft(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		$version = $e->arguments(0);
		if(!$version) $version = 0;
		$e->return = $this->drafts->hasDraft($page, $version);
	}
	
	/**
	 * Hook for Page::draftPage()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPageDraftPage(HookEvent $e) {
		
		/** @var Page $page */
		$page = $e->object;
		$draft = $page->get('_isDraft');
		
		if($draft) {
			// $page is already the $draftPage
			$e->return = $page;
			return;
			
		} else if(!$this->drafts->allowDraft($page)) {
			$draft = null;
			
		} else {
			$this->hookPageDraft($e);
			$draft = $e->return;
		}
		
		if(!$draft) {
			$e->return = new NullPage();
			return;
		}
		
		$e->return = $draft->draftPage();	
	}

	/**
	 * Hook for Page::livePage()
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookPageLivePage(HookEvent $e) {
		
		/** @var Page $page */
		$page = $e->object;
		$draft = $page->get('_isDraft');

		if($draft) {
			// $page is a $draftPage
			$e->return = $draft->livePage();	
			return;

		} else {
			// $page is already the live page
			$e->return = $page;
		} 
	}

	/**
	 * Hook for Page::draftChanges()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPageDraftChanges(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		if(!$this->drafts->allowDraft($page)) {
			$draft = null;
		} else {
			$this->hookPageDraft($e);
			$draft = $e->return;
		}
		if(!$draft) {
			$e->return = array();
			return;
		}
		$e->return = $draft->changes();
	}

	/**
	 * Hook for Page::saveDraft()
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookPageSaveDraft(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object; 
		$e->return = $this->drafts->saveDraftPage($page, $e->arguments(0));
	}

	/**
	 * Hook for Page::publishDraft()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPagePublishDraft(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		$e->return = $this->drafts->publishDraft($page, $e->arguments(0));
	}

	/**
	 * Hook for Page::deleteDraft()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPageDeleteDraft(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		$e->return = $this->drafts->deleteDraft($page, $e->arguments(0));
	}

	/**
	 * Add a new Page::draft() method
	 *
	 * USAGE
	 * =====
	 * $page->draft(); returns ProDraft object for $page. 
	 * $page->draft(true); to create a new draft of the page in the database, if one doesn't already exist. Returns ProDraft.
	 * $page->draft(123); to retrieve a past version draft (where 123 is version); null returned if version does not exist.
	 *
	 * @param HookEvent $event
	 *
	 *
	 */
	public function hookPageDraft(HookEvent $event) {
		/** @var Page $page */
		$page = $event->object;
		$arg0 = $event->arguments(0);
		$arg1 = $event->arguments(1);
		$create = is_bool($arg0) ? $arg0 : false; // i.e. $page->draft(true); saves new draft if it doesn't already exist
		$version = is_int($arg0) ? $arg0 : 0;
	
		$draft = $this->getPageDraftsCache($page, $version);
		
		if(!$draft) {
			if($version && !$this->drafts->hasDraft($page, $version)) {
				// return null when requested version does not exist
				$event->return = null;
				return;
			}
			$draft = new ProDraft($this->drafts, $page);
			if($version) $draft->set('version', $version);
			$draft = $this->drafts->populateDraft($draft, $create);
		} else if($create) {
			$this->drafts->populateDraft($draft, true);
		}
		$event->return = $draft;
		$this->addToPageDraftsCache($page, $draft);
		if(is_bool($arg0)) return;
		if($arg0 && !is_int($arg0)) {
			if($arg1 !== null) {
				$draft->set($arg0, $arg1);
			} else {
				$event->return = $draft->get($arg0);
				return;
			}
		}
	}
	
	/**
	 * Add a new Page::version() method
	 *
	 * USAGE
	 * =====
	 * $page->version(123456); to retrieve ProDraft version object, where 123456 is version number.
	 *
	 * @param HookEvent $event
	 * @throws WireException for invalid arguments
	 *
	 */
	public function hookPageVersion(HookEvent $event) {
		/** @var Page $page */
		$page = $event->object;
		$version = $event->arguments(0);
		if(!is_int($version)) throw new WireException("No version supplied");
		$event->return = $this->hookPageDraft($event);
	}

	/**
	 * Get all versions for the page
	 * 
	 * USAGE
	 * =====
	 * $page->versions(); returns an array of ProDraft version objects, newest to oldest
	 * 
	 * @param HookEvent $event
	 * 
	 */
	public function hookPageVersions(HookEvent $event) {
		/** @var Page $page */
		$page = $event->object; 
		$options = $event->arguments(0);
		if(!is_array($options)) $options = array();
		$event->return = $this->drafts->getVersions($page, $options);
	}

	/**
	 * Hook to Fields::saved
	 *
	 * Clear the disallowFields cache
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookFieldSaved(HookEvent $e) {
		$this->wire('cache')->delete(ProDrafts::disallowFieldsCacheName);
		$this->drafts->debugMessageLog("Cleared ProDrafts disallowed fields cache", __FUNCTION__);
	}

	/**
	 * Hook to PagefilesManager::path()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPagefilesManagerPath(HookEvent $e) {

		/** @var Page $page */
		$page = $e->object->page;
		$draft = $page->isDraft();
		if(!$draft) return;

		$path = $e->return;
		$draftPath = $path . ProDrafts::tableName . '-' . $draft->name . '/';
		// $this->debugMessageVerbose("draftPath: $draftPath", __FUNCTION__); 

		if(!is_dir($draftPath)) {
			// copy files from live path to draft path
			$files = array();
			$this->drafts->debugMessage("Creating: $draftPath", __FUNCTION__);
			wireMkdir($draftPath);
			$dir = opendir($path);
			while(false !== ($file = readdir($dir))) {
				if($file == '.' || $file == '..') continue;
				$liveFile = $path . $file;
				$draftFile = $draftPath . $file;
				if(is_dir($liveFile)) continue; // skip over directories
				copy($liveFile, $draftFile);
				wireChmod($draftFile);
				$mtime = filemtime($liveFile);
				$atime = fileatime($liveFile);
				touch($draftFile, $mtime, $atime);
				$files[] = $file;
			}
			closedir($dir);
			// record all files that were originally copied
			file_put_contents($draftPath . '.prodrafts-files.json', json_encode($files));
		}

		$e->return = $draftPath;
	}

	/**
	 * Hook to PagefilesManager::url()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPagefilesManagerUrl(HookEvent $e) {
		$page = $e->object->page;
		$draft = $page->isDraft();
		if(!$draft) return;
		$draftUrl = $e->return . ProDrafts::tableName . '-' . $draft->name . '/';
		$e->return = $draftUrl;
	}

	/**
	 * Hook before Pages::save
	 *
	 * Account for changes to native properties. Here we restore the original values to those native properties,
	 * unless the draft is being published.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookBeforePageSave(HookEvent $e) {

		$page = $e->arguments(0);
		$draft = $page->isDraft();
		if(!$draft) return;
		if($draft->publish || $draft->publishField) return;

		// ensure that modified time and user are not updated during draft saves
		$options = $e->arguments(1);
		$options['quiet'] = true; 
		$e->arguments(1, $options);
		
		$natives = $this->drafts->getNativePageProperties();
		
		$query = $this->wire('database')->prepare("SELECT * FROM pages WHERE id=:id");
		$query->bindValue(':id', $page->id);
		$query->execute();
		
		$row = $query->fetch(PDO::FETCH_ASSOC);

		foreach($row as $name => $value) {
			if(!in_array($name, $natives)) continue;
			if(!$this->drafts->isAllowedField($name)) continue;
			if($name == 'created' || $name == 'modified') $value = strtotime($value);
			$pageValue = $page->get($name);
			if($value != $pageValue || $draft->version > 0) {
				// native property changed in draft,
				// save changed value in draft and restore live value
				$this->drafts->debugMessage("Value for native property '$name' changed from $value => $pageValue", __FUNCTION__);
				$draft->setContent($name, $pageValue, __FUNCTION__);
				$page->setQuietly($name, $value);
			}
		}
		
		$query->closeCursor();
	}

	/**
	 * Hook before Pages::saveField
	 *
	 * Ensure that 'quiet' mode is used when page is a draft
	 *
	 * @param HookEvent $event
	 *
	 */
	public function hookBeforePageSaveField(HookEvent $event) {
		
		$page = $event->arguments(0);
		$options = $event->arguments(2);

		$draft = $page->isDraft();
		if(!$draft) return;
		if($draft->publish || $draft->publishField) return;
		
		$options['quiet'] = true;
		$event->arguments(2, $options);
	}

	/**
	 * Hook to Pages::saved() (also used by hookFieldSaved method in this class)
	 *
	 * Commits draft data to DB.
	 *
	 * @param HookEvent $e
	 * @param null $changes
	 *
	 */
	public function hookPageSaved(HookEvent $e, $changes = null) {

		/** @var Page $page */
		$page = $e->arguments(0);
		/** @var ProDraft $draft */
		$draft = $page->draft();

		if(!$draft->exists()) return;

		if($draft->publish) {
			// if publish of draft was requested, cleanup now and exit
			$this->finishPublishPage($page);
			$draft->set('publish', false);
			return;
		}

		$this->drafts->saveDraft($draft);
	}

	/**
	 * Hook to Pages::hookFieldSaved()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPageFieldSaved(HookEvent $e) {
		$field = $e->arguments(1);
		$this->hookPageSaved($e, array($field->name));
	}

	/**
	 * Complete publish of a draft and cleanup (companion to hookPageSaved method)
	 *
	 * This method performs the cleanup after a draft is published, and the Pages::save() that initiated it
	 * is when it is actually published.
	 *
	 * @param Page $page
	 *
	 */
	protected function finishPublishPage(Page $page) {

		$draft = $page->draft();
		if(!$draft->exists()) return;
		$name = $draft->name;

		$this->drafts->debugMessage("Finishing publish of: $page", __FUNCTION__);

		// draft will publish on it's own when the page saves, but we need to drop our draft content
		// @todo check if we can retain this data temporarily until Pages::saved() to confirm no errors
		$tableName = ProDrafts::tableName;
		$sql = "DELETE from $tableName WHERE pages_id=:pages_id AND version=:version";
		$query = $this->wire('database')->prepare($sql);
		$query->bindValue(':pages_id', $page->id, PDO::PARAM_INT);
		$query->bindValue(':version', 0, PDO::PARAM_INT);
		$query->execute();

		// move draft files to live files: first get the draft files path
		// temporarily set as non-draft to get live path
		$draftFilesPath = $page->filesManager->path();
		$draft->name = '';
		$liveFilesPath = $page->filesManager->path();		// get live files path
		$jsonFile = $draftFilesPath . '.prodrafts-files.json';
		if(is_file($jsonFile)) {
			// remove files that were originally present in live files path when draft was created
			$files = json_decode(file_get_contents($jsonFile), true);
			foreach($files as $file) {
				$file = $liveFilesPath . basename($file);
				if(!is_file($file)) continue;
				unlink($file);
			}
			unlink($jsonFile);
		} else {
			$page->filesManager->emptyPath(false, false);        // clear out live files	
		}
		$draft->name = $name;
		$page->filesManager->moveFiles($liveFilesPath); 	// move draft files to live

		// remove draft files directory
		wireRmdir($draftFilesPath, true);

		$this->removeFromPageDraftsCache($page, $draft);
		$page->isDraft(false);
		$this->drafts->setPageDraftStatus($page, false);
	}

	/**
	 * Hook to Fieldtype::savePageField
	 *
	 * When page is a draft, this hook replaces Fieldtype::savePageField. It collects the changed
	 * data into the draft and prevents the Fieldtype's savePageField from being called. The Pages::saved()
	 * hook then commits that draft content to the pages_drafts table.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookFieldtypeSavePageField(HookEvent $e) {

		/** @var Page $page */
		$page = $e->arguments(0);
		if(!$this->drafts->allowDraft($page)) return;
		$isDraft = $page->isDraft();
		
		// the _wasDraft is set by ProDraftsPageEditorHooks::hookEditLoadPage
		// and it indicates if the page was a draft before the submit_save_draft button was clicked
		// if wasDraft is null (unset) then ProcessPageEdit was not used to edit the page
		$wasDraft = $page->get('_wasDraft');
	
		// if not a draft and wasDraft not present, then ProcessPageEdit is not being used here
		if(!$isDraft && is_null($wasDraft)) return;
		
		/** @var Field $field */
		$field = $e->arguments(1);
		$e->return = true; 
		
		if(!$isDraft && $wasDraft && !$this->drafts->isAllowedField($field->name)) {
			// don't allow non-draftable fields to be saved during draft publish in ProcessPageEdit
			$e->replace = true;
			return;
		}

		// if not a draft, allow savePageField to continue with its original plan
		if(!$isDraft) return;
			
		/** @var ProDraft $draft */
		$draft = $page->draft();

		if(!$draft->exists()) {
			$this->drafts->debugMessageVerbose('Draft not active', __FUNCTION__);
			return;
		}

		// if draft is being published, no need to continue with saving draft content
		if($draft->publish) {
			$this->drafts->debugMessageVerbose('Draft is being published', __FUNCTION__);
			if(!$this->drafts->isAllowedField($field->name)) {
				// don't allow non-draftable fields to be saved during draft publish
				$e->replace = true;
			}
			return;
		}
		
		// if 1 field in draft is being published, no need to continue with saving draft content
		if($draft->publishField) {
			$this->drafts->debugMessageVerbose("Draft field '$draft->publishField' is being published", __FUNCTION__);
			return;
		}
		
		// if schema isn't responsible for storing all data for this fieldtype, it can't be stored in a draft
		if(!$this->drafts->isAllowedField($field->name)) {
			if($isDraft && $wasDraft === true) {
				// don't allow to be saved
				$e->replace = true;
			} else {
				// allow it to be saved for immediate publish
				return;
			}
		}

		// if no change is indicated for this field, no need to continue
		if($draft->version == 0 && !$page->isChanged($field->name) && !in_array($field->name, $draft->changes)) {
			$this->drafts->debugMessageVerbose("No change to: $field->name", __FUNCTION__);
			return;
		}
		
		// if we reach this point, we will be taking over the savePageField
		// prevent actual savePageField from being called
		$e->replace = true;

		/* @todo for repeater suppport
		if(strpos($field->type->className(), 'Repeater') !== false) {
			// let savePageField run
		   $e->replace = false;
		}
		*/
		
		$this->drafts->debugMessage("Bypassing $field->type::sleepValue() to save $field->name content in draft");

		// Populate draft with the ready-to-save draft value
		$value = $page->getUnformatted($field->name);
		$value = $field->type->sleepValue($page, $field, $value);
		$draft->setContent($field->name, $value, __FUNCTION__);

		$e->return = true;
	}

	/**
	 * Hook to Fieldtype::sleepValue()
	 *
	 * Update value for a field to make it ready for publish
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookFieldtypeSleepValue(HookEvent $e) {

		$page = $e->arguments(0);
		$field = $e->arguments(1);
		/** @var ProDraft $draft */
		$draft = $page->draft();
		$changes = $draft->changes;

		if(!$draft->exists() || (!$draft->publish && $draft->publishField != $field->name)) return;
		if(!in_array($field->name, $changes) && !$page->isChanged($field->name)) return;
		if(!$this->drafts->isAllowedField($field->name)) return;

		$value = $e->return;

		// if not a value that we operate on, don't continue
		if(!is_string($value) && !is_array($value)) return;

		$numChanges = 0;
		$value = $this->updateMarkupReferences($page, $field, $value, $numChanges);

		if($numChanges) $e->return = $value;
	}

	/**
	 * Update any markup href or src references for draft assets in $value to point to live assets
	 *
	 * @param Page $page
	 * @param Field $field
	 * @param array|string $value
	 * @param int $numChanges Populates the number of changes made
	 * @return array|string Updated value
	 *
	 */
	protected function updateMarkupReferences(Page $page, Field $field, $value, &$numChanges) {

		$isArray = is_array($value);
		$config = $this->wire('config');
		$data = $isArray ? $value : array($value);
		$rootURL = $config->urls->root;
		$filesURL = $config->urls->files;
		if($rootURL != '/') $filesURL = str_replace($rootURL, '/', $filesURL);
		$dirs = null;

		foreach($data as $k => $v) {
			if(!is_string($v)) continue;
			if(strpos($v, $filesURL) === false) continue;
			if(is_null($dirs)) {
				$dirs = $this->drafts->getFileDirs($page);
				if($rootURL != '/') {
					// normalize in case site is moved from subdir to non-subdir while still containing drafts
					$dirs['url'] = str_replace($rootURL, '/', $dirs['url']);
					$dirs['_url'] = str_replace($rootURL, '/', $dirs['_url']);
				}
			}
			if(strpos($v, $dirs['_url']) === false) continue;
			$this->drafts->debugMessage("Updating $field->name for $dirs[_url] => $dirs[url]", __FUNCTION__);
			$data[$k] = str_replace($dirs['_url'], $dirs['url'], $v);
			$numChanges++;
		}

		return $isArray ? $data : reset($data);
	}

	/**
	 * Hook to Pages::deleted
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookPageDeleted(HookEvent $e) {
		$page = $e->arguments(0);
		$this->drafts->deleteDraft($page);
	}

}