<!DOCTYPE html> 
<html> 
<head>
	<title><?php echo $browserTitle ?></title>

	<link rel="stylesheet" href="<?php echo $config->urls->ProDrafts; ?>layout/source/stable/layout-default.css">
	<link rel="stylesheet" href="<?php echo $config->urls->ProDrafts; ?>ProcessProDraftsEdit.css">

	<?php
	foreach($config->scripts as $file) {
		if(strpos($file, 'JqueryCore.js') === false && strpos($file, 'JqueryUI.js') === false) continue;
		echo "<script src='$file'></script>";
	}
	?>

	<script src="<?php echo $config->urls->ProDrafts; ?>layout/source/stable/jquery.layout.js"></script>
	<script>
		
		$(document).ready(function() {
			
			var layout;
			
			function viewBothResize() {
				var windowWidth = $(document).width() / 2;
				layout.sizePane("west", windowWidth);
			}

			layout = $('body').layout({
				resizable: true,
				slidable: true,
				closable: false,
				maskContents: true
			});
			
			viewBothResize();
			
			setTimeout(function() {
				$(window).resize(viewBothResize);
			}, 2000);
			
		});

	</script>
	
</head>
<body class='ProcessProDraftsViewBoth'>	
	<iframe id='pwpd-draft' class="pane ui-layout-west" src='<?php echo $draftURL; ?>'></iframe>
  	<iframe id='pwpd-published' class="pane ui-layout-center" src='<?php echo $publishedURL; ?>'></iframe>
</body>
</html>