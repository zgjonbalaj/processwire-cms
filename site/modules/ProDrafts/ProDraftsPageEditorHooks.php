<?php

/**
 * ProcessWire Pro Drafts: Page Editor Hooks
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 * 
 * @todo add a "Draft" section in Settings tab with info about draft and options to abandon any change.
 * 
 */

class ProDraftsPageEditorHooks extends Wire {

	/**
	 * @var ProDrafts
	 * 
	 */
	protected $drafts;

	/**
	 * Current draft being edited
	 * 
	 * @var null|ProDraft
	 * 
	 */
	protected $draft = null;

	/**
	 * The page used by /processwire/page/drafts/ with assigned ProcessProDrafts process
	 * 
	 * @var Page
	 * 
	 */
	protected $proDraftsPage;

	/**
	 * @var Page Page being edited
	 * 
	 */
	protected $editPage;

	/**
	 * Whether or not this request is processing a page edit form
	 * 
	 * @var bool
	 * 
	 */
	protected $processing = false;

	/**
	 * Text for draft notification
	 * 
	 * @var string
	 * 
	 */
	protected $draftNotice = '';

	/**
	 * Actions to accompany $draftNotice
	 * 
	 * @var array of markup strings
	 * 
	 */
	protected $draftActions = array();

	/**
	 * Current Process module
	 * 
	 * @var string|null
	 *
	 */
	protected $currentProcess = null;

	/**
	 * Are we currently in live preview mode?
	 * 
	 * @var bool
	 * 
	 */
	protected $isLivePreview = false;

	/**
	 * Construct page editor hooks
	 * 
	 * @param ProDrafts $drafts
	 * @param Page $page
	 * 
	 */
	public function __construct(ProDrafts $drafts, Page $page) {
		
		$this->drafts = $drafts;
		$this->isLivePreview = $this->wire('input')->get('pwpd_livepreview') ? true : false;
		$superuser = $this->wire('user')->isSuperuser();
		$config = $this->wire('config');
		
		if($page->process == 'ProcessPageEditImageSelect') {
			$this->hookProcessPageEditImageSelect();	
			/*
			$this->addHookBefore('ProcessPageEditImageSelect::execute', $this, 'hookProcessPageEditImageSelect');
			$this->addHookBefore('ProcessPageEditImageSelect::executeResize', $this, 'hookProcessPageEditImageSelect');
			$this->addHookBefore('ProcessPageEditImageSelect::executeEdit', $this, 'hookProcessPageEditImageSelect');
			*/
			
		} else if($page->process == 'ProcessPageEdit') {
			
			$this->processing = count($_POST) > 0 && !$this->wire('config')->ajax;
			$this->addHookAfter('ProcessPageEdit::loadPage', $this, 'hookEditLoadPage');
			$this->addHookAfter('ProcessPageEdit::buildForm', $this, 'hookEditBuildForm');
			$this->addHookBefore('Session::redirect', $this, 'hookSessionRedirect');
			
			if(!$this->processing) {
				$this->addHookBefore('ProcessPageEdit::buildFormView', $this, 'hookEditBuildFormView');
				if($drafts->maxVersions > 0) {
					$this->addHookAfter('ProcessPageEdit::buildFormSettings', $this, 'hookEditBuildFormSettings');
				}
			}
			
			$moduleID = $this->wire('modules')->getModuleID('ProcessProDrafts');
			$this->proDraftsPage = $this->wire('pages')->get("template=admin, process=$moduleID, include=all");
			
		} else if(strpos((string) $page->process, 'ProcessPageList') === 0) {
			$config->scripts->add($config->urls->ProDrafts . 'ProDrafts.js');
		} 
		
		if($this->drafts->livePreview) {
			// for detection from ProDrafts.js
			$this->wire('adminTheme')->addBodyClass('ProDraftsUseLivePreview');
		}

		$this->currentProcess = $this->wire('page')->process;
		if(is_object($this->currentProcess)) $this->currentProcess = $this->currentProcess->className();
		
		if(!$superuser && $this->wire('permissions')->has('page-publish')) {
			// setup hooks for non-superusers on systems that have page-publish permission installed
			$processes = array(
				'ProcessPageList',
				'ProcessPageLister',
				'ProcessPageListerPro',
				'ProcessPageEdit',
				'ProcessProDrafts',
			);
			if(in_array($this->currentProcess, $processes)) {
				$this->addHookBefore('Page::editable', $this, 'hookBeforePageEditable');
				$this->addHookAfter('Page::editable', $this, 'hookAfterPageEditable');
				$this->addHookAfter('User::hasPagePermission', $this, 'hookAfterUserHasPagePermission');
			}
		}
		
	}
	
	/**
	 * Hook before Page::editable applicable for non-superuser installations with page-publish permission
	 *
	 * Temporarily makes page have unpublished status, so that we can make drafts of published pages editable.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookBeforePageEditable(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		$status = $page->get('status');
		if($status & Page::statusUnpublished) return;
		if(!$this->wire('user')->hasPermission('page-publish', $page)) {
			// user lacks page-publish permission to this page
			$page->setQuietly('_PWPD_status', $status);
			$status = $status | Page::statusUnpublished;
			$page->setQuietly('status', $status);
		}
	}

	/**
	 * Hook after Page::editable applicable for non-superuser installations with page-publish permission
	 *
	 * Follows up from the before hook above, restoring the previous status.
	 * Also revokes field edit permission when in ListerPro, to prevent editing of published pages,
	 * since ListerPro does not currently support drafts.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookAfterPageEditable(HookEvent $e) {

		/** @var Page $page */
		$page = $e->object;
		$status = $page->get('_PWPD_status');
		if($status !== null) $page->setQuietly('_PWPD_status', null); // remove from page

		// if no _PWPD_status was set, then user has page-publish permission to $page, or it is unpublished, so exit
		if($status === null) return;

		// user lacks page-publish permission to this page
		// restore previous status and remove temporary var from page
		$page->setQuietly('status', $status);

		// check if a field argument was provided to the editable() call
		if($e->return && $e->arguments(0)) {
			// field argument provided to $page->editable() call
			if($this->currentProcess == 'ProcessPageListerPro' && !$page->hasStatus(Page::statusUnpublished)) {
				// prevent ListerPro editing of published pages in ListerPro edit mode
				$e->return = false;
			}
		}
	}

	/**
	 * Hook after User::hasPagePermission, for non-superuser users without page-publish permission only
	 *
	 * Revokes permissions for published pages that should require page-publish
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookAfterUserHasPagePermission(HookEvent $e) {

		if(!$e->return) return;

		$page = $e->arguments(1);
		if(!$page) return;

		$permission = $e->arguments(0);
		if($permission instanceof Permission) $permission = $permission->name;
		if($permission == 'page-publish' || $permission == 'page-edit') return;

		// if PWPD_status is set then it indicates user does not have page-publish permission
		$status = $page->get('_PWPD_status');

		// if page is unpublished, and not as a result of our own hooks, then let the permission through
		if($page->hasStatus(Page::statusUnpublished) && $status === null) return;

		// user assumed to have requested permission at this point, determine if it should be revoked
		// the following permissions are those that we will revoke if needed
		static $checkPermissions = array(
			'page-move',
			'page-sort',
			'page-delete',
			'page-trash',
			'page-template',
			'page-hide',
			'page-lock',
			'page-rename',
		);

		if(in_array($permission, $checkPermissions)) {
			$user = $e->object;
			if(!is_null($status) || !$user->hasPermission('page-publish', $page)) {
				// revoke permission
				$e->return = false;
			}
		}
	}


	/**
	 * Hook before ProcessPageEditImageSelect::execute, ::executeResize and ::executeEdit
	 * 
	 */
	public function hookProcessPageEditImageSelect() {
		// @todo this needs to account for versions
		$id = (int) $this->wire('input')->get('id');
		if(!$id) {
			$file = $this->wire('input')->get('file');
			if($file) list($id, $unused) = explode(',', $file);
			$id = (int) $id; 
		}
		if($id) {
			$p = $this->wire('pages')->get($id);
			if($this->drafts->allowDraft($p)) {
				if($p->id && !$p->isUnpublished()) $this->drafts->makeDraftPage($p);
			}
		}
	}

	/**
	 * Hook to ProcessPageEdit::loadPage()
	 *
	 * Populate draft content to the $page being edited.
	 *
	 * @param HookEvent $e
	 * @throws WireException
	 *
	 */
	public function hookEditLoadPage(HookEvent $e) {

		$page = $e->return;
		$this->editPage = $page; 
		$input = $this->wire('input');
		$user = $this->wire('user');
		$drafts = $this->drafts;
		
		$this->wire('modules')->get('JqueryUI')->use('modal');
		$this->wire('modules')->get('JqueryCore')->use('cookie');
	
		// if drafts not allowed for this page, exit now
		if(!$this->drafts->allowDraft($page)) return;
	
		/*
		if($input->post('submit_save') && !$user->hasPermission('page-publish', $page)) {
			if($this->wire('permissions')->has('page-publish')) {
				throw new WireException("You do not have permission to publish this page");
			}
		}
		*/

		if($input->post('submit_save_draft')) {
		
			// set a _wasDraft var for inspection by ProDraftsHooks::hookFieldtypeSavePageField
			if(!$page->hasStatus(Page::statusUnpublished)) {
				$wasDraft = $drafts->hasDraft($page) ? true : false;
				$page->setQuietly('_wasDraft', $wasDraft);
				// save draft requested
				$drafts->makeDraftPage($page);
			}
			
			// make ProcessPageEdit proceed with its regular save by making it think the submit_save 
			// button was clicked rather than the submit_save_draft button
			$input->post->submit_save = 1;
			$drafts->debugMessage('Save draft', __CLASS__ . '.' . __FUNCTION__);
		
			// exclude autosave fields that match the autosaveNotRegex strings
			if($input->post('submit_save_draft') == 'autosave') {
				$notRegex = explode("\n", $this->drafts->autosaveNotRegex);
				foreach(array_keys($_POST) as $name) {
					$exclude = false;
					foreach($notRegex as $re) {
						if(preg_match(trim($re), $name)) {
							$exclude = true;
							break;
						}
					}
					if($exclude) {
						unset($_POST[$name]);
						$input->post->__unset($name);
					}
				}
			}

		} else if($this->wire('config')->ajax && isset($_SERVER['HTTP_X_FIELDNAME'])) {

			// ajax file submitted
			if(!$page->isUnpublished()) {
				$fieldName = $this->wire('sanitizer')->fieldName($_SERVER['HTTP_X_FIELDNAME']);
				if(strpos($fieldName, '_repeater') && ctype_digit(substr($fieldName, -1))) {
					// don't allow a draft to be made from a file field in a repeater
				} else {
					$drafts->makeDraftPage($page);
				}
			}

		} else if($input->post('submit_delete_draft') && $input->post('delete_draft')) {

			// delete draft requested and confirmed
			$drafts->deleteDraft($page);
			$drafts->debugMessage('Delete draft', __CLASS__ . '.' . __FUNCTION__);

		} else {

			// regular page load or publish requested
			// populate draft only if there is already one
			// @todo for versions see EXTRAS #1
			
			/** @var ProDraft $draft */
			$draft = $page->draft();
			$this->draft = $draft;
			
			if(!$page->publishable() && !$draft->exists() && !$page->isUnpublished()) {
				// regular page edit, where edit is only allowed if page is a draft
				$drafts->makeDraftPage($page);
			}
			
			if($draft->exists()) {
				if($input->post('submit_save')) {
					// publish requested, save unpopulated page to new version
					if($this->drafts->maxVersions > 0) {
						$version = $this->drafts->savePageVersion($page, $draft->changes);
						$this->drafts->debugMessage("Saved page version ($version)", __FUNCTION__);
					}
				}
				$draft->populatePage();
				// see EXTRAS #2
			}

			// if save requested and page is a draft, publish it
			if(($input->post('submit_save') || $input->post('submit_save_unpublished')) && $draft->exists()) {
				$page->setQuietly('_wasDraft', true);
				// publish draft
				$draft->publish = true; 
				// mark all fields in the draft as changed
				foreach($draft->changes() as $change) {
					$page->trackChange($change);
				}
			}
		}
	}

	/**
	 * Hook to ProcessPageEdit::buildForm()
	 *
	 * Adds a "Save Draft" button
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildForm(HookEvent $e) {

		/** @var ProcessPageEdit $process */
		$process = $e->object;
		
		/** @var Page $page */
		$page = $process->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		
		/** @var InputfieldForm $form */
		$form = $e->return;
		
		$isDraft = $page->isDraft();
		$wasDraft = $page->get('_wasDraft');

		// exclude page editors that are not in contexts we support
		$context = $this->wire('input')->get('context');
		if($context == 'PageTable' || $context == 'repeater') return; // @todo remove 'repeater' for repeater support
	
		// make sure that pwpd_livepreview is retained between PageEdit requests
		if($this->isLivePreview) {
			$form->set('action', $form->get('action') . '&pwpd_livepreview=1');
		}

		// updates to form specific to non-processing state
		if(!$this->processing && ($this->draft || $this->drafts->livePreview)) {
			$this->pageEditBuildForm($page, $form);
			// add ProDrafts notification to top
			if($this->wire('adminTheme')->addHookAfter('getExtraMarkup', $this, 'hookAdminThemeGetExtraMarkup'));
		}
	
		// remove disallowed fields from the form
		foreach($this->drafts->getDisallowFields() as $name) {
			if($name == 'name') continue;
			
			/** @var Inputfield $inputfield */
			$inputfield = $form->getChildByName($name);
			if(!$inputfield) continue;
			
			// if processing form now, and wasn't previously a draft, allow non-editable fields to be processed
			if($this->processing && $wasDraft === false) continue;
			
			if($isDraft || $wasDraft) {
				$inputfield->required = false;
				$inputfield->collapsed = Inputfield::collapsedYesLocked;
				if($inputfield->notes) $inputfield->notes .= "\n";
				$inputfield->notes .= $this->_('This field cannot be edited in a draft');
			} else {
				$inputfield->addClass('InputfieldNoProDraft', 'wrapClass');
			}
		}
	
		if($this->drafts->autosave > 0) {
			// identify fields that may not be autosaved
			foreach($this->drafts->autosaveNotField as $f) {
				list($id, $name) = explode('-', $f);
				$inputfield = $form->getChildByName($name);
				if(!$inputfield) {
					$field = $this->wire('fields')->get((int) $id);
					if($field->name != $name) $inputfield = $form->getChildByName($field->name);
				}
				if(!$inputfield) continue;
				$inputfield->addClass('InputfieldNoAutosave', 'wrapClass');
			}
		}
	}

	/**
	 * Updates to page edit form when not processing
	 * 
	 * Expects $this->draft to be populated with ProDraft
	 * 
	 * @param Page $page
	 * @param InputfieldForm $form
	 *
	 */
	protected function pageEditBuildForm(Page $page, InputfieldForm $form) {
		
		$draft = $this->draft;
		$isDraft = $page->isDraft();
		$changes = $draft->get('changes');
		$config = $this->wire('config');
		$user = $this->wire('user');
		$isUnpublished = false;
		
		// if page is unpublished, then no draft is needed
		if($draft->pageStatus & Page::statusUnpublished) {
			$isUnpublished = true;
			//$this->message($this->_('This page is currently unpublished and not visible on the site.'));
			//return;
		}

		// add custom css/js 
		$config->styles->add($config->urls->ProDrafts . 'ProDraftsPageEditorHooks.css');
		$config->scripts->add($config->urls->ProDrafts . 'ProDrafts.js');
		$config->scripts->add($config->urls->ProDrafts . 'md5.min.js');
		$config->scripts->add($config->urls->ProDrafts . 'ProDraftsPageEditorHooks.js');

		// publish button
		if(!$isUnpublished) {
			$submitPublish = $form->getChildByName('submit_publish');
			if($submitPublish) {
				$submitPublish->parent->remove($submitPublish);
			}
		}
		
		/** @var InputfieldSubmit $submit */
		$submit = $form->getChildByName('submit_save');
		
		if($submit && !$isUnpublished) {
			// Set existing submit button to be a "Publish" button rather than a "Save" button
			// And add a new "Save Draft" button
			$submit->attr('value', $this->_('Publish'));
			$button = $this->wire('modules')->get('InputfieldSubmit');
			$button->attr('id+name', 'submit_save_draft');
			$button->attr('value', $this->_('Save Draft'));
			$button->icon = $this->drafts->icon;
			$button->addClass('head_button_clone');
			if(!$submitPublish) $button->addClass('ui-priority-secondary');
			$form->insertAfter($button, $submit);
			
			if(!$page->publishable()) {
				$form->remove($submit);
			}
		}
		
		if(!$isUnpublished) $this->pageEditBuildFormDraft($page, $form, $draft);

		$jsconfig = $this->wire('config')->js('ProDrafts');
		if(empty($jsconfig)) $jsconfig = array();
		if(empty($jsconfig['urls'])) $jsconfig['urls'] = array();
		if(empty($jsconfig['labels'])) $jsconfig['labels'] = array();
		
		$notes = $this->_('This field contains unpublished changes');
		$jsconfig['labels']['hasChanges'] = $notes . ' ' . $this->_('(click icon to compare)');
		$jsconfig['labels']['notSupported'] = $this->_('This field does not support drafts. Changes will always save to the live version.');
		$jsconfig['labels']['saveDraftWarn'] = $this->_('Warning, changes to the following fields will be published immediately because they do not support drafts.');
		$jsconfig['labels']['saving'] = $this->_('Saving');
		$jsconfig['labels']['editingDraft'] = $this->_('1 change');
		$jsconfig['labels']['editingDraftNone'] = $this->_('No changes');
		$jsconfig['labels']['editingDraftPlural'] = $this->_('%d changes');
		$jsconfig['labels']['confirmExit'] = $this->_('There are unsaved changes. Are you sure you want to exit?');
		$jsconfig['labels']['confirmPublish'] = $this->_('Are you sure you want to publish?');
		$jsconfig['icon'] = $this->drafts->icon;
		$jsconfig['autosave'] = (int) $this->drafts->autosave;
		$jsconfig['autosaveInterval'] = (int) $this->drafts->autosaveInterval;
		$jsconfig['autosaveDelay'] = (int) $this->drafts->autosaveDelay;
		$jsconfig['livePreview'] = (int) $this->drafts->livePreview;
		$jsconfig['confirmPublish'] = (int) $this->drafts->confirmPublish;
		$jsconfig['unpublished'] = $this->editPage->isUnpublished() ? true : false;
		$jsconfig['debug'] = ProDrafts::debug;
		$jsconfig['urls']['compare'] = "{$this->proDraftsPage->url}changes/?id={$this->editPage->id}&field=";

		foreach($changes as $name) {
			if($name == 'name') $name = '_pw_page_name';
			$field = $form->getChildByName($name);
			if(!$field) continue;
			$field->addClass('InputfieldHasProDraft', 'wrapClass');
			//$jsconfig['urls']["compare_$name"] = "{$this->proDraftsPage->url}changes/?id={$this->editPage->id}&field=$name";
		}
		
		$this->wire('config')->js('ProDrafts', $jsconfig);
	}

	/**
	 * Updates specific to page edit form when not processing AND page is already a draft
	 * 
	 * Expects $this->draft to be populated with ProDraft
	 * 
	 * @param Page $page
	 * @param InputfieldWrapper $form
	 * 
	 */
	protected function pageEditBuildFormDraft(Page $page, InputfieldWrapper $form) {
		
		$draft = $this->draft;
		$changes = $draft->get('changes');
	
		/*
		// Convert existing "Delete" button to be a "Delete Draft" button
		$submit = $form->getChildByName('submit_delete');
		
		if($submit) {
			$submit->attr('name', 'submit_delete_draft');
			$submit->attr('value', $draft->version > 1 ? $this->_('Delete Version') : $this->_('Delete Draft'));
			$submit->attr('type', 'submit');

			$checkbox = $form->getChildByName('delete_page');
			$checkbox->attr('name', 'delete_draft');
			if($draft->version > 1) {
				$checkbox->attr('value', $checkbox->attr('value') . ':' . $draft->version);
				$checkbox->label = sprintf($this->_('Delete Version %s ?'), $draft->versionName);
				$checkbox->description = $this->_('Check the box to confirm you want to delete this version.');
			} else {
				$checkbox->label = $this->_('Delete/Abandon Draft?');
				$checkbox->description = $this->_('Check the box to confirm you want to abandon this draft. The page itself will remain, only the draft changes will be removed.');
			}
			if(count($changes)) {
				$checkbox->notes = $this->_('Changes have been made to the following fields:') . "\n• " . implode("\n• ", $changes);
			}
		}
		*/
	
		$actions = $this->getAllActions($page, $draft); 
	
		if($draft->version > 1) {
			// draft version notice
			$this->draftNotice = 
				sprintf($this->_('Warning: You are editing version %s.'), $draft->versionName) . ' ' .
				$this->_('Publish/save buttons will make this version the current live/draft page.') . '<br />';
			$this->draftActions['editCurrent'] = $actions['editCurrent'];
			if($page->viewable()) {
				$this->draftActions['viewVersion'] = $actions['viewVersion'];
				$this->draftActions['viewPublished'] = $actions['viewPublished'];
			}
			$this->draftActions['compare'] = $actions['compare'];
			
		} else {
			// regular draft notice
			$this->draftNotice = '';
			if($page->viewable()) {
				if($this->drafts->livePreview) $this->draftActions['livePreview'] = $actions['editPreview'];
				if(!$page->isUnpublished()) $this->draftActions['viewBoth'] = $actions['viewBoth'];
				$this->draftActions['viewDraft'] = $actions['viewDraft'];
				$this->draftActions['viewPublished'] = $actions['viewPublished'];
			}
			$this->draftActions['compare'] = $actions['compare'];
			if($this->drafts->hasDraftPermission($page, 'delete')) {
				$this->draftActions['abandon'] = $actions['abandon'];
			}
		}
	}

	/**
	 * Hook before Session::redirect
	 * 
	 * This is to ensure pwpd_livepreview GET vars are retained in query string
	 * 
	 * @param HookEvent $event
	 * 
	 */
	public function hookSessionRedirect(HookEvent $event) {
		$url = $event->arguments(0);	
		if($this->isLivePreview) {
			$url .= (strpos($url, '?') ? '&' : '?') . 'pwpd_livepreview=1';
			$event->arguments(0, $url);
		}
	}

	/**
	 * Get action links for draft actions
	 * 
	 * @param Page $page
	 * @param ProDraft $draft
	 * @return array
	 * 
	 */
	protected function getAllActions(Page $page = null, ProDraft $draft = null) {
		
		if(is_null($page)) $page = $this->editPage;
		if(is_null($draft)) $draft = $this->draft;
		
		$modal = (int) $this->wire('input')->get('modal');
		$modal = $modal ? "&modal=$modal" : "";
		$modalClass = 'pw-modal pw-modal-large pwpd-longclick pwpd-if-changes';
		$pageURL = $page->url;
		
		$actions = array(
			'viewDraft' => "<a target='_blank' class='$modalClass' href='$pageURL?draft=1'>" .
				$this->_('Draft') . "</a>",
			'viewVersion' => "<a target='_blank' class='$modalClass' href='$pageURL?draft=$draft->version'>" .
				$this->_('Version') . "</a>",
			'viewPublished' => "<a target='_blank' class='$modalClass' href='$pageURL'>" .
				$this->_('Published') . "</a>",
			'viewBoth' => "<a target='_blank' class='pwpd-if-changes' href='{$this->proDraftsPage->url}view/?id={$this->editPage->id}'>" .
				$this->_('Both') . "</a>",
			'compare' => "<a target='_blank' class='$modalClass' href='{$this->proDraftsPage->url}changes/?id=$page->id&draft=$draft->version'>" .
				$this->_('Differences') . "</a>",
			'abandon' => "<a class='pwpd-if-changes' href='{$this->proDraftsPage->url}action/?action=delete&ids[]=$page->id$modal&edit_after=$page->id'>" .
				$this->_('Abandon') . "</a>",
			'editCurrent' => "<a href='./?id=$page->id$modal'>" .
				$this->_('Return to Current') . "</a>",
			'editPreview' => "<a href='{$this->proDraftsPage->url}edit/?id={$this->editPage->id}'>" .
				$this->_('Live Preview') . "</a>",
		);
		
		return $actions;
	}

	/**
	 * Hook to AdminTheme::getExtraMarkup to add ProDrafts notification to page
	 * 
	 * Expects $this->draftNotice and $this->draftActions to be populated
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookAdminThemeGetExtraMarkup(HookEvent $e) {
	
		if(!count($this->draftActions)) {
			if(!$this->drafts->livePreview) return;
			$actions = $this->getAllActions();
			$this->draftActions = array('editPreview' => $actions['editPreview']);
		}
	
		/** @var AdminTheme $adminTheme */
		$adminTheme = $e->object;
		$adminClass = $adminTheme->className();
		$markup = $e->return;
		$draftIcon = "<i class='fa fa-fw fa-{$this->drafts->icon}'></i>";
		$arrowIcon = "<span><i class='fa fa-arrow-right fa-fw'></i></span>";
		$reno = $adminClass == 'AdminThemeReno' || in_array('AdminThemeReno', class_parents($adminTheme));
		
		if($this->isLivePreview) {
			// live preview
			$adminTheme->addBodyClass('ProDraftsLivePreview');
			$editURL = $this->editPage->editUrl();
			$noticeType = $reno ? 'detail' : 'NoticeWarning';
			$markup['notices'] .= 
				"<div class='ProDraftsNotice $noticeType'>" .
					"<div class='container'>" .
						"<p>$draftIcon " . 
							"<span class='ProDraftsNoticeText'>$this->draftNotice</span> &nbsp; " . 
							"<a class='pwpd-exit' href='#' data-url='$editURL'>" . 
								$this->_('Exit') . 
								"<i class='fa fa-fw fa-times-circle'></i> " .
							"</a>" . 
							"<a class='pwpd-refresh' href='#'>" . 
								$this->_('Refresh') .
								"<i class='fa fa-fw fa-refresh'></i> " . 
							"</a>" . 
						"</p>" . 
					"</div>" .
				"</div>";
			
		} else {
			// regular editor
			
			$actions = array(
				'view' => array(),
				'action' => array(),
			);
			
			foreach($this->draftActions as $key => $action) {
				if(strpos($key, 'view') === 0) {
					$actions['view'][] = $action;
				} else {
					$actions['action'][] = $action;
				}
			}
		
			$sep = " <span class='pwpd-if-changes'>&nbsp;/&nbsp;</span> ";
			$out =
				"<div class='ProDraftsViews'>" .
					(count($actions['view']) ?	
						"<i class='ProDraftsViewsIcon'><i class='fa fa-fw fa-eye'></i>&nbsp;</i>" . 
						"<strong class='pwpd-if-changes'>" . $this->_('View') . " <i class='fa fa-fw fa-angle-right'></i></strong>&nbsp;" . 
						implode($sep, $actions['view']) : '') . 
					"&nbsp; &nbsp; " . 
					"<a id='ProDraftsNoticeCloser' href='#' title='" . 
						$this->_('Click to hide drafts info') . "'><i class='fa fa-fw fa-times-circle'></i></a>&nbsp;" .
				"</div>" . 
				(count($actions['action']) ? 
					"<div class='ProDraftsActions'>" . 
						"$draftIcon <strong class='ProDraftsNoticeText'>$this->draftNotice</strong> " . 	
						"&nbsp;<i class='fa fa-angle-right'></i>&nbsp; " . 
						implode($sep, $actions['action']) . " &nbsp; " . 
					"</div>" : '');
			
			$class = 'ProDraftsNotice ';
			$class .= count($this->draft->changes()) ? 'pwpd-changes ' : 'pwpd-no-changes ';

			if((int) $this->wire('input')->cookie('ProDraftsNoticeClosed') > 0) {
				$adminTheme->addBodyClass('ProDraftsNoticeClosed');
				$class .= 'pwpd-notice-closed ';
			}

			if($reno) {
				$markup['content'] .=
					"<div class='$class AdminThemeReno detail ui-helper-clearfix'>" .
						$out . 
					"</div>";
			} else {
				$markup['notices'] .=
					"<div class='$class NoticeWarning'>" .
						"<div class='container ui-helper-clearfix'>" .
							$out . 
						"</div>" .
					"</div>";
			}
			
			$markup['notices'] .= "<a id='ProDraftsNoticeOpener' class='' href='#' title='" .
				$this->_('Click to show drafts info') . "'>$draftIcon&nbsp;</a>";
		}
		
		$e->return = $markup;
		$this->draftNotice = '';
	}

	/**
	 * Hook to ProcessPageEdit::buildFormView()
	 *
	 * Make the "View" link go to a draft view, when editing a draft
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildFormView(HookEvent $e) {
		/** @var ProcessPageEdit $editor */
		$editor = $e->object;
		/** @var Page $page */
		$page = $editor->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		$version = (int) $this->wire('input')->get('draft');
		$draft = $version > 1 ? $page->draft($version) : $page->draft();
		if($draft && $draft->exists()) {
			$url = $page->httpUrl() . "?draft=" . ($draft->version > 0 ? $draft->version : 1);
			$e->arguments(0, $url);
		}
	}

	/**
	 * Hook to ProcessPageEdit::buildFormSettings()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildFormSettings(HookEvent $e) {
		$process = $e->object;
		$page = $process->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		$form = $e->return;
		$f = $this->modules->get('InputfieldMarkup');
		$f->attr('id+name', 'ProDraftsVersions');
		$f->label = $this->_('Versions');
		$f->icon = $this->drafts->icon;
	
		/** @var MarkupAdminDataTable $table */
		$table = $this->wire('modules')->get('MarkupAdminDataTable');
		$table->setEncodeEntities(false);
		$table->headerRow(array(
			$this->_x('Version', 'version-th'), 
			$this->_x('Differences from live', 'version-th'), 
			$this->_x('User', 'version-th'),
			$this->_x('Actions', 'version-th')
		));
		
		$versions = $this->drafts->getVersions($page);
		if($this->drafts->hasDraft($page, 0)) {
			array_unshift($versions, $page->draft());
		}
		
		$labels = array(
			'view' => $this->_x('View', 'version-action'),
			'edit' => $this->_x('Edit', 'version-action'),
			'compare' => $this->_x('Differences', 'version-action'),
			'compareIcon' => "<i class='fa fa-exchange fa-fw ui-priority-secondary detail'></i>",
			'modal' => 'pw-modal pw-modal-large', 
		);
		
		$table->row(array(
			$this->_('Live'),
			$this->_('[none]'),
			$page->modifiedUser->name,
			/*
			"<span class='ProDraftsCompare' style='float:right;margin-right:2em;'>" .
				"<input type='radio' class='ProDraftsCompare1' name='_prodraft1' value='1' checked /> $labels[compareIcon] " . 
				//"<input type='radio' class='ProDraftsCompare2' name='_prodraft2' value='1' />" .
			"</span>" . 
			*/
			"<a class='$labels[modal]' href='$page->httpUrl'>$labels[view]</a>" . 
			($page->isDraft() ? " / <a href='./?id=$page->id'>$labels[edit]</a>" : "") 
			//($page->isDraft() ? " / <a class='pw-modal pw-modal-large' href='./?id=$page->id'>$labels[edit]</a>" : "")
		));
		
		$checkedVersion = $this->draft->version;
		
		foreach($versions as $draft) {
		
			//$ver = $draft->version;
			//$checked = $ver == $checkedVersion ? " checked" : "";
			$actions =
				"<a class='$labels[modal]' href='$page->httpUrl?draft=$draft->version'>$labels[view]</a> / " .
				"<a href='./?id=$page->id&draft=$draft->version'>$labels[edit]</a> / " .
				"<a class='$labels[modal]' href='{$this->proDraftsPage->url}changes/?id=$page->id&draft=$draft->version'>$labels[compare]</a>";
				/*
				"<span class='ProDraftsCompare' style='float:right;margin-right:2em;'>" . 
					"<input type='radio' class='ProDraftsCompare1' name='_prodraft1' value='$ver' /> $labels[compareIcon] " . 
					"<input type='radio' class='ProDraftsCompare2' name='_prodraft2' value='$ver'$checked />" . 
				"</span>" . 
				*/
			
			$table->row(array(
				$draft->versionName, 
				$this->wire('sanitizer')->entities(implode(', ', $draft->changes)), 
				$this->wire('users')->get($draft->modified_users_id)->name, 
				$actions
			));
		}

		/*
		$href = $this->proDraftsPage->url . "changes/?id=$page->id&draft=";
		$btn = $this->wire('modules')->get('InputfieldButton');
		//$btn->href = $href;
		$btn->attr('data-basehref', $href);
		$btn->value = $labels['compare'];
		$btn->attr('id', 'ProDraftsCompareButton');
		$btn->icon = 'arrow-up';
		$btn->addClass('pw-modal pw-modal-large');
		$btn->attr('style', 'float:right');
		*/
	
		$info = ProDrafts::getModuleInfo();
		//$f->value = $table->render() . $btn->render() . "<p class='detail'>ProDrafts v$info[version]</p>";
		$f->value = $table->render() . "<p class='detail'>ProDrafts v$info[version]</p>";
		$form->add($f);

	}

	/* 
	 * EXTRAS #1
     * @todo when we add back version support
	 * 
	$version = (int) $this->wire('input')->get('draft');
	if($version) {
		$draft = $page->draft($version);
		if(!$draft) {
			$this->error($this->_('Unknown version requested')); 
			$draft = $page->draft();
		} else {
		}
	} else {
		$draft = $page->draft();
	}
	 *
	 */
	
	/* 
	 * EXTRAS #2
	 *  
	if($page->modified > $draft->created) {
		$this->warning(
			$this->_('Live version of this page is newer than the draft.') . ' ' . 
			date('Y-m-d H:i:s', $page->modified) . " live modified time<br />" . 
			date('Y-m-d H:i:s', $draft->created) . " draft created time",
			Notice::allowMarkup
		);
	}
	*
	*/

	
}