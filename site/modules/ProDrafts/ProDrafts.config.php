<?php

/**
 * ProcessWire Pro Drafts: Module Configuration
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 *
 */

class ProDraftsConfig extends ModuleConfig {

	public function getDefaults() {
		return array(
			'draftsMode' => ProDrafts::draftsModeOff,
			'templateIDs' => array(),
			'maxVersions' => 0,
			'licenseKey' => '',
			'autosave' => 0,
			'autosaveNotRegex' => "/^delete_/\n/_delete$/\n",
			'autosaveNotInput' => array(),
			'autosaveInterval' => 1000, 
			'autosaveDelay' => 500,
			'livePreview' => 0, 
			'confirmPublish' => 0,
		);
	}
	
	public function getInputfields() {
		
		$data = $this->wire('modules')->getModuleConfigData('ProDrafts');
		$form = parent::getInputfields();
		if(empty($data['licenseKey'])) $form->add($this->getProductKeyInputfield($data));
		$labelOff = $this->_('No');
		$labelOn = $this->_('Yes');
		$labelMs = $this->_('Specify value in milliseconds (for example 1000 is 1 second). Setting this to lower numbers is useful for more frequent Live Preview updates (if used).');
		
		$fieldset = $this->modules->get('InputfieldFieldset');
		$fieldset->attr('name', '_drafts');
		$fieldset->label = $this->_('Drafts');
		$fieldset->icon = ProDrafts::defaultIcon;
		$form->add($fieldset);
		
		$f = $this->modules->get('InputfieldRadios');
		$f->attr('name', 'draftsMode');
		$f->label = $this->_('Enable drafts?');
		$f->icon = 'toggle-on';
		$f->addOption(ProDrafts::draftsModeOff, $labelOff);
		$f->addOption(ProDrafts::draftsModeAll, $labelOn . ' ' . $this->_('(all pages where supported)'));
		$f->addOption(ProDrafts::draftsModeTemplates, $labelOn . ' ' . $this->_('(only pages using templates you select)'));
		$fieldset->add($f);
		
		$f = $this->modules->get('InputfieldCheckboxes');
		$f->attr('name', 'templateIDs');
		$f->label = $this->_('Which page templates should use drafts?');
		$f->description = $this->_('Pages using the templates you select below will support drafts.');
		$f->set('optionColumns', 3);
		$f->icon = 'cubes';
		$f->showIf = 'draftsMode=2';
		foreach($this->wire('templates') as $template) {
			if($template->flags & Template::flagSystem) continue;
			$f->addOption($template->id, $template->name);
		}
		$fieldset->add($f);
		
		$f = $this->modules->get('InputfieldRadios');
		$f->attr('name', 'confirmPublish');
		$f->label = $this->_('Confirm before publishing?');
		$f->description = $this->_('When enabled, a confirmation dialog will confirm that the user intends to publish when they click the "Publish" button in the page editor.');
		$f->addOption(0, $labelOff);
		$f->addOption(1, $labelOn);
		$f->showIf = 'draftsMode>0';
		$f->icon = 'question-circle';
		if(empty($data['confirmPublish'])) $f->collapsed = Inputfield::collapsedYes;
		$fieldset->add($f);
		
		$fieldset = $this->modules->get('InputfieldFieldset');
		$fieldset->attr('name', '_autosave');
		$fieldset->label = $this->_('Automatic save');
		$fieldset->showIf = 'draftsMode>0';
		$fieldset->icon = 'life-ring';
		$form->add($fieldset);
		
		$f = $this->modules->get('InputfieldRadios');
		$f->attr('name', 'autosave');
		$f->label = $this->_('Enable automatic save?');
		$f->description .= $this->_('When enabled this automatically and silently saves changes in the background to your page draft, ensuring work is always up-to-date.');
		$f->notes = $this->_('Normal auto-save monitors keydown and change events and saves when there has been no UI interaction for about a second.');
		$f->notes .= ' ' . $this->_('Normal auto-save is recommended for most cases unless any lag is detected. Normal auto-save is also strongly recommended if using Live Preview.');
		$f->notes .= ' ' . $this->_('Relaxed auto-save checks less frequently and only monitors change events.');
		$f->icon = 'toggle-on';
		$f->addOption(0, $labelOff);
		$f->addOption(1, $labelOn . ' ' . $this->_x('(normal)', 'autosave-mode'));
		$f->addOption(5, $labelOn . ' ' . $this->_x('(relaxed)', 'autosave-mode'));
		$fieldset->add($f);
	
		$f = $this->modules->get('InputfieldAsmSelect');
		$f->attr('name', 'autosaveNotField');
		$f->label = $this->_('Exclude fields');
		$f->description = $this->_('Fields selected here will not be automatically saved. Use this option if you come across any input types that do not behave well with auto-save.');
		$f->collapsed = Inputfield::collapsedBlank;
		$f->showIf = 'autosave>0';
		$f->icon = 'minus-circle';
		foreach($this->wire('fields') as $field) {
			$f->addOption("$field->id-$field->name", $field->name);
		}
		$fieldset->add($f);

		$f = $this->modules->get('InputfieldTextarea');
		$f->attr('name', 'autosaveNotRegex');
		$f->label = $this->_('Exclude variables (advanced)');
		$f->description = $this->_('Some fields include several properties in submitted POST data, some of which you may not want to auto-save. For instance, deletion checkboxes on multi-value fields.');
		$f->description .= ' ' . $this->_('Use this option to specify regular expressions that match POST variable names that should be excluded. For most cases, you should leave these at the default.');
		$f->notes = $this->_('Specify one full PCRE regular expression per line.');
		$f->collapsed = Inputfield::collapsedYes;
		$f->showIf = 'autosave>0';
		$f->icon = 'minus-circle';
		$fieldset->add($f);
		
		$f = $this->modules->get('InputfieldInteger');
		$f->attr('name', 'autosaveInterval');
		$f->label = $this->_('Time interval');
		$f->description = $this->_('How often auto-save should look for changes.');
		$f->notes = $labelMs;
		$f->collapsed = Inputfield::collapsedYes;
		$f->showIf = 'autosave=1';
		$f->icon = 'clock-o';
		$fieldset->add($f);
		
		$f = $this->modules->get('InputfieldInteger');
		$f->attr('name', 'autosaveDelay');
		$f->label = $this->_('Interaction delay');
		$f->description = $this->_('How long auto-save should wait after the last keyboard interaction before saving changes.');
		$f->notes = $labelMs;
		$f->collapsed = Inputfield::collapsedYes;
		$f->showIf = 'autosave=1';
		$f->icon = 'clock-o';
		$fieldset->add($f);

		$fieldset = $this->modules->get('InputfieldFieldset');
		$fieldset->attr('name', '_livePreview');
		$fieldset->label = $this->_('Live preview');
		$fieldset->showIf = 'draftsMode>0, autosave>0';
		$fieldset->icon = 'eye';
		$form->add($fieldset);	
		
		$f = $this->modules->get('InputfieldRadios');
		$f->attr('name', 'livePreview');
		$f->label = $this->_('Enable live preview?');
		$f->description = $this->_('Live preview enables you to have the page editor and front-end page preview side-by-side, so that you can see your edits reflected in your site as you make them.');
		$f->description .= ' [' . $this->_('Read more about live preview') . '](https://processwire.com/api/modules/prodrafts/#live-preview)';
		$f->addOption(0, $labelOff);
		$f->addOption(1, $labelOn . ' ' . $this->_x('(normal)', 'livepreview-mode'));
		$f->addOption(2, $labelOn . ' ' . $this->_x('(refresh only)', 'livepreview-mode'));
		$f->notes = $this->_('The "normal" mode attempts to update the preview without reloading/refreshing the entire page, when possible.'); 
		$f->notes .= ' ' . sprintf($this->_('You can also [provide hints](%s) to live preview in normal mode.'), 'https://processwire.com/api/modules/prodrafts/#live-preview-hints-in-normal-mode');
		$f->notes .= ' ' . $this->_('The "refresh only" mode forces the preview to refresh the entire page for changes.');
		$f->notes .= ' ' . $this->_('Use the "refresh only" mode if you notice rendering problems when using the "normal" mode.');
		$f->icon = 'toggle-on';
		if(empty($data['livePreview'])) $f->collapsed = Inputfield::collapsedYes;
		$fieldset->add($f);
		
		$f = $this->modules->get('InputfieldHidden');
		$f->attr('name', 'maxVersions');
		$f->label = $this->_('Max versions');
		$f->description = $this->_('Maximum number of past versions to keep for each page.');
		$form->add($f);

		return $form;
	}

	/**
	 * @param array $data
	 * @return Inputfield
	 * 
	 */
	protected function getProductKeyInputfield(array $data) {
		/** @var Inputfield $f */
		$f = $this->wire('modules')->get('InputfieldText');
		$f->attr('id+name', 'licenseKey');
		$licenseKey = isset($data['licenseKey']) ? $data['licenseKey'] : '';
		$input = $this->wire('input');
		$session = $this->wire('session');
		$config = $this->wire('config');
		if($input->post('licenseKey') && $input->post('licenseKey') != $licenseKey) {
			// validate 
			$http = new WireHttp();
			$license = $this->wire('sanitizer')->text($input->post('licenseKey'));
			$data = array(
				'action' => 'validate',
				'license' => $license,
				'host' => $config->httpHost,
				'ip' => ip2long($session->getIP())
			);
			$result = $http->post('http://processwire.com/validate-product/', $data);
			if($result === 'valid') {
				$licenseKey = $license;
				$f->notes = "Validated!";
				$this->message("ProDrafts product key has been validated!");
			} else {
				$licenseKey = '';
				$f->error("Unable to validate product key: $result");
			}
		}
		if(empty($licenseKey)) $input->post->__unset('licenseKey');
		$f->attr('value', $licenseKey);
		$f->required = true;
		$f->label = $this->_('Product key');
		if($licenseKey) $f->label .= " - VALIDATED!";
		$f->attr('value', $config->demo ? 'disabled for demo mode' : $licenseKey);
		$f->icon = $licenseKey ? 'check-square-o' : 'question-circle';
		$f->description = "Paste in your ProDrafts product key.";
		$f->notes = "If you did not purchase ProDrafts for this site, please [purchase here](https://processwire.com/ProDrafts/).";
		if($licenseKey) $f->collapsed = Inputfield::collapsedYes;
		return $f;
	}

	
}
