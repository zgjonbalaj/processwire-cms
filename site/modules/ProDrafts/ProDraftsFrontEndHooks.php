<?php

/**
 * ProcessWire Pro Drafts: Front-End Hooks
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 *
 */

class ProDraftsFrontEndHooks extends Wire {
	
	/**
	 * Page ID that we have confirmed to be editable
	 *
	 * @var int
	 *
	 */
	protected $editableDraftPageID = 0;
	
	/**
	 * Data used for live preview
	 *
	 * @var array
	 *
	 */
	protected $livePreviewData = array();

	/**
	 * Field names requiring full page reload when used in live preview
	 *
	 * @var array
	 *
	 */
	protected $livePreviewReloadFields = array();

	/**
	 * @var ProDrafts
	 * 
	 */
	protected $drafts = null;

	/**
	 * Construct
	 * 
	 * @param ProDrafts $drafts
	 * 
	 */
	public function __construct(ProDrafts $drafts) {
		$this->drafts = $drafts;
		$this->addHookBefore('ProcessPageView::ready', $this, 'hookPageViewReady');
	}

	/**
	 * Hook to ProcessPageView::ready()
	 *
	 * We replace the current page with the draft, when it's editable and $input->get->draft == 1
	 *
	 * @param HookEvent $e
	 * @throws Wire404Exception if requested draft or version does not exist
	 *
	 */
	public function hookPageViewReady(HookEvent $e) {
	
		$version = (int) $_GET['draft'];
		if(!$version) return;
	
		// don't continue if template is admin
		$page = $this->wire('page');
		if($page->template == 'admin') return;
	
		// don't continue if user isn't logged in
		$user = $this->wire('user');
		if(!$user->isLoggedin()) return;
	
		// if the user is not superuser, apply additional permission checks
		if(!$user->isSuperuser()) {
			// check if system has page-publish permission installed
			if($this->wire('permissions')->has('page-publish')) {
				// perform editable check as if page was unpublished
				$status = $page->status;
				$page->setQuietly('status', $status | Page::statusUnpublished);
				$editable = $page->editable();
				$page->setQuietly('status', $status);
				if($editable) {
					// for future editable() checks that may be performed on front-end
					$this->editableDraftPageID = $page->id;
					$this->addHookAfter('Page::editable', $this, 'hookAfterPageEditable');
				} else {
					// disallow draft from being viewed
					return;
				}
			} else {
				// if page is not editable don't allow draft to be viewed
				if(!$page->editable()) return;
			}
		}
		
		// at this point we know the user is allowed to view the page draft
	
		// live preview hooks, if enabled
		if($this->drafts->livePreview && !empty($_GET['livepreview'])) {
			$this->addHookAfter('Fieldtype::formatValue', $this, 'hookFieldtypeFormatValue', array('priority' => 200));
			$this->addHookAfter('Page::render', $this, 'hookPageRender', array('priority' => 105));
		}

		// obtain the appropriate draft
		if($version > 1 && $this->drafts->maxVersions) {
			$draft = $page->draft($version);
		} else {
			$draft = $page->draft();
		}
		
		if(!$draft || !$draft->exists()) {
			// requested draft/version does not exist
			return;
		}

		// populate page with draft content
		$draft->populatePage(); 
	}

	/**
	 * Hook after Page::editable
	 *
	 * Assigned from the hookPageViewReady method
	 *
	 * @param HookEvent $event
	 *
	 */
	public function hookAfterPageEditable(HookEvent $event) {
		if($event->return) return;
		$page = $event->object;
		if($page->id == $this->editableDraftPageID) {
			$event->return = true;
		}
	}

	/**
	 * Hook after Fieldtype::formatValue (for live preview)
	 *
	 * Adds <!--PWPD--> comments in output of text values for later processing by hookPageRender
	 *
	 * @param HookEvent $event
	 *
	 */
	public function hookFieldtypeFormatValue(HookEvent $event) {
		$value = $event->return;
		if(strpos($value, '<!--PWPD') !== false) return;
		$page = $event->arguments(0);
		if($page->id != $this->wire('page')->id) return;
		$field = $event->arguments(1);
		if(!is_string($value) || !strlen($value)) {
			$this->livePreviewReloadFields[$field->id] = $field->name;
			return;
		}
		static $n = 0;
		$n++;
		$openComment = "<!--PWPD-$field->name-$n-->";
		$closeComment = str_replace("PWPD", "/PWPD", $openComment);
		$value = "$openComment$value$closeComment";
		$this->livePreviewData[$n] = array(
			'name' => $field->name,
			'openComment' => $openComment,
			'closeComment' => $closeComment,
			'class' => "pwpd-$field->name pwpd-$field->name-$n",
			'id' => "$n",
		);
		$event->return = $value;
	}

	/**
	 * Hook after Page::render (for live preview)
	 *
	 * Identifies <!--PWPD--> comments added by hookFieldtypeFormatValue and adds class names
	 * to closest HTML tags/elements, so that they can be recognized and selectively updated
	 * by the javascript side of live preview
	 *
	 * @param HookEvent $event
	 *
	 */
	public function hookPageRender(HookEvent $event) {

		$out = $event->return;
		$maxTries = 100;
		$page = $event->object; 
		$containers = array(
			'address', 'article', 'aside', 'blockquote', 'caption', 'col', 'colgroup',
			'dd', 'div', 'dl', 'dt', 'embed', 'fieldset', 'figcaption', 'figure', 'footer',
			'form', 'header', 'hgroup', 'li', 'main', 'nav', 'object', 'ol',
			'output', 'p', 'pre', 'section', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead',
			'title', 'tr', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'span', 'em', 'strong',
			'a', 'b', 'i', 'address', 'abbr'
		);

		// identify all open comments indicating a field that can use live preview
		// our goal is to locate the closest parent block level tag and add a pwpd-fieldName class to it

		// iterate live preview names collected from formatValue calls
		foreach($this->livePreviewData as $info) {

			$openComment = $info['openComment'];
			$closeComment = $info['closeComment'];
			$pwpdClass = $info['class'];
			$fieldName = $info['name'];
			$pwpdID = $info['id'];

			// iterate for each occurance of <!--PWPD-$fieldName-->
			$openPos = strrpos($out, $openComment);

			// if comment doesn't appear in markup, then skip it
			if($openPos === false) continue;

			// if the closeComment doesn't appear in the markup (or appears before) then it's
			// likely a partial value that we can't update
			if(strpos($out, $closeComment) < $openPos) continue;

			// str contains everything in the markup up until the <!--PWPD-$fieldName-->
			$str = substr($out, 0, $openPos);

			// find element closest to the end of $str
			foreach($containers as $element) {
				$pos = strripos($str, "<$element");
				if(strripos($str, "</$element>") > $pos) continue;
				$positions[$element] = $pos;
			}

			if(empty($positions)) break;
			arsort($positions);
			$n = 0;

			do {
				if(++$n > $maxTries) break;
				$pos1 = array_shift($positions);
				if(empty($pos1)) {
					$elem = null;
					break;
				}
				$pos2 = strpos($str, '>', $pos1);
				$elem = substr($str, $pos1, ($pos2 - $pos1) + 1);
				if(empty($elem) || strpos($elem, 'pw-edit-')) {
					$elem = null;
					continue;
				} else {
					if(substr_count($out, $elem) === 1) break;
				}
			} while(1);

			// if we didn't find the element we can use, abort
			if(!$elem || empty($positions)) continue;

			// replacement of $elem that will contain our class
			$repl = $elem;
			//echo "<p>$fieldName: " . htmlentities($repl) . "</p>";

			if(strpos($repl, ' data-pwpd=') === false) {
				$repl = str_replace(">", " data-pwpd='$pwpdID'>", $repl);
			} else {
				// use existing data-pwpd, and update pwpdClass to use it too
				if(preg_match('/data-pwpd=\'(\d+)\'/', $repl, $matches)) {
					$pwpdClass = str_replace("-$pwpdID", "-" . $matches[1], $pwpdClass);
				}
			}

			// update or add a class attribute
			if(stripos($repl, 'class=') === false) {
				// no class attribute present yet, add one
				if(strpos($repl, 'pwpd ') === false) $pwpdClass = "pwpd $pwpdClass";
				$repl = str_replace('>', " class='$pwpdClass'>", $repl);
				
			} else {
				// may need to add pwpd classes to existing class attribute
				$parts = preg_split('![^-_a-zA-Z0-9]!', $repl);
				if(in_array("pwpd-$fieldName", $parts)) {
					// needed pwpd class is already present
				} else if(preg_match('!(\sclass\s*=\s*)(["\']?)\s*([^"\'\s<>=/]*)!i', $repl, $matches)) {
					// need to add the pwpd classes
					$full = $matches[0];
					$attr = $matches[1];
					$quote = $matches[2];
					$class = $matches[3];
					if(!in_array('pwpd', $parts)) $pwpdClass = "pwpd $pwpdClass";
					if($quote) {
						$repl = str_replace($full, "$full $pwpdClass", $repl);
					} else {
						$repl = str_replace($full, "$attr'$class $pwpdClass'", $repl);
					}
				} else {
					// can't identify or use class attribute
					continue; 
				}
			}
				
			$out = str_replace($elem, $repl, $out);
			$out = str_replace(array($openComment, $closeComment), '', $out);

			//echo "<p>Replace: \"" . htmlentities($elem) . "\" => \"" . htmlentities($repl). "\"</p>";

		} // foreach fieldNames

		// cleanup, in case anything left
		if(strpos($out, 'PWPD-') !== false) {
			$out = preg_replace('%<!--/?PWPD-[^>]+>%', '', $out);
		}

		// populate list of fields requiring reload to the body tag		
		$skip = "|" . implode("|", $this->livePreviewReloadFields) . "|";
		$out = str_ireplace('<body', "<body data-pwpd-reload='$skip'", $out);
		
		// identify links for multi-language environment
		if($this->wire('modules')->isInstalled('LanguageSupportPageNames')) {
			$urls = array();
			$user = $this->wire('user');
			$lang = $user->language;
			foreach($this->wire('languages') as $language) {
				$user->language = $language;	
				$urls[$language->name] = $page->url();
			}
			$user->language = $lang;
			if(preg_match_all('!(\shref=["\']?)(' . implode('|', $urls) . ')(["\'\s>])!', $out, $matches)) {
				$replacements = array();
				foreach($matches[0] as $key => $full) {
					$start = $matches[1][$key];
					$href = $matches[2][$key]; 
					$close = $matches[3][$key];
					$replacements[$full] = "$start$href?draft=1&livepreview=1$close";
				}		
				$out = str_replace(array_keys($replacements), array_values($replacements), $out);
			}
		}

		$event->return = $out;
	}
}