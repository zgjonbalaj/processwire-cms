<!DOCTYPE html> 
<html> 
<head>
	<title><?php echo $browserTitle ?></title>

	<link rel="stylesheet" href="<?php echo $config->urls->ProDrafts; ?>layout/source/stable/layout-default.css">
	<link rel="stylesheet" href="<?php echo $config->urls->ProDrafts; ?>ProcessProDraftsEdit.css">

	<?php
	foreach($config->scripts as $file) {
		if(strpos($file, 'JqueryCore.js') === false && strpos($file, 'JqueryUI.js') === false) continue;
		echo "<script src='$file'></script>";
	}
	?>
	
	<script src="<?php echo $config->urls->ProDrafts; ?>layout/source/stable/jquery.layout.js"></script>
	<script src="<?php echo $config->urls->ProDrafts; ?>ProcessProDraftsEdit.js"></script>
	
</head>
<body class='ProcessProDraftsEdit'>	
  	<iframe id='pwpd-site' class="pane ui-layout-center" data-src='<?php echo $siteURL; ?>' src='<?php echo $siteURL; ?>'></iframe>
	<iframe id='pwpd-admin' class="pane ui-layout-west" src='<?php echo $editURL; ?>'></iframe>
</body>
</html>