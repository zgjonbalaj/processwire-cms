/**
 * ProcessWire Pro Drafts: Live Preview
 * 
 * This is the javascript for ProcessProDraftsEdit.php 
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * https://processwire.com/ProDrafts/
 *
 */

var ProDraftsLivePreview = {

	/**
	 * Force reload of the site window
	 *
	 */
	reloadSiteAll: function() {
		var $site = $('#pwpd-site');
		if($site.attr('src') == $site[0].contentWindow.document.location.href) {
			$('#pwpd-site')[0].contentWindow.document.location.reload();
		} else {
			$site[0].contentWindow.document.location = $site.attr('src');
		}
	},

	/**
	 * Reload the site window
	 *
	 * @param array fieldNames
	 *
	 */
	reloadSite: function(fieldNames) {

		// contents of the site window
		var $contents = $('#pwpd-site').contents();
		
		// is a full refresh/reload needed?
		var needsRefresh = false;
		
		// pipe separated list of field names that require reload
		var reloadStr = $contents.find('body').attr('data-pwpd-reload');

		if(typeof reloadStr == "undefined") {
			// if no data-pwpd-reload attribute specified on body tag, force refresh
			needsRefresh = true;
			
		} else {
			// determine if a refresh will be needed or not
			for(var n = 0; n < fieldNames.length; n++) {
				var fieldName = fieldNames[n];
				if(reloadStr.indexOf('|' + fieldName + '|') > -1) {
					// check if a user specified selector is available
					if($contents.find('.pwpd-' + fieldName).length == 0) {
						needsRefresh = true;
						break;
					}
				}
			}
		}

		if(needsRefresh) {
			ProDraftsLivePreview.reloadSiteAll();
			return;
		}

		$('#pwpd-admin')[0].contentWindow.ProDraftsSpinner.start();

		$.get($('#pwpd-site').attr('data-src'), function(data) {
			
			console.log(fieldNames);
			
			var $data = $(data);
			
			for(var n = 0; n < fieldNames.length; n++) {

				var fieldName = fieldNames[n];
				if(fieldName === null) continue;

				var selector = '.pwpd-' + fieldName;
				var $items = $contents.find(selector);
				
				$items.each(function() {
					
					var id, sel, $elem, pwpdID;
					var $item = $(this);
					
					// items with class "pwpd" are those added automatically by ProDraftsFrontEndHooks
					// so the next two variables contain only parent/nested items added by user
					var $userParentItems = $item.parents(selector + ':not(.pwpd)');	
					var $userNestedItems = $item.find(selector + ':not(.pwpd)');

					// don't use automatically added items if there are user specified ones
					// instead, we let the user specified ones take over
					if($userParentItems.length) {
						console.log('skipping because has parent items:');
						console.log($item);
						return;
					}
					if($userNestedItems.length) {
						console.log('skipping because has nested items:');
						console.log($item);
						return;
					}
					
					pwpdID = $item.hasClass('pwpd') ? $item.attr('data-pwpd') : null;
					
					if(typeof pwpdID == "undefined" || pwpdID == null || !pwpdID.length) {
						// item has no data-pwpd attribute, likely a user specified pwpd-[fieldName] class
						id = $item.attr('id');	
						// check if it has an id attribute
						if(typeof id != "undefined" && id != null && id.length) {
							// use the id attribute
							sel = '#' + id;
						} else {
							// use just the class attribute
							sel = selector + ':not(.pwpd)';
						}
					} else {
						// use the two class names
						sel = selector + '.pwpd-' + fieldName + '-' + pwpdID;
					}
					
					$elem = $data.find(sel);
					console.log('sel=' + sel);
					console.log($elem);
					
					if($elem.length) {
						$contents.find(sel).empty().html($elem.html());
					}
					
				});
				
				/*
				} else {
					// check for user-specified class
					selector = '.pd-' + fieldName;
					$items = $contents.find(selector);
					$items.each(function() {
						var $item = $(this);
						var id = $item.attr('id');	
						var sel = selector;
						if(typeof id != "undefined" && id.length) {
							// item to update has an 'id' attribute, so we'll use that
							// this enables different rendered instances of the same field
							sel = '#' + id;
						}
						var $elem = $data.find(sel);
						if($elem.length) $contents.find(sel).empty().html($elem.html());
					});
				}
				*/
				
				// console.log($(data).find(selector));
				// $contents.find(selector).empty().append($(data).find(selector).children());
				
				$('#pwpd-admin')[0].contentWindow.ProDraftsSpinner.stop();

			}
		});
	},

	/**
	 * Initialize live preview
	 * 
	 */
	init: function() {
		
		var windowWidth = $(document).width();
		var editorWidth = windowWidth / 2.8;

		var layout = $('body').layout({
			resizable: true,
			slidable: true,
			closable: false,
			maskContents: true
		});
		
		if(editorWidth < 350) editorWidth = 350;

		layout.sizePane("west", editorWidth);

		//var siteWindow = $('#pwpd-site')[0].contentWindow.document;
		//var adminWindow = $('#pwpd-admin')[0].contentWindow.document;

		setTimeout(function() {
			$('#pwpd-admin').on('load', function() {
				ProDraftsLivePreview.reloadSiteAll();
			});
			$('#pwpd-site').on('load', function() {
				var src = $('#pwpd-site')[0].contentWindow.document.location.href;
				if(src.indexOf('livepreview=1') > -1) {
					$('#pwpd-site').attr('data-src', src);
				}
			});
		}, 2000);
	}
}

/**
 * jQuery document.ready
 * 
 */
jQuery(document).ready(function($) {
	ProDraftsLivePreview.init();	
});
	
	
