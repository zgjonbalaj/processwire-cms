<?php

/**
 * ProcessWire Ready File
 *
 * This file is included when ProcessWire has finished rendering and delivering
 * a page, and is in the process of shutting down. It is called immediately
 * before the API is disengaged, so you can still access any API variable and
 * update $session values as needed. Admittedly, this is probably not the place
 * you would put hooks, but it is an ideal place to perform your own shutdown,
 * should your application call for it.
 *
 */