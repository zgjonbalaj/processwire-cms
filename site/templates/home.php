<?

# Include DOC Header
require("_includes/header/doc-header.inc");

?>
	<body id="pg-<?= $page->id ?>">

		<div class="clearfix oc-container oc-left oc-js">
			<div class="oc-overlay oc-js"></div>

			<!-- Include Header -->
			<? require("_includes/header/header.inc"); ?>

			<section class="container">
				<div class="row">
					<div class="col-xs-12">
						<?= $page->body ?>
					</div>
				</div>
			</section>

			<!-- Footer -->
			<? require("_includes/footer/footer.inc"); ?>

		</div>
		<!-- End Off Canvas -->

	</body>
</html>