/**
 *
 * Document Ready Functions
 *
 **/

$(document).ready(function() {

  /*----------  Off Canvas Utilities  ----------*/
  $('#oc-icon, .oc-overlay').stop().click(function() {
    $('.oc-js').toggleClass('active');
  });

  $('span.oc-toggle').stop().click(function(e) {
    e.preventDefault();
    $(this).next('ul').stop().slideToggle();
    if ($(this).hasClass('glyphicon-plus')) {
      $(this).stop().removeClass('glyphicon-plus').addClass('glyphicon-minus');
    } else {
      $(this).stop().removeClass('glyphicon-minus').addClass('glyphicon-plus');
    }
  });

  // MatchHeight .eh Elements

  $.fn.matchHeight._maintainScroll = true;

  $(".eh").matchHeight();

});


/**
 *
 * Window Resize Functions
 *
 **/

$(window).resize(function() {

  /*----------  Off Canvas Menu Hide  ----------*/
  var windowWidth = $(window).width();

  if (windowWidth >= 768) {
    $('.oc-js').removeClass('active');
    $('span.oc-toggle').each(function() {
      $(this).removeClass('glyphicon-minus').addClass('glyphicon-plus');
      $(this).next('ul').css('display', '');
    });
  }

  // MatchHeight Update
  $.fn.matchHeight._update();

});
