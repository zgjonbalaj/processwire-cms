<header>
	<div class="container">
		<div class="row">

			<!-- Logo -->
			<div class="col-xs-12">
				<div class="logo">
					<h1><?= $settings->company ?></h1>
				</div>
			</div>

		</div>
	</div>
</header>

<!-- Main Navigation -->
<div class="container-nav">
	<? require("_includes/nav/main.inc"); ?>
</div>
