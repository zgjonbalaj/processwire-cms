<?php

	# Preload Checks
	require("_includes/lib/cms-onload.inc");

	# Load Global Variables
	require("_includes/lib/cms-globals.inc");

	# Include CMS Class
	require("_includes/lib/cms-functions.inc");

?>

<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="<?= $cms->generateCompany() ?>">
		<meta name="copyright" content="<?= $cms->generateCompany() ?>">
		<meta name="description" content="<?= $cms->generateDescription($page) ?>">
		<meta name="robots" content="<?= $cms->generateRobots($page) ?>">

		<meta property="og:site_name" content="<?= $cms->generateCompany() ?>">
		<meta property="og:title" content="<?= $cms->generateTitle($page) ?>">
		<meta property="og:url" content="<?= $page->httpUrl ?>">
		<meta property="og:description" content="<?= $cms->generateDescription($page) ?>">
		<meta property="og:type" content="<?= $page->seo->{'og:type'} ?>">

		<title><?= $cms->generateTitle($page) ?></title>

		<link href="<?= $cms->generateCanonical($page) ?>" rel="canonical">
		<link href="<?= $assets ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= $assets ?>/js/slick/slick.css" rel="stylesheet">
		<link href="<?= $assets ?>/js/slick/slick-theme.css" rel="stylesheet">
		<link href="<?= $assets ?>/css/main.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<link href="<?= $assets ?>/css/ie.css" rel="stylesheet">
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
