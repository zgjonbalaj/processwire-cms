	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?= $assets ?>/js/jquery/jquery-3.1.1.min.js"><\/script>')</script>
	<script src="<?= $assets ?>/js/bootstrap/bootstrap.min.js"></script>
	<script src="<?= $assets ?>/js/slick/slick.min.js"></script>
	<script src="<?= $assets ?>/js/notify-js/notify.min.js"></script>
	<script src="<?= $assets ?>/js/matchheight/jquery.matchHeight-min.js"></script>
	<script src="<?= $assets ?>/js/responsifyjs/responsify.min.js"></script>
	<script src="<?= $assets ?>/js/main.js"></script>

	<? if (($settings->live->value == "live") && !($page->isHidden()) && ($settings->google_analytics != '')): ?>

	<? require("google-analytics.inc"); ?>

	<? else: ?>

	<? require("google-analytics-dev.inc"); ?>

	<? endif ?>

	<? require("_includes/lib/cms-notifications.inc"); ?>