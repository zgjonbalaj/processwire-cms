<div id="oc-icon" class="oc-js">
  <span class="bar1"></span>
  <span class="bar2"></span>
  <span class="bar3"></span>
  <span class="bar4"></span>
</div>

<nav id="nav" role="navigation" class="clearfix oc-container oc-right oc-js">
	<? # TOP LEVEL ?>
	<ul class="clearfix">
		<li <?= $cms->liAttr($site->url) ?>><a href="/">Home</a></li>
		<? foreach ($site->children as $child): ?>
			<li <?= $cms->liAttr($child->url) ?>><a<?= $cms->aAttr($child->url) ?>><?= $child->title ?></a>
				<? if ($child->hasChildren): ?>
					<span <?= $cms->spanAttr($child->url) ?>></span>

					<? # FIRST LEVEL ?>
					<ul class="clearfix">
						<? foreach ($child->children as $child_1): ?>
							<li <?= $cms->liAttr($child_1->url) ?>><a<?= $cms->aAttr($child_1->url) ?>><?= $child_1->title ?></a>
								<? if ($child_1->hasChildren): ?>
								<span <?= $cms->spanAttr($child_1->url) ?>></span>

									<? # SECOND LEVEL ?>
									<ul class="clearfix">
										<? foreach ($child_1->children as $child_2): ?>
											<li <?= $cms->liAttr($child_2->url) ?>><a<?= $cms->aAttr($child_2->url) ?>><?= $child_2->title ?></a>
												<? if ($child_2->hasChildren): ?>
													<span <?= $cms->spanAttr($child_2->url) ?>></span>

													<? # THIRD LEVEL ?>
													<ul class="clearfix">
														<? foreach ($child_2->children as $child_3): ?>
															<li <?= $cms->liAttr($child_3->url) ?>><a<?= $cms->aAttr($child_3->url) ?>><?= $child_3->title ?></a></li>
														<? endforeach ?>
													</ul>
												<? endif ?>
											</li>
										<? endforeach ?>
									</ul>
								<? endif ?>
							</li>
						<? endforeach ?>
					</ul>
				<? endif ?>
			</li>
		<? endforeach ?>
	</ul>
</nav>