<?php

	namespace ProcessWire;

	# PHP INFO Request
	if (isset($input->get->phpinfo)) {
		if ($user->isSuperuser()) {
			phpinfo();
			exit;
		} else {
			$session->redirect('/manage/');
		}
	}

	# Hidden Pages During Development
	$allowed_urls = array(
		'/',
	);

	# Determine Source, Allowed Page, Superuser Access
	if (!in_array($page->url, $allowed_urls) && !$user->isSuperuser()) {
		$session->redirect('/');
	}

	# Check for redirect headers
	$page->redirect ? $session->redirect($page->redirect) : '';

	# Set Error Reporting
	error_reporting($config->debug ? 0 : E_ALL);

	# Cache Control
	if (isset($input->get->clearcache)) {
		$notifyJs    = true;
		$cache       = wire('modules')->get('ProCache');
		$cachedPages = $cache->numCachedPages();
		$info        = $cache->pageInfo($page);

		# Determine case and send proper message to notify.js.
		switch ($input->get->clearcache) {
			case "all":

				# Clear entire site cache.
				$cache->clearAll();
				$notifyMsg  = array("Cleared cache memory!", "success");
				break;
			case "page":

				# Clear the current page cache.
				$cache->clearPage($page);
				$notifyMsg  = array("Page cache cleared!", "success");
				break;
			case "info":

				# Return information about total pages cached.
				$notifyMsg  = array("Total pages cached: {$cachedPages}", "info");
				break;
			default:

				# Dump the cache data of this page.
				if ($info != false) {
					$notifyMsg = array(var_dump($info), "info");
				} else {
					$notifyMsg = array("This page is not cached!", "warn");
				}
				break;
		}

	}

	# Browser Cache Control
	if (isset($input->get->clearbrowsercache)) {
		header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$notifyJs  = true;
		$notifyMsg = array("Cleared browser cache!", "success");

	}



?>