<?php

	namespace ProcessWire;

	/**
	*
	* The following class adds varying functionality to the templates
	* through ProcessWire. Current available methods are:
	*
	**/

	class customFunctions extends Wire {

		/**
		*
		* Generates specific classes for each list item during
		* page navigation output.
		*
		**/

		public function liAttr($url) {

			$page        = wire('page');
			$pages       = wire('pages');
			$parents		 = $page->parents();
			$currentPage = $pages->get($url);
			$classes     = "";
			$active      = false;

			// Check if the link is the current page and add
			// active class to the li.

			if ($page->url == $url) {
				$active = true;
			}

			// If the current link is a child of this page add
			// class to the current link.

			if ($parents->has("url=$url")) {
				$active = true;
			}

			// If any of the previous results returned active
			// add the class. (Note) This will add active to
			// all parent & rootParent pages.

			if ($active) {
				$classes .= "active ";
			}

			// If the page has children add has-children to it

			if ($currentPage->numChildren > 0) {
				$classes .= "has-children ";
			}

			// Get the URL of the link and add the full URL
			// with "-" as delimiter as class.

			$li_class = str_replace('/', '-', $url);
			$li_class = ltrim($li_class, "-");
			$li_class = rtrim($li_class, "-");
			$classes .= $li_class;

			// If no classes are available or have not been
			// created then do nothing otherwise add all the
			// classes together and echo them.

			if ($classes != "") {
				return "class=\"" . $classes . "\"";
			}
		}

		/**
		*
		* Generates specific attributes for each link during
		* page navigation output.
		*
		**/

		public function aAttr($url) {

			$pages       = wire('pages');
			$currentPage = $pages->get($url);
			$attributes  = "";

			if ($currentPage->redirect) {
				$urlParts = parse_url($currentPage->redirect);
				if (!empty($urlParts['host']) && $urlParts['host'] != $_SERVER['HTTP_HOST']) {
					$attributes .= " href=\"$currentPage->redirect\" target=\"_blank\"";
				} else {
					$attributes .= " href=\"$currentPage->redirect\"";
				}
			} else {
				$attributes .= " href=\"$url\"";
			}

			// Return all attributes
			if ($attributes != "") {
				return $attributes;
			}
		}

		/**
		*
		* Generates specific attributes for each oc span element during
		* page navigation output.
		*
		**/

		public function spanAttr($url) {

			$page        = wire('page');
			$pages       = wire('pages');
			$parents     = $page->parents();
			$currentPage = $pages->get($url);
			$classes     = "";
			$active      = false;

			// Check if the link is the current page and add
			// active class to the li.

			if ($page->url == $url) {
				$active = true;
			}

			// If the current link is a child of this page add
			// class to the current link.

			if ($parents->has("url=$url")) {
				$active = true;
			}

			// If any of the previous results returned active
			// add the class. (Note) This will add active to
			// all parent & rootParent pages.

			if ($active) {
				$classes .= "glyphicon-minus ";
			} else {
				$classes .= "glyphicon-plus ";
			}


			// If the page has children add has-children to it

			if ($currentPage->numChildren > 0) {
				$classes .= "has-children ";
			}

			// Get the URL of the link and add the full URL
			// with "-" as delimiter as class.

			$li_class = str_replace('/', '-', $url);
			$li_class = ltrim($li_class, "-");
			$li_class = rtrim($li_class, "-");
			$classes .= $li_class;

			// If no classes are available or have not been
			// created then do nothing otherwise add all the
			// classes together and echo them.

			if ($classes != "") {
				return "class=\"oc-toggle visible-xs glyphicon " . $classes . "\"";
			}
		}

		/**
		*
		* Function to prevent one word widows in <h1></h1> by
		* adding a nonbreaking space between the last two words
		* takes in a block of HTML code
		*
		**/

		public function preventWidow($html) {

			$headline_start = stripos($html, '<h1>');
			$headline_end = stripos($html, '</h1>');

			if ($headline_start !== false && $headline_end !== false) {

				$headline_length = $headline_end - $headline_start;
				$headline = substr($html, $headline_start, $headline_end);

				$new_headline = preg_replace( '|([^\s])\s+([^\s]+)\s*$|', '$1&nbsp;$2', $headline);

				$html = str_replace($headline, $new_headline, $html);

			}

			return $html;

		}

		/**
		*
		* This PHP Function generates a meta description string at
		* least 80 characters long, ending with a period if possible, and no
		* more than 156 characters. The output of this function should probably
		* be html encoded with a function such as htmlspecialchars() for use
		* in a web page.
		*
		**/

		public function generateDescription($page) {


			$page = wire('page');

			if (!empty($page->seo->description)) {
				return $page->seo->description;
			}

			$html = $page->body;

			if ($html === '' || empty($html)) {
				// Default description
				return $page->title;

			} else {

				$html 		= substr($html, strpos($html, "<p>"));

			  // Remove all html entities
			  $html 		= preg_replace("/&#?[a-z0-9]+;/i","",$html);

			  // Filter html tags & new lines
				$html 		= strip_tags($html);
				$html 		= str_replace(PHP_EOL, ' ', $html);
				$html 		= html_entity_decode($html);

				// Filter output to meta specs.
			  $html 		= substr($html, 0, 150);

			  $remove 	= substr(strrchr($html, " "), 1);
			  $html 		= rtrim($html, $remove);
			  $html     = trim($html);

			  return $html . "...";

			  // Return sentence or max words.
			  if (substr($html, -1) == '.') {
			  	return $html . "...";
			  } else {
			  	if (preg_match("/^.{1,155}\b/s", $html, $match)) {
					  return $match[0];
			  	} else {
			  		// Return html if no regex match was found;
			  		return "No description could be generated!";
			  	}
			  }
			}
		}

		/**
		*
		* Title Function
		*
		* Order of Precedence:
		* --------------------
		* 1. SEO Title - SEO field must be filled out.
		* 2. Title | Company - Company must be present & both must be less than 150 char combined.
		* 3. Title - If SEO Title or Company not available output just the page title.
		*
		**/

		public function generateTitle($page) {

			$pages = wire('pages');

			if (!empty($page->seo->title)) {
				return $page->seo->title;
			}

			$title   = $page->title;
			$company = $pages->get('/settings/')->company;

			if (!empty($company) && strlen($title) <= 55) {
				return $title . " | " . $company;
			} else {
				return $title;
			}

		}


		/**
		*
		* Robots Function
		*
		* Generates the appropriate robots based on the site mode.
		*
		**/

		public function generateRobots($page) {

			$pages    = wire('pages');
			$settings = $pages->get('/settings/');
			$status   = $settings->live;

			if ($status->value === "dev" || $page->sitemap_ignore == 1) {
				return "NOINDEX NOFOLLOW NOARCHIVE";
			} else if ($status->value === "live") {
				return "INDEX FOLLOW ARCHIVE";
			} else {
				return "Site mode not selected or misconfigured under settings";
			}

		}

		/**
		*
		* Company Name Function
		*
		* Used across the site in various locations including meta tags.
		*
		**/

		public function generateCompany() {

			$pages   = wire('pages');
			$company = $pages->get('/settings/')->company;

			if (!empty($company)) {
				return $company;
			} else {
				return "No company name defined in settings";
			}

		}

		/**
		*
		* Canonical display method
		*
		* Order of Precedence:
		* --------------------
		* 1. Page SEO Canonical URL - On a per page basis, seo canonical must be entered.
		* 2. Settings Canonical - Sitewide Canonical URL. this must be filled out.
		* 3. Page URL - Current page HTTP URL if none of the above available.
		**/

		public function generateCanonical($page) {

			$sanitizer = wire('sanitizer');
			$settings =  wire('pages')->get('/settings/');

			if (!(filter_var($sanitizer->url($page->seo_canonical), FILTER_VALIDATE_URL) === FALSE)) {
				return $page->seo_canonical;
			} elseif (!(filter_var($sanitizer->url($settings->canonical . $page->url), FILTER_VALIDATE_URL) === FALSE)) {
				return $settings->canonical . $page->url;
			} else {
				return $page->httpURL;
			}
		}

		/**
		*
		* This function returns the video ID from a given a YouTube URL
		*
		**/

		public function getYouTubeVideoID($url) {
			$regEx = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";

			if (preg_match($regEx, $url, $matches)) {
				return $matches[1];
			} else {
				return false;
			}
		}

	}

	$cms = new customFunctions();

?>