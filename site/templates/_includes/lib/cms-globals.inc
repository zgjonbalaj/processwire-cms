<?

	namespace ProcessWire;

	// Get home page page as $site
	$site 			= $pages->get('/');

	// Get site settings page as $settings
	$settings		= $pages->get('/settings/');

	// Get templates/_assets dir as $assets
	$assets			= $config->urls->templates . '_assets';


?>