<? if ($notifyJs): ?>
	<script>
		$(window).load(function() {
		  $.notify("<?= $notifyMsg[0] ?>", {
			// whether to hide the notification on click
			clickToHide: true,
			// whether to auto-hide the notification
			autoHide: true,
			// if autoHide, hide after milliseconds
			autoHideDelay: 6000,
			// show the arrow pointing at the element
			arrowShow: true,
			// arrow size in pixels
			arrowSize: 8,
			// default positions
			elementPosition: 'bottom left',
			globalPosition: 'top center',
			// default style
			style: 'bootstrap',
			// default class (string or [string])
			className: '<?= $notifyMsg[1] ?>',
			// show animation
			showAnimation: 'slideDown',
			// show animation duration
			showDuration: 400,
			// hide animation
			hideAnimation: 'slideUp',
			// hide animation duration
			hideDuration: 800,
			// padding between element and notification
			gap: 3
		  });
		});
	</script>
<? endif ?>