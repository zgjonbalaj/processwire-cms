<?

# Include DOC Header
require("_includes/header/doc-header.inc");

?>
	<body id="pg-<?= $page->id ?>">

		<!-- Include Header -->
		<? require("_includes/header/header.inc"); ?>

		<div class="oc oc-content">
			<div class="oc oc-overlay"></div>

			<section class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>
							Oops!<br>
							<small>Page Not Found</small>
						</h1>
						<p>The page you have requested cannot be found. If you believe you have receieved this message in error please contact our administrator.</p>
					</div>
				</div>
			</section>

		</div>

		<!-- Footer -->
		<? require("_includes/footer/footer.inc"); ?>

	</body>
</html>