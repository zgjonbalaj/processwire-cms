<?php

/**
 * ProcessWire Ready File
 *
 * This file is included immediately after the API is fully ready. It behaves
 * the same as a ready() method in an autoload module. The current $page has
 * been determined, but not yet rendered. This is an excellent place to attach
 * hooks that may need to know something about the current page. It's also an
 * excellent place to include additional classes or libraries that will be used
 * on all pages in your site.
 *
 */