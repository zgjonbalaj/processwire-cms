# --- WireDatabaseBackup {"time":"2017-03-06 16:40:54","user":"admin","dbName":"processwire","description":"backup made by CronjobDatabaseBackup","tables":[],"excludeTables":[],"excludeCreateTables":[],"excludeExportTables":[]}

DROP TABLE IF EXISTS `ProcessRedirects`;
CREATE TABLE `ProcessRedirects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `counter` int(10) unsigned DEFAULT '0',
  `redirect_from` varchar(255) NOT NULL DEFAULT '',
  `redirect_to` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `redirect_from` (`redirect_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.wire/modules/', 'AdminTheme/AdminThemeDefault/AdminThemeDefault.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nFieldtype/FieldtypeCache.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypeDatetime.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypeURL.module\nFileCompilerTags.module\nImageSizerEngineIMagick.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldHidden.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldName.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldText.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldURL.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryMagnific/JqueryMagnific.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryUI/JqueryUI.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageSupport.module\nLanguageSupport/LanguageSupportFields.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/ProcessLanguage.module\nLanguageSupport/ProcessLanguageTranslator.module\nLazyCron.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupCache.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupPageFields.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupRSS.module\nPage/PageFrontEdit/PageFrontEdit.module\nPagePathHistory.module\nPagePaths.module\nPagePermissions.module\nPageRender.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessHome.module\nProcess/ProcessList.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessLogin/ProcessLogin.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessPageClone.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessPageSort.module\nProcess/ProcessPageTrash.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessPageView.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessProfile/ProcessProfile.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessUser/ProcessUser.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemUpdater/SystemUpdater.module\nTextformatter/TextformatterEntities.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterNewlineUL.module\nTextformatter/TextformatterPstripper.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterStripTags.module', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVersions.info', '{\"208\":2,\"129\":119,\"156\":104}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.site/modules/', 'BatchChildEditor/BatchChildEditor.module\nBatchChildEditor/ProcessChildrenCsvExport.module\nCronjobDatabaseBackup/CronjobDatabaseBackup.module\nEditorSkinLightwire/EditorSkinLightwire.module\nFieldtypeMultiplier/FieldtypeMultiplier.module\nFieldtypeMultiplier/InputfieldMultiplier.module\nFieldtypeTable/FieldtypeTable.module\nFieldtypeTable/InputfieldTable.module\nFieldtypeTextareas/FieldtypeTextareas.module\nFieldtypeTextareas/InputfieldTextareas.module\nFormBuilder/FormBuilder.module\nFormBuilder/InputfieldFormBuilderFile.module\nFormBuilder/ProcessFormBuilder.module\nJqueryDataTables/JqueryDataTables.module\nMarkupSEO/MarkupSEO.module\nMarkupSitemapXML/MarkupSitemapXML.module\nModulesManager/ModulesManager.module\nModulesManager/ModulesManagerNotification.module\nPageReferencesTab/PageReferencesTab.module\nProCache/ProCache.module\nProCache/ProcessProCache.module\nProcessChangelog/ProcessChangelog.module\nProcessChangelog/ProcessChangelogHooks.module\nProcessChangelog/ProcessChangelogRSS.module\nProcessDatabaseBackups/ProcessDatabaseBackups.module\nProcessDiagnostics/DiagnoseDatabase.module\nProcessDiagnostics/DiagnoseFiles.module\nProcessDiagnostics/DiagnoseImagehandling.module\nProcessDiagnostics/DiagnosePhp.module\nProcessDiagnostics/DiagnoseWebserver.module\nProcessDiagnostics/ProcessDiagnostics.module\nProcessPageCloneAdaptUrls/ProcessPageCloneAdaptUrls.module\nProcessRedirects/ProcessRedirects.module\nProcessWireConfig/ProcessWireConfig.module.php\nProcessWireUpgrade/ProcessWireUpgrade.module\nProcessWireUpgrade/ProcessWireUpgradeCheck.module\nProDrafts/ProcessProDrafts.module\nProDrafts/ProDrafts.module\nProtectedMode/ProtectedMode.module\nTextformatterAutoLinks/TextformatterAutoLinks.module', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__159385c34a9bc5c80bb3dcde579250ae', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProCache\\/ProCache.module\",\"hash\":\"07c84c7b679fe93e9a1bd292dfd76d0d\",\"size\":53270,\"time\":1459188898,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProCache\\/ProCache.module\",\"hash\":\"c77915adef69c04d9271be46f0840601\",\"size\":54515,\"time\":1459188898}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__111c37561470d9ccf71e2fa3bcfc99ef', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProDraftsHooks.php\",\"hash\":\"92930b74fd64952a6eeb7450fe543bc1\",\"size\":23506,\"time\":1456322180,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProDraftsHooks.php\",\"hash\":\"1ebf4c459a2dc19d2a1924cab62bf2fd\",\"size\":24680,\"time\":1456322180}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a5a02585d4caf0123c0375fcf462b59d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProDraft.php\",\"hash\":\"f98d19b376bdee5f2cf70b4fdf3435dd\",\"size\":21052,\"time\":1456325308,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProDraft.php\",\"hash\":\"3404e04ab76cfb61884b59e9d41952c6\",\"size\":21234,\"time\":1456325308}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ProDrafts.disallowFields', '[]', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e49b043e4c0708abdb8cc2c64696f363', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeMultiplier\\/MultiplierArray.php\",\"hash\":\"93cba68a3bd69500a1ae8a07b820472f\",\"size\":2250,\"time\":1448796316,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeMultiplier\\/MultiplierArray.php\",\"hash\":\"f91927e84aad66676a1fae813a013038\",\"size\":2276,\"time\":1448796316}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__621956062438e63d8586b5dd179f05ea', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeMultiplier\\/InputfieldMultiplier.module\",\"hash\":\"2c4d4d9dd05e21b0925005966208a218\",\"size\":10558,\"time\":1448810406,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeMultiplier\\/InputfieldMultiplier.module\",\"hash\":\"38d9c1fa8c5c493d855bb30d9d68e59e\",\"size\":11035,\"time\":1448810406}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c684d14b7c1050ce9c7a84a2f66c10c9', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeMultiplier\\/InputfieldMultiplierBase.php\",\"hash\":\"ea66aa4e44003b845fd7d2e6b8eaa3e6\",\"size\":3541,\"time\":1448810268,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeMultiplier\\/InputfieldMultiplierBase.php\",\"hash\":\"57c12a1336ef9bde98fab74bbbda2699\",\"size\":3645,\"time\":1448810268}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2cf60c420389b22dd23e7c3cdf594483', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTable\\/FieldtypeTable.module\",\"hash\":\"87c7875db49538527fa82fa1a6160d93\",\"size\":76172,\"time\":1469793584,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTable\\/FieldtypeTable.module\",\"hash\":\"e1aa0d8e1a036e378d2e4dc731bdb279\",\"size\":78549,\"time\":1469793584}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__59076dbb59e5940a9f133f350ce8715a', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTable\\/InputfieldTable.module\",\"hash\":\"42d16fb0e50df131a2b186e3bc353fa0\",\"size\":33481,\"time\":1469208266,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTable\\/InputfieldTable.module\",\"hash\":\"d8a861f8fa9079ada5efd98db297dbb8\",\"size\":33951,\"time\":1469208266}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b872b10df7525abbb2335941ee46494c', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTextareas\\/FieldtypeTextareas.module\",\"hash\":\"8a44a1b5397e3b23cde28b620f1b969f\",\"size\":35312,\"time\":1438783684,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTextareas\\/FieldtypeTextareas.module\",\"hash\":\"d84fe70fc314ef229d8032adc934d9d2\",\"size\":36497,\"time\":1438783684}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5b52d8049a9791e4396303bc5a17efe6', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTextareas\\/InputfieldTextareas.module\",\"hash\":\"70e96301dc861dbe166178614d797a75\",\"size\":8582,\"time\":1438863586,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTextareas\\/InputfieldTextareas.module\",\"hash\":\"fb1046dd58ff748d39711374045f5b44\",\"size\":8886,\"time\":1438863586}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2864c7f50c6433274ef1f94de422b78d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/FormBuilderConfig.php\",\"hash\":\"3ebae51f94053491372e6c49db2412a1\",\"size\":12726,\"time\":1444299042,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderConfig.php\",\"hash\":\"e3498f5b9d50a4d3cd3581e995553d46\",\"size\":13598,\"time\":1444299042}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__36806cb533e90a71890544042465f429', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilderFile.module\",\"hash\":\"b902e55baa146640dede406d33ab1247\",\"size\":12258,\"time\":1459447042,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilderFile.module\",\"hash\":\"dcec6f43f7557754c58018861d6c4578\",\"size\":12526,\"time\":1459447042}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__aade3a765051d2dba4d83bd1eeaeb45d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilder.php\",\"hash\":\"2bec87ab53922ca5796c9fbfebc350ed\",\"size\":886,\"time\":1458570766,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/InputfieldFormBuilder.php\",\"hash\":\"a3059d055e260c712414e546cd9ed29d\",\"size\":899,\"time\":1458570766}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__91dc9b461e20bc22c11c6ae61e7633ee', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/BatchChildEditor\\/BatchChildEditor.module\",\"hash\":\"072567297fa09a68900fc3abed1534a8\",\"size\":145230,\"time\":1488825313,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/BatchChildEditor\\/BatchChildEditor.module\",\"hash\":\"0c9b36ae1265b0badffe024d7983e5e6\",\"size\":150216,\"time\":1488825313}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bdd67e1dac62c407d9e0cf21e161b2d4', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/CronjobDatabaseBackup\\/CronjobDatabaseBackup.module\",\"hash\":\"8f7b16ea2c3eba65e1c33a4c8a369bc2\",\"size\":15124,\"time\":1480429812,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/CronjobDatabaseBackup\\/CronjobDatabaseBackup.module\",\"hash\":\"54e333cd4848be2bdde5f582aa44634f\",\"size\":15969,\"time\":1480429812}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__04bbca7c8e9830e2b55eebf5567025ee', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/FormBuilder.module\",\"hash\":\"5de5477736598b29ab1aedb9ef4b6b95\",\"size\":18514,\"time\":1459446154,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilder.module\",\"hash\":\"13cdfbda5636486bf1ceee6bcc3d1ad3\",\"size\":19711,\"time\":1459446154}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1812728000d1873c8ba2b965bedfac95', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/FormBuilderMain.php\",\"hash\":\"9184c857703876e29af4bab622e196dc\",\"size\":21933,\"time\":1458640320,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/FormBuilderMain.php\",\"hash\":\"06de9e7485fd497215f4fae195a7bbd1\",\"size\":23250,\"time\":1458640320}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b6b3020a0a058794c51875bba036b23a', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilder.module\",\"hash\":\"bc7d7edd8f87cb049eaa7e3d9e984556\",\"size\":104002,\"time\":1459446154,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FormBuilder\\/ProcessFormBuilder.module\",\"hash\":\"4da38e8d6200a2e5e094c587f9b575ef\",\"size\":105761,\"time\":1459446154}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__10e2c86b21d7fb0be5d2f1e6d448f0c5', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/JqueryDataTables\\/JqueryDataTables.module\",\"hash\":\"57f0a29fcca2df924059df00805df09c\",\"size\":971,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/JqueryDataTables\\/JqueryDataTables.module\",\"hash\":\"e6ae9a8ab7716c8cae8cc7ec3cda2473\",\"size\":984,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a6829f8b233dee31ac9820c5cfdbde17', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ModulesManager\\/ModulesManager.module\",\"hash\":\"8d0c632945794a3f33282e11400039f3\",\"size\":31359,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ModulesManager\\/ModulesManager.module\",\"hash\":\"c8b252bd458d440ebded0a70760fe38a\",\"size\":31788,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__dc706ef212890573e45a146c823cf4c3', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProCache\\/ProcessProCache.module\",\"hash\":\"a05b631263366c0e87d40513600faae0\",\"size\":52424,\"time\":1459178882,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProCache\\/ProcessProCache.module\",\"hash\":\"173c0e756eb226fb0ae991b97a5be966\",\"size\":53207,\"time\":1459178882}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__be13ded73d60d6184c90782f564cfb0d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessBatcher\\/ProcessBatcher.module\",\"hash\":\"baca8c4d58d5bc922e0f996a67c04e0d\",\"size\":26711,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessBatcher\\/ProcessBatcher.module\",\"hash\":\"416dd3f060faf2ec9ed15dc7ba5b6d65\",\"size\":27221,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2eadb7b6c3fefdf8e652b8a669fd6adb', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelog.module\",\"hash\":\"e9952f5b61db959ac65e4d106513564a\",\"size\":50046,\"time\":1485786115,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelog.module\",\"hash\":\"3596be59bcc0c895778464c99c72894d\",\"size\":51502,\"time\":1485786115}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__121eb772a5e69835b94843f5d873f4f0', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDatabaseBackups\\/ProcessDatabaseBackups.module\",\"hash\":\"e5dea11b1afb638b9a47edcb10eab399\",\"size\":12324,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDatabaseBackups\\/ProcessDatabaseBackups.module\",\"hash\":\"b60bbe3016c7d47159d299c1b87ae4e6\",\"size\":12441,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__44e194f2ade00742526b7d20146e55e8', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseDatabase.module\",\"hash\":\"e1e7a8f3461f1631c0136a77c181c82d\",\"size\":9542,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseDatabase.module\",\"hash\":\"7e7cffca946afb3931aeed3bc3740233\",\"size\":9788,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d79e2554bb2d579c1ac217886b8eccc5', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseFiles.module\",\"hash\":\"c0bbb0d9ddb27f412461f26cd1a1ef11\",\"size\":6110,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseFiles.module\",\"hash\":\"db51a0e754924fc49cdd295e40aa651a\",\"size\":6350,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c334878ac90d111704e2b4a77b2b247d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseImagehandling.module\",\"hash\":\"37e6100f7bb8e8fc5fc83cb5cf371fa3\",\"size\":6833,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__eff0fe729a50d245f8b83bdabf911f53', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/DiagnosePhp.module\",\"hash\":\"b36f04d3ae3e0a63753a40f40d68035d\",\"size\":8319,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/DiagnosePhp.module\",\"hash\":\"5f4171adcc66835ef6796360de50aeff\",\"size\":8555,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__70eec67bc78ae547a5e5a1ca97a1aa8a', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseWebserver.module\",\"hash\":\"b9ce60853b455abb2b3217b5e68a0f86\",\"size\":4425,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/DiagnoseWebserver.module\",\"hash\":\"e5e964d8761089c69fdcb20bca936f12\",\"size\":4673,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__270bd61e8570d529a93d09a5283a660e', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessDiagnostics\\/ProcessDiagnostics.module\",\"hash\":\"b43482fb5fccf513a2ca26a09b64af74\",\"size\":10814,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessDiagnostics\\/ProcessDiagnostics.module\",\"hash\":\"c79c7d04acd4aff010ca91b439714377\",\"size\":11077,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7e74bb7d27be044ad78fc8142ac88935', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessWireConfig\\/ProcessWireConfig.config.php\",\"hash\":\"08595824e2b40f34b542a33a461120c7\",\"size\":2198,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireConfig\\/ProcessWireConfig.config.php\",\"hash\":\"21fba1097a9010d6d5c1c186a360aa7d\",\"size\":2211,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a8ba3b771e444353e6504ba66313aef2', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"66cc6ed58e83f659bc4a51665b4d2d83\",\"size\":26751,\"time\":1475605025,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgrade.module\",\"hash\":\"e755d22d24df133c8b6ce26f22e281c1\",\"size\":27050,\"time\":1475605025}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f7db3dd485c59246f4f3d116f9b6b792', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"c3c743773b0bce19a80fe6d3a7a8f516\",\"size\":622,\"time\":1475605025,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.config.php\",\"hash\":\"2a9c49f29d6273cd1cf98c764aae5530\",\"size\":635,\"time\":1475605025}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fd572eef2df688318afe6ef997f9ce0d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/MarkupSEO\\/MarkupSEO.module\",\"hash\":\"496d0cff410ea4f08b474c8402ab6f8b\",\"size\":38243,\"time\":1472061755,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSEO\\/MarkupSEO.module\",\"hash\":\"f3476cecb06295c27a645f1ac2a3aabf\",\"size\":39764,\"time\":1472061755}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9d02faf5d6904ea26620df9227d52f2d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"7180d16180a51cd2565232dabcdbcebd\",\"size\":4996,\"time\":1486410402,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/MarkupSitemapXML\\/MarkupSitemapXML.module\",\"hash\":\"3cffc1f2e592241702db4991f3cf8b7e\",\"size\":5204,\"time\":1486410402}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5daca4de02469765594d4de404382ed8', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/PageReferencesTab\\/PageReferencesTab.module\",\"hash\":\"dc4bd4e9653ab8c400b61a40c42748d6\",\"size\":6258,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/PageReferencesTab\\/PageReferencesTab.module\",\"hash\":\"0424e09f6969d448ff6c0e90b6217661\",\"size\":6809,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__df37f9ff18cc251935b6c160c7cc9dc3', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProcessProDrafts.module\",\"hash\":\"3af0df7465ce2cec6a817ce72b64e7a1\",\"size\":26210,\"time\":1456410500,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProcessProDrafts.module\",\"hash\":\"cae14151544dc93a50834a130c59aafe\",\"size\":26943,\"time\":1456410500}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__00e5c6269eca00911f67f7d1c94b8a23', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProDrafts.config.php\",\"hash\":\"88fa9218ae11b3b41fb3ba7e6347a809\",\"size\":9689,\"time\":1456414390,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProDrafts.config.php\",\"hash\":\"a7bbab2a666ea246dcab45350c96ce73\",\"size\":9819,\"time\":1456414390}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__4ab9c705918944a3cdd78f7b17ccf984', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TextformatterAutoLinks\\/TextformatterAutoLinks.module\",\"hash\":\"5890d4c04999cbda7511ef29aa945337\",\"size\":15019,\"time\":1413543638,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TextformatterAutoLinks\\/TextformatterAutoLinks.module\",\"hash\":\"59f74d91d5230006929ff5b17709314e\",\"size\":15756,\"time\":1413543638}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9b8eeafb2d1e7c2bf047c0c77da89709', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ModulesManager\\/ModulesManagerNotification.module\",\"hash\":\"4b6232793bd700912f196418688b5356\",\"size\":7226,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ModulesManager\\/ModulesManagerNotification.module\",\"hash\":\"bd3cbe7bc2116ab28ad912c754de8095\",\"size\":7473,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9e15982c53533d7f891cdd05a63e9656', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelogRSS.module\",\"hash\":\"7608b384d1619a327c5c6b0ce998b3e6\",\"size\":4039,\"time\":1485786115,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelogRSS.module\",\"hash\":\"b8a326fd0df880bde1904c54f998d01f\",\"size\":4260,\"time\":1485786115}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d1b49b60bbb2e9200771731e9d6a7e6c', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/CssPanel.inc\",\"hash\":\"5a32a6238fb33efcb79119e804630940\",\"size\":12449,\"time\":1478875047,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/CssPanel.inc\",\"hash\":\"5a32a6238fb33efcb79119e804630940\",\"size\":12449,\"time\":1478875047}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d1dc8258cd6c09f918ea9949ed471200', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/ProcesswireInfoPanel.inc\",\"hash\":\"7ac1aa0d053e4691f4d61328d567378d\",\"size\":68420,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/ProcesswireInfoPanel.inc\",\"hash\":\"7bc66b6e3287e410a3a15adffb39b63d\",\"size\":69044,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2b48c155952b26d961b00c4f583db897', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageTableToRepeaterMatrix.action.php\",\"hash\":\"f17f7f153c59090fafee4785a8fa557d\",\"size\":17228,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageTableToRepeaterMatrix.action.php\",\"hash\":\"1406dae699bc16b6ae2044cb7033e7e3\",\"size\":17306,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bf8043e81adabe73857bebd2b7f6ba76', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/TemplateFieldsBatcher.action.php\",\"hash\":\"19dbfc70b8f4d9d42e3c3d3c74d8f39f\",\"size\":2824,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/TemplateFieldsBatcher.action.php\",\"hash\":\"87912673faa8ff2e1b5f502ba498ebc1\",\"size\":2902,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0aad00a9874c70814aa6042fbf25ff16', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/TemplateRolesBatcher.action.php\",\"hash\":\"2e3c0d8f49800ee0f100af3e51d22513\",\"size\":5521,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/TemplateRolesBatcher.action.php\",\"hash\":\"65128a3efc5595712151544846b27eec\",\"size\":5534,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__41e87ff3631d49ef5f44d3c50e34f489', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/UserRolesPermissionsBatcher.action.php\",\"hash\":\"1381e2b0497e873e083ac6f93f4e1934\",\"size\":3947,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/UserRolesPermissionsBatcher.action.php\",\"hash\":\"1381e2b0497e873e083ac6f93f4e1934\",\"size\":3947,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesUninstalled.info', '{\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1482426231,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":107,\"versionStr\":\"1.0.7\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1482426231,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTitleLanguage\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeTextareaLanguage\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeTextLanguage\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Field that stores a single line of text in multiple languages\",\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageSupport\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"versionStr\":\"1.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"ProcessWire multi-language support.\",\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1482426231,\"installed\":false,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"addFlag\":32},\"LanguageSupportFields\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Required to use multi-language fields.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageSupportPageNames\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":9,\"versionStr\":\"0.0.9\",\"author\":\"Ryan Cramer\",\"summary\":\"Required to use multi-language page names.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1482426231,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LanguageTabs\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"versionStr\":\"1.1.4\",\"author\":\"adamspruijt, ryan\",\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1482426231,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessLanguage\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"versionStr\":\"1.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage system languages\",\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"},\"created\":1482426231,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"useNavJSON\":true},\"ProcessLanguageTranslator\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"versionStr\":\"1.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"created\":1482426231,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1482426232,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1482426232,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1482426232,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":6,\"versionStr\":\"0.0.6\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1482426232,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"TextformatterMarkdownExtra\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"versionStr\":\"1.3.0\",\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"created\":1482426232,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1482426232,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1482426232,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1482426232,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1482426232,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1482426232,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ModulesManagerNotification\":{\"name\":\"ModulesManagerNotification\",\"title\":\"Modules Manager Notification\",\"version\":11,\"versionStr\":\"0.1.1\",\"author\":\"Philipp \'Soma\' Urlich\",\"summary\":\"Send email notification to specified email address with installed modules updates available.\",\"requiresVersions\":{\"LazyCron\":[\">=\",0],\"ModulesManager\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1457642155,\"installed\":false,\"configurable\":true,\"namespace\":\"\\\\\"},\"ProcessChangelogRSS\":{\"name\":\"ProcessChangelogRSS\",\"title\":\"Changelog RSS\",\"version\":\"1.0.0\",\"versionStr\":\"1.0.0\",\"author\":\"Teppo Koivula\",\"summary\":\"Output an RSS feed from Process Changelog data\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/process-changelog\\/\",\"icon\":\"rss\",\"requiresVersions\":{\"ProcessChangelog\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1485786115,\"installed\":false,\"configurable\":true,\"namespace\":\"\\\\\"}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1cac7a3e26b819fc769f9ff2a166962c', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTable\\/TableRow.php\",\"hash\":\"d47a40a433d6adaf1a41e1313e79a41d\",\"size\":1671,\"time\":1468500374,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTable\\/TableRow.php\",\"hash\":\"63e845058b9440ac9127923e7df5cfeb\",\"size\":1697,\"time\":1468500374}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9d1329e7fcc1229f468b469318a4c082', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTable\\/TableRows.php\",\"hash\":\"6ca7576af49fad5e60f0f0c5c9a67294\",\"size\":10601,\"time\":1469105368,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTable\\/TableRows.php\",\"hash\":\"0eecbcb616d481f364b93161848ba15f\",\"size\":10692,\"time\":1469105368}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d81526ea60123174e578b8b99fd7c11c', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeTextareas\\/TextareasData.php\",\"hash\":\"1df76b1e21436013554897d2df833eed\",\"size\":4171,\"time\":1425308942,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeTextareas\\/TextareasData.php\",\"hash\":\"0c2516d86f582f60e0a534fcb33bdc06\",\"size\":4249,\"time\":1425308942}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__a38d8ae4b537947f86fd6aa695d2ad69', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/BatchChildEditor\\/ProcessChildrenCsvExport.module\",\"hash\":\"aeccf8a877ff4f12afc5c26365372633\",\"size\":11570,\"time\":1488825313,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/BatchChildEditor\\/ProcessChildrenCsvExport.module\",\"hash\":\"1234e6efe181895849b4d44df2ab3bb2\",\"size\":12246,\"time\":1488825313}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__64db595ef2732bca1078361bc44593bd', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/FieldtypeMultiplier\\/FieldtypeMultiplier.module\",\"hash\":\"c2e37d97d7cac9e0577d23a84ee23552\",\"size\":10933,\"time\":1448810292,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeMultiplier\\/FieldtypeMultiplier.module\",\"hash\":\"f8988b2f482c8259ce823115f80653b6\",\"size\":11707,\"time\":1448810292}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1ed13a1ce3bf6d95b7ade1ea8bdc5ac4', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/templates\\/admin.php\",\"hash\":\"e82aa1bcb8bb7493a8ed511325198232\",\"size\":445,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"ccf26bf80c613371852662ba01b0a885\",\"size\":565,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__20564ad6220123908d2a8653fef31d20', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/tracy.php\",\"hash\":\"9e38a388e52571fe8debe480d77ab704\",\"size\":607,\"time\":1483715480,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/tracy.php\",\"hash\":\"24a17a050265c9f8900aa91c282dd075\",\"size\":3055,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bf7e8e68101909052a4972fe66e8f636', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Bar.php\",\"hash\":\"c4f59641f5b962ca6c37d6fc399c1eaf\",\"size\":5949,\"time\":1483715480,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Bar.php\",\"hash\":\"92cb555b8dfcb0ef3bade984140ee8a7\",\"size\":7268,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__18001b988d17eeda186b9712b51df87e', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/BlueScreen.php\",\"hash\":\"cabb7d1fe11e05356a060a1a7b9d4b70\",\"size\":6895,\"time\":1483715480,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/BlueScreen.php\",\"hash\":\"9da6bd6fb1786af56de584cb09059eba\",\"size\":7451,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8a0d876e89f764b91c22bc48cc4ede67', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/DefaultBarPanel.php\",\"hash\":\"c7011b83f32cf68e721588c7a0013295\",\"size\":896,\"time\":1483715480,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/DefaultBarPanel.php\",\"hash\":\"d10d4830a6f3add866c56ef6b5e1e6b5\",\"size\":1398,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__8e0338d4ec7bfaf944783fad26344755', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Debugger.php\",\"hash\":\"d68ad340af6d3a2036cefcdc0bd50636\",\"size\":16534,\"time\":1483715480,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Debugger.php\",\"hash\":\"6e4ca766303d22a539bebd65a34b0ec2\",\"size\":16740,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b2d034546f21ee93e831d28484b3d608', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/TDClass.inc\",\"hash\":\"1e7da1a52b25e6cb9c710ec207634451\",\"size\":4534,\"time\":1483715480,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/TDClass.inc\",\"hash\":\"8e916f08a51c68430df46507db929f67\",\"size\":4573,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__90a5e73e13e6609bcab5fd7d3d2569ef', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelogHooks.module\",\"hash\":\"a768f3fa1266110bba02e9a085c22fcc\",\"size\":21020,\"time\":1485786115,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessChangelog\\/ProcessChangelogHooks.module\",\"hash\":\"531a9758c6486bed177cf7d2a64b4e44\",\"size\":21618,\"time\":1485786115}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__078a7a82bcbd7eb50fa3dd0b0242b924', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessPageCloneAdaptUrls\\/ProcessPageCloneAdaptUrls.module\",\"hash\":\"a9c1aa7c46ee2fd0c472ebbcd36acd2b\",\"size\":2055,\"time\":1458760221,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessPageCloneAdaptUrls\\/ProcessPageCloneAdaptUrls.module\",\"hash\":\"0c00419350ca26c062b34db5778dc9aa\",\"size\":2107,\"time\":1458760221}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__40d744301dd3b6c278b96efb7f598ad1', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessRedirects\\/ProcessRedirects.module\",\"hash\":\"433312110faf70c371ecc2e916ce23d1\",\"size\":10418,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessRedirects\\/ProcessRedirects.module\",\"hash\":\"64ebdbcf073289faa76cbcc7555f25f0\",\"size\":10483,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fe023a0ca1fbb8220b5a871e85c36f9d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"1f30f3a328cbd2d9b9ceeb9e7cb0ab9e\",\"size\":11162,\"time\":1475605025,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireUpgrade\\/ProcessWireUpgradeCheck.module\",\"hash\":\"967631030b36c79030d70a146b00afd5\",\"size\":11253,\"time\":1475605025}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__da5eba37b0a1c6871d4930deab2d3cd9', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProDrafts.module\",\"hash\":\"7f19f1d25f0b1029bfffe3af98259f5b\",\"size\":39113,\"time\":1457876876,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProDrafts.module\",\"hash\":\"292179ee3fe0baf77fc63c8ea9d59563\",\"size\":40103,\"time\":1457876876}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bb98221ef544c7830c8785803feef0be', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProtectedMode\\/ProtectedMode.module\",\"hash\":\"b17606312b99bcc795239bb35d40dc01\",\"size\":14268,\"time\":1478537551,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProtectedMode\\/ProtectedMode.module\",\"hash\":\"648f9ece165661e89130f2a9d20c6bfd\",\"size\":14853,\"time\":1478537551}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__acdbede0b53df2dea389c147ff4292f8', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/TracyDebugger.module\",\"hash\":\"4ecb2af3e0a8489587ebdb23a32c2519\",\"size\":87501,\"time\":1483715480,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/TracyDebugger.module\",\"hash\":\"cf4385aa0d6660a63df118e7f7cf978f\",\"size\":101909,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__84a6bc4d056b888ac4329f2f4732a489', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/IBarPanel.php\",\"hash\":\"359a1c87b41b02986bed8565584f9577\",\"size\":398,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/IBarPanel.php\",\"hash\":\"359a1c87b41b02986bed8565584f9577\",\"size\":398,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__46e3e8efc98dceac143ce02064fe682e', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Dumper.php\",\"hash\":\"0804b284e9e487510b9d0180b734f2f2\",\"size\":17175,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Dumper.php\",\"hash\":\"0804b284e9e487510b9d0180b734f2f2\",\"size\":17175,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3a7c9dbd7d3e84da27a0b091bd535e61', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/ILogger.php\",\"hash\":\"ee0e689788f1fd2bba7f9c027a2eceb0\",\"size\":381,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/ILogger.php\",\"hash\":\"ee0e689788f1fd2bba7f9c027a2eceb0\",\"size\":381,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b293314c9d972378cc85406eaefdc7c2', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/FireLogger.php\",\"hash\":\"e131039dc16f72e1caa1b6c77270640f\",\"size\":4678,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/FireLogger.php\",\"hash\":\"e131039dc16f72e1caa1b6c77270640f\",\"size\":4678,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f176ed24fcc72eab956520099a563bf1', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Helpers.php\",\"hash\":\"2a653aa631106247d9cfd4f29f2f65aa\",\"size\":7126,\"time\":1483715480,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Helpers.php\",\"hash\":\"2a653aa631106247d9cfd4f29f2f65aa\",\"size\":7126,\"time\":1483715480}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f678e17c4371678fe530f82d0f031cde', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Logger.php\",\"hash\":\"2f49ea5ff7f3db8f551fc3eabbeb466e\",\"size\":5848,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/Logger.php\",\"hash\":\"2f49ea5ff7f3db8f551fc3eabbeb466e\",\"size\":5848,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9bfbdd98fa3317b6cb82640b409c181c', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/OutputDebugger.php\",\"hash\":\"ab950dcb4dc14016e4b7a1f3376acd51\",\"size\":1869,\"time\":1478537527,\"ns\":\"Tracy\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/Tracy\\/OutputDebugger.php\",\"hash\":\"ab950dcb4dc14016e4b7a1f3376acd51\",\"size\":1869,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__edb614310a5faa36c9116de4e7715255', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/shortcuts.php\",\"hash\":\"214451931f359047362be1ce905f46d1\",\"size\":555,\"time\":1478537527,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/tracy-master\\/src\\/shortcuts.php\",\"hash\":\"214451931f359047362be1ce905f46d1\",\"size\":555,\"time\":1478537527}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1c2fb7ff29af51faa61dbd5b1c3ea9ab', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/ShortcutMethods.inc\",\"hash\":\"7a64c06881fd0345f41d39cad3efff13\",\"size\":5594,\"time\":1478875047,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/ShortcutMethods.inc\",\"hash\":\"7a64c06881fd0345f41d39cad3efff13\",\"size\":5594,\"time\":1478875047}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__23921218c94b6609bcdb0f18a66ac76e', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProDrafts\\/ProDraftsPageEditorHooks.php\",\"hash\":\"aefc195f137bce346847016af43c1bf4\",\"size\":31752,\"time\":1457876794,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProDrafts\\/ProDraftsPageEditorHooks.php\",\"hash\":\"b4c2e5cf85392fc47c50e13ea7f73294\",\"size\":32236,\"time\":1457876794}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__411ddcb16926965ba8b25d85637d7190', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/ProcesswireLogsPanel.inc\",\"hash\":\"19f6367d9a52437216389c3244771ce6\",\"size\":5783,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/ProcesswireLogsPanel.inc\",\"hash\":\"f70495d75a53e07ca836f481aec4cdbd\",\"size\":5874,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1827beae2534ab8707d3f04fb1617353', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/TracyLogsPanel.inc\",\"hash\":\"19d4c7de26094e8d1e3b2073f4ade1a6\",\"size\":12415,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/TracyLogsPanel.inc\",\"hash\":\"2be83df4138bd8827555c90279f14eac\",\"size\":12545,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0b41fe760c8907c9ab2068cdc138f595', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/MethodsInfoPanel.inc\",\"hash\":\"aba59a7501dfe312cba7380668efea05\",\"size\":5468,\"time\":1478875047,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/MethodsInfoPanel.inc\",\"hash\":\"aba59a7501dfe312cba7380668efea05\",\"size\":5468,\"time\":1478875047}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b70acf44d4268c024786ee2384b9ff3d', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/DebugModePanel.inc\",\"hash\":\"16e7e99797c633c3414b7c98e76a41b9\",\"size\":27233,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/DebugModePanel.inc\",\"hash\":\"3dd38cfe23bf0a9e5dfbb83e9775ee1e\",\"size\":27779,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ae03c963fbc4e0a113e45d39d6dd6198', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/TemplatePathPanel.inc\",\"hash\":\"92dde57cf4366f3711a933af184a13ca\",\"size\":12476,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/TemplatePathPanel.inc\",\"hash\":\"76366b637fcf7592196a14c6c79c6ec1\",\"size\":12593,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c9e81524411429f28427cbc3c7e296c5', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/ConsolePanel.inc\",\"hash\":\"31296ab358a681cb0a354b0779d91af4\",\"size\":13253,\"time\":1478875047,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/ConsolePanel.inc\",\"hash\":\"1099d18ca8b291ea02c0842e6679f9e4\",\"size\":13370,\"time\":1478875047}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__702891c593b7ce1feccfda67c5b5770b', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/TemplateEditorPanel.inc\",\"hash\":\"5d542dc1fcf76484fafa7502da3be07c\",\"size\":10424,\"time\":1478875047,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/TemplateEditorPanel.inc\",\"hash\":\"cb2997c264fe28c6eb92039591f06b92\",\"size\":10515,\"time\":1478875047}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2a21317048946a624f359a92d5115064', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/TracyDebugger\\/PanelSelectorPanel.inc\",\"hash\":\"e32c4dc54ac75919eca7d2a6524d9c68\",\"size\":9915,\"time\":1480429953,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TracyDebugger\\/PanelSelectorPanel.inc\",\"hash\":\"bd5936c25221ec568ce72928a4405087\",\"size\":9993,\"time\":1480429953}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__479e68bf552fd0c3a94aa0c52d82581f', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/templates\\/home.php\",\"hash\":\"0e9c88bff1bcd2e903a418d13b47578b\",\"size\":572,\"time\":1486497486,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"6a1729e35dd842e4d8b721d7190fa91d\",\"size\":930,\"time\":1486497486}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3fe9c2c90c9058a9ad8ecfbc91872c0f', '{\"source\":{\"file\":\"_includes\\/header\\/doc-header.inc\",\"hash\":\"5e9b352b941c1b0e27ff6012e5f95a7f\",\"size\":1866,\"time\":1458759740,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/header\\/doc-header.inc\",\"hash\":\"8441eb43e70c5ad475ba9753f052d7ba\",\"size\":2223,\"time\":1458759740}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f60e52980361cd2f36ee384db3f31dca', '{\"source\":{\"file\":\"_includes\\/lib\\/cms-onload.inc\",\"hash\":\"d67d26446aedbb6750cccde42ca17932\",\"size\":2010,\"time\":1486409769,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/lib\\/cms-onload.inc\",\"hash\":\"d67d26446aedbb6750cccde42ca17932\",\"size\":2010,\"time\":1486409769}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3ec638721ab4e12a0e3559db2405f60a', '{\"source\":{\"file\":\"_includes\\/lib\\/cms-globals.inc\",\"hash\":\"f20e69aba1c4c9c83fdc1ca836afb45c\",\"size\":270,\"time\":1478538063,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/lib\\/cms-globals.inc\",\"hash\":\"f20e69aba1c4c9c83fdc1ca836afb45c\",\"size\":270,\"time\":1478538063}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ef21afb00cc710bb63069114d2bf6287', '{\"source\":{\"file\":\"_includes\\/lib\\/cms-functions.inc\",\"hash\":\"d2cf5131d3a4d2745ca9075992cd27b9\",\"size\":8708,\"time\":1478883724,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/lib\\/cms-functions.inc\",\"hash\":\"d2cf5131d3a4d2745ca9075992cd27b9\",\"size\":8708,\"time\":1478883724}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__21e585691ed69e6cca30b6ba34a624db', '{\"source\":{\"file\":\"_includes\\/header\\/header.inc\",\"hash\":\"3875789dcbd5228c4669e4092a8383c1\",\"size\":314,\"time\":1472066508,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/header\\/header.inc\",\"hash\":\"71f9cfed9790881a40f5ebd3ac73a612\",\"size\":433,\"time\":1472066508}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1fae619597bdd6afa4fc6f14330df677', '{\"source\":{\"file\":\"_includes\\/nav\\/main.inc\",\"hash\":\"4a7632ff61e5bc0919624efd1a1f3622\",\"size\":1743,\"time\":1486411179,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/nav\\/main.inc\",\"hash\":\"4a7632ff61e5bc0919624efd1a1f3622\",\"size\":1743,\"time\":1486411179}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5670012e6e7a5de3d1f6022de8749438', '{\"source\":{\"file\":\"_includes\\/footer\\/footer.inc\",\"hash\":\"47096434ca81cfd4687e0701be77f21e\",\"size\":211,\"time\":1483973035,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/footer\\/footer.inc\",\"hash\":\"8b96196f9af6e508eb964b02b3365094\",\"size\":447,\"time\":1483973035}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b9607df3221d70f9eede51e8cbb5a1ae', '{\"source\":{\"file\":\"_includes\\/nav\\/footer.inc\",\"hash\":\"d41d8cd98f00b204e9800998ecf8427e\",\"size\":0,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/nav\\/footer.inc\",\"hash\":\"d41d8cd98f00b204e9800998ecf8427e\",\"size\":0,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__61f8b01136f1373009d1323d0398d43e', '{\"source\":{\"file\":\"_includes\\/footer\\/scripts.inc\",\"hash\":\"a5cf5b2467dbf2d17d5ec0e787913e47\",\"size\":899,\"time\":1485787056,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/footer\\/scripts.inc\",\"hash\":\"fced54ec2607a8fe6dd699191c685650\",\"size\":1290,\"time\":1485787056}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__3ee5aa7292eb9baed29f36ff80bd36f8', '{\"source\":{\"file\":\"_includes\\/footer\\/google-analytics-dev.inc\",\"hash\":\"dc5afbf8dbee83c736af052de856c3c6\",\"size\":429,\"time\":1472063779,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/footer\\/google-analytics-dev.inc\",\"hash\":\"dc5afbf8dbee83c736af052de856c3c6\",\"size\":429,\"time\":1472063779}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__739b48386a9a0d75db915201f033c918', '{\"source\":{\"file\":\"_includes\\/lib\\/cms-notifications.inc\",\"hash\":\"fa9014bfce10b5ee5e382ff21813af66\",\"size\":923,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/_includes\\/lib\\/cms-notifications.inc\",\"hash\":\"fa9014bfce10b5ee5e382ff21813af66\",\"size\":923,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b4a4b91f157710edfa668a42e871ba58', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/templates\\/404.php\",\"hash\":\"0fecb738de0546befd8269c79d0be580\",\"size\":688,\"time\":1485808397,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/404.php\",\"hash\":\"0ebcf6ab0834e1a602e56f728362aa7d\",\"size\":1046,\"time\":1485808397}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__07892c6a10140f0f9eaf99b33d1ca0ed', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/templates\\/default.php\",\"hash\":\"f6f8b09aea915249b7c30025fd62ff4c\",\"size\":543,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/default.php\",\"hash\":\"f78cc49f117c0550f65ed29c95bad9a0\",\"size\":901,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__aaf18921d3bb93f0c1dd41e99a3de06f', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessWireConfig\\/ProcessWireConfig.module.php\",\"hash\":\"69191495699e9a0350cd82f6d2b8a0e0\",\"size\":21701,\"time\":1457642155,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessWireConfig\\/ProcessWireConfig.module.php\",\"hash\":\"f707b33c201448ddbc058789da31e93d\",\"size\":21909,\"time\":1457642155}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1c27dcb840143a9d55bfa884779b56d6', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/EditorSkinLightwire\\/EditorSkinLightwire.module\",\"hash\":\"b3d5fd3dc6da7cda18cd052fbf699974\",\"size\":2122,\"time\":1478893684,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/EditorSkinLightwire\\/EditorSkinLightwire.module\",\"hash\":\"da4bc2d1a02f377e9c9f8218ef16397d\",\"size\":2330,\"time\":1478893684}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__074320ec5f9a8357f72c5776887bf1bb', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyContentToOtherField.action.php\",\"hash\":\"f029a5c2000b83de62e799fb24b631f1\",\"size\":2533,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyContentToOtherField.action.php\",\"hash\":\"8ac3925d3a3e6ca0419f2df9c5a44db1\",\"size\":2546,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bab83f7a3b35d0ab7d5e6ca2c82af328', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyFieldContentToOtherPage.action.php\",\"hash\":\"ce238614a83c10a7f628821a8660a0bc\",\"size\":2135,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyFieldContentToOtherPage.action.php\",\"hash\":\"ce238614a83c10a7f628821a8660a0bc\",\"size\":2135,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ce056d41c16e3e0b2386758e2aa542c4', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyTableFieldRowsToOtherPage.action.php\",\"hash\":\"6fb4100c2f99524f11d31266c988337c\",\"size\":4550,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CopyTableFieldRowsToOtherPage.action.php\",\"hash\":\"6fb4100c2f99524f11d31266c988337c\",\"size\":4550,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__24751d54fd9c7671c98aa44a455ea4ab', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CreateUsersBatcher.action.php\",\"hash\":\"fcc56e5f88be37e84f3622bdd47fb7e5\",\"size\":4095,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/CreateUsersBatcher.action.php\",\"hash\":\"930e99b509f7a6ef35944721206ead16\",\"size\":4108,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__21e57e51ee87ed3fdcf8bb5b1fa762fe', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/DeleteUnusedFields.action.php\",\"hash\":\"552a0a9c90729e1f36bc69f0b19f47d2\",\"size\":1708,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/DeleteUnusedFields.action.php\",\"hash\":\"a929256ca0fed7191c5b94aa6bf9d4a5\",\"size\":1760,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__cf9d2399ea08d1f57b5ce6de71c691ca', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/DeleteUnusedTemplates.action.php\",\"hash\":\"348fba3d24d20186865c0d105db2123f\",\"size\":1805,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/DeleteUnusedTemplates.action.php\",\"hash\":\"f25bd7df2cc234be7a55eef0ca81a418\",\"size\":1831,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__573d219459bdb496a21f77fe2063adc1', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/FieldSetOrSearchAndReplace.action.php\",\"hash\":\"06052662f7d1d72b879fab2f0530e317\",\"size\":4329,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/FieldSetOrSearchAndReplace.action.php\",\"hash\":\"fcf3dc61774db74a60da08f7b0f423bd\",\"size\":4381,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fa64796e6010e342a209495418dc6f48', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageActiveLanguagesBatcher.action.php\",\"hash\":\"0e473c695428b6038f3686f2a20d5a39\",\"size\":2738,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageActiveLanguagesBatcher.action.php\",\"hash\":\"3c8a9bb363392a24158176286a2e5aa7\",\"size\":2751,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0cbcd56b7b9fe3ef4c222f2dcf2cc22a', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageManipulator.action.php\",\"hash\":\"12a7524a32de853092ec9d73c3e9d7d4\",\"size\":3826,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/actions\\/PageManipulator.action.php\",\"hash\":\"373e461e367a38a3345ee5c404b55f75\",\"size\":3904,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Permissions.names', '{\"batch-child-editor\":1038,\"batcher\":1023,\"changelog\":1152,\"config-edit\":1041,\"db-backup\":1034,\"form-builder\":1025,\"form-builder-add\":1026,\"logs-edit\":1014,\"logs-view\":1013,\"page-clone\":1017,\"page-clone-tree\":1018,\"page-delete\":34,\"page-edit\":32,\"page-edit-created\":1142,\"page-edit-images\":1143,\"page-edit-recent\":1011,\"page-hide\":1144,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-publish\":1145,\"page-rename\":1146,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"tracy-debugger\":1149,\"user-admin\":52,\"user-admin-all\":1147,\"user-admin-webmaster\":1148}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVerbose.info', '{\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"162\":{\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"author\":\"Tom Reno (Renobird)\",\"core\":true,\"versionStr\":\"0.1.7\"},\"199\":{\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.4\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.0\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.4\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.5\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.1\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"163\":{\"summary\":\"Field that stores single and multi select options.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.3\"},\"164\":{\"summary\":\"A fieldtype containing a group of editable pages.\",\"core\":true,\"versionStr\":\"0.0.8\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"166\":{\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"167\":{\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"168\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.1.3\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.0\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"240\":{\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"1.2.0\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.4\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.5.7\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.5\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.4\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.3\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"161\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.1.9\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.4\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.6\"},\"169\":{\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"core\":true,\"versionStr\":\"1.1.2\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"165\":{\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"core\":true,\"versionStr\":\"0.1.3\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn\'t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.7\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.0.7\"},\"177\":{\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"http:\\/\\/processwire.com\\/talk\\/index.php\\/topic,284.0.html\",\"core\":true,\"versionStr\":\"1.0.2\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"201\":{\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"core\":true,\"versionStr\":\"1.0.1\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"113\":{\"summary\":\"Adds a render() method to all PageArray instances for basic unordered list generation of PageArrays.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.4\"},\"171\":{\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"core\":true,\"versionStr\":\"0.0.2\"},\"172\":{\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.2\"},\"173\":{\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"160\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.1\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.3\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.8\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"174\":{\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a \\\"copy\\\" option to all applicable pages in the PageList.\",\"core\":true,\"versionStr\":\"1.0.3\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"12\":{\"summary\":\"List pages in a hierarchal tree structure\",\"core\":true,\"versionStr\":\"1.1.8\"},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.4\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.2\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"159\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.3\"},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\"},\"229\":{\"summary\":\"Enables you to browse active database sessions.\",\"core\":true,\"versionStr\":\"0.0.3\"},\"228\":{\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"core\":true,\"versionStr\":\"0.0.5\"},\"125\":{\"summary\":\"Throttles the frequency of logins for a given account, helps to reduce dictionary attacks by introducing an exponential delay between logins.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"176\":{\"summary\":\"Field that stores user notifications.\",\"core\":true,\"versionStr\":\"0.0.4\"},\"175\":{\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"core\":true,\"versionStr\":\"0.1.2\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.5\"},\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"207\":{\"summary\":\"Quick batch creation (titles only or CSV import for other fields), editing, sorting, deletion, and CSV export of all children under a given page.\",\"author\":\"Adrian Jones\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/batch-child-editor\\/\",\"versionStr\":\"1.6.2\",\"permissions\":{\"batch-child-editor\":\"Batch Child Editor\"}},\"208\":{\"summary\":\"Helper module for BatchChildEditor for creating CSV to export\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/batch-child-editor\\/\",\"versionStr\":\"1.3.9\"},\"211\":{\"summary\":\"Periodic automatic backup of the database. Set interval in settings.\",\"author\":\"kixe\",\"href\":\"https:\\/\\/processwire.com\\/talk\\/topic\\/8207-cronjob-database-backup\\/\",\"versionStr\":\"1.1.5\"},\"246\":{\"summary\":\"A lighter skin for CKEditor.\",\"versionStr\":\"1.0.3\"},\"183\":{\"summary\":\"Turns any Fieldtype into a multiple-value version of itself.\",\"versionStr\":\"0.1.0\"},\"184\":{\"summary\":\"Multiplies single inputs with a fixed quantity.\",\"versionStr\":\"0.1.0\"},\"185\":{\"summary\":\"Field that lets you define a database table of custom fields.\",\"versionStr\":\"0.1.4\"},\"186\":{\"summary\":\"Field that lets you define a database table of custom inputs.\",\"versionStr\":\"0.1.4\"},\"187\":{\"summary\":\"Multiple text\\/textarea fields combined into one field.\",\"versionStr\":\"0.0.6\"},\"188\":{\"summary\":\"Multiple text\\/textarea fields combined into one field.\",\"versionStr\":\"0.0.6\"},\"193\":{\"summary\":\"Create or edit forms and manage submitted entries.\",\"versionStr\":\"0.3.0\"},\"195\":{\"summary\":\"Form Builder file upload input (alpha test)\",\"versionStr\":\"0.0.1\"},\"194\":{\"summary\":\"Create or edit forms and manage submitted entries.\",\"versionStr\":\"0.3.0\"},\"179\":{\"summary\":\"Provides jQuery DataTables for use in ProcessWire.\",\"href\":\"http:\\/\\/datatables.net\",\"versionStr\":\"1.9.4\"},\"192\":{\"summary\":\"The all-in-one SEO solution for ProcessWire.\",\"versionStr\":\"0.8.7\"},\"200\":{\"summary\":\"Generates an XML sitemap at yoursite.com\\/sitemap.xml for use with Google Webmaster Tools etc.\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/index.php\\/topic,867.0.html\",\"versionStr\":\"1.1.0\"},\"180\":{\"summary\":\"Browse Modules from modules.processwire.com. Download, update or install them. Consider this a proof of concept and use at your own risk.\",\"author\":\"Philipp \'Soma\' Urlich\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/1550-modules-manager\\/\",\"versionStr\":\"2.1.6\"},\"219\":{\"summary\":\"Adds a tab with a list of pages referencing the current page to the page edit view.\",\"author\":\"Niklas Lakanen\",\"versionStr\":\"0.3.0\"},\"189\":{\"summary\":\"For a big performance boost, Pro Cache delivers site pages as static files without PHP or MySQL.\",\"author\":\"Ryan Cramer Design, LLC\",\"href\":\"http:\\/\\/processwire.com\\/ProCache\\/\",\"versionStr\":\"3.1.4\"},\"190\":{\"summary\":\"Configuration and maintenance utilty for the ProCache module\",\"author\":\"Ryan Cramer Design, LLC\",\"href\":\"http:\\/\\/processwire.com\\/ProCache\\/\",\"versionStr\":\"3.1.4\"},\"242\":{\"summary\":\"Keep track of changes (edits, removals, additions etc.)\",\"author\":\"Teppo Koivula\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/process-changelog\\/\",\"versionStr\":\"1.4.1\"},\"243\":{\"summary\":\"Hooks required by Process Changelog for collecting data\",\"author\":\"Teppo Koivula\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/process-changelog\\/\",\"versionStr\":\"1.1.10\"},\"203\":{\"summary\":\"Create and\\/or restore database backups from ProcessWire admin.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.3\",\"permissions\":{\"db-backup\":\"Manage database backups (recommended for superuser only)\"},\"page\":{\"name\":\"db-backups\",\"parent\":\"setup\",\"title\":\"DB Backups\"}},\"214\":{\"summary\":\"Allows collection of database diagnostics\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.0.2\"},\"215\":{\"summary\":\"Allows collection of file and directory diagnostics\",\"author\":\"Stephen Dickinson, QBox.co\",\"versionStr\":\"0.0.1\"},\"218\":{\"summary\":\"Allows collection of image handling diagnostics\",\"author\":\"Horst\",\"versionStr\":\"0.0.1\"},\"213\":{\"summary\":\"Allows collection of PHP diagnostics\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.0.7\"},\"217\":{\"summary\":\"Allows collection of webserver diagnostics\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.0.1\"},\"212\":{\"summary\":\"Allows evaluation of run-time environment and detection of changes.\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.2.2\"},\"231\":{\"versionStr\":\"0.0.4\"},\"202\":{\"summary\":\"Manage redirects\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/index.php\\/topic,171.0.html\",\"versionStr\":\"1.0.0\"},\"210\":{\"summary\":\"Enables you to customize most ProcessWire config settings from the admin, plus create your own.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.4\",\"permissions\":{\"config-edit\":\"Edit ProcessWire config settings (superuser recommended)\"},\"page\":{\"name\":\"config\",\"parent\":\"setup\",\"title\":\"Config\"}},\"244\":{\"summary\":\"Tool that helps you identify and install core and module upgrades.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.7\"},\"182\":{\"summary\":\"Automatically checks for core and installed module upgrades at routine intervals.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.7\"},\"238\":{\"summary\":\"List, manage and compare draft pages.\",\"author\":\"Ryan Cramer Design, LLC\",\"href\":\"https:\\/\\/processwire.com\\/ProDrafts\\/\",\"versionStr\":\"0.0.2\",\"page\":{\"name\":\"prodrafts\",\"parent\":\"page\",\"title\":\"Drafts\"}},\"237\":{\"summary\":\"Enables support of draft page versions.\",\"author\":\"Ryan Cramer Design, LLC\",\"href\":\"https:\\/\\/processwire.com\\/ProDrafts\\/\",\"versionStr\":\"0.0.4\"},\"209\":{\"summary\":\"Allows you to put your site into protected mode so that users must be logged in to access the front-end of the site.\",\"author\":\"Adrian Jones\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/protected-mode\\/\",\"versionStr\":\"0.1.3\"},\"198\":{\"summary\":\"Automatically links any of the configured text phrases to a URL, any time they appear.\",\"versionStr\":\"0.0.2\"}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b41df4c1ca12c60f22938f13966ae422', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessAdminActions\\/ProcessAdminActions.module\",\"hash\":\"c362be45b2db1f2e2fdd9c29a5cd4610\",\"size\":32263,\"time\":1483719153,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessAdminActions\\/ProcessAdminActions.module\",\"hash\":\"759f253da55ad6c2c4563e8be10351f4\",\"size\":33729,\"time\":1483719153}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5d802953d13248dbb4ca3129a0bfecc4', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/modules\\/ProcessGeneralSettings\\/ProcessGeneralSettings.module\",\"hash\":\"a4aeec8aff9715446a3402a7cbdacc6b\",\"size\":8935,\"time\":1483719407,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/ProcessGeneralSettings\\/ProcessGeneralSettings.module\",\"hash\":\"94d6995d307ee8aa61711107d619cb7c\",\"size\":10025,\"time\":1483719407}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__18db8b3be95ee287619159c87d812c80', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/ready.php\",\"hash\":\"11e9ff919969a531919cab791c32c465\",\"size\":468,\"time\":1486159049,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/ready.php\",\"hash\":\"11e9ff919969a531919cab791c32c465\",\"size\":468,\"time\":1486159049}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e9d762a153496435b22fd381d8143b2b', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/finished.php\",\"hash\":\"c47865b6b058d6828800ea19972f960f\",\"size\":479,\"time\":1486159176,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/finished.php\",\"hash\":\"c47865b6b058d6828800ea19972f960f\",\"size\":479,\"time\":1486159176}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2d3adcddb8dd86e71b3c8740245fc80b', '{\"source\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/init.php\",\"hash\":\"17f8b60e62bdb1168f311c6ff80537b8\",\"size\":467,\"time\":1486158986,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/Volumes\\/Backup\\/WebDev\\/ProcessWire Local\\/Master\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/init.php\",\"hash\":\"17f8b60e62bdb1168f311c6ff80537b8\",\"size\":467,\"time\":1486158986}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ProcessWireUpgradeCheck_loginHook', '{\"branches\":{\"dev\":{\"name\":\"dev\",\"title\":\"Dev\",\"zipURL\":\"https:\\/\\/github.com\\/processwire\\/processwire\\/archive\\/dev.zip\",\"version\":\"3.0.54\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/processwire\\/processwire\\/dev\\/wire\\/core\\/ProcessWire.php\"},\"master\":{\"name\":\"master\",\"title\":\"Master\",\"zipURL\":\"https:\\/\\/github.com\\/processwire\\/processwire\\/archive\\/master.zip\",\"version\":\"3.0.42\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/processwire\\/processwire\\/master\\/wire\\/core\\/ProcessWire.php\"},\"legacy-master\":{\"name\":\"legacy-master\",\"title\":\"Legacy\\/Master\",\"zipURL\":\"https:\\/\\/github.com\\/processwire\\/processwire-legacy\\/archive\\/master.zip\",\"version\":\"2.8.35\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/processwire\\/processwire-legacy\\/master\\/wire\\/core\\/ProcessWire.php\"},\"prev-dev\":{\"name\":\"prev-dev\",\"title\":\"Prev\\/Dev\",\"zipURL\":\"https:\\/\\/github.com\\/ryancramerdesign\\/ProcessWire\\/archive\\/dev.zip\",\"version\":\"2.7.3\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/ryancramerdesign\\/ProcessWire\\/dev\\/wire\\/core\\/ProcessWire.php\"},\"prev-devns\":{\"name\":\"prev-devns\",\"title\":\"Prev\\/Devns\",\"zipURL\":\"https:\\/\\/github.com\\/ryancramerdesign\\/ProcessWire\\/archive\\/devns.zip\",\"version\":\"3.0.33\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/ryancramerdesign\\/ProcessWire\\/devns\\/wire\\/core\\/ProcessWire.php\"},\"prev-master\":{\"name\":\"prev-master\",\"title\":\"Prev\\/Master\",\"zipURL\":\"https:\\/\\/github.com\\/ryancramerdesign\\/ProcessWire\\/archive\\/master.zip\",\"version\":\"2.7.3\",\"versionURL\":\"https:\\/\\/raw.githubusercontent.com\\/ryancramerdesign\\/ProcessWire\\/master\\/wire\\/core\\/ProcessWire.php\"}},\"moduleVersions\":{\"BatchChildEditor\":{\"title\":\"Batch child editor\",\"local\":\"1.5.3\",\"remote\":\"1.6.2\",\"new\":1,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.5.24\"]}},\"ProCache\":{\"title\":\"ProCache\",\"local\":\"3.1.4\",\"remote\":\"3.1.5\",\"new\":1,\"requiresVersions\":[]}}}', '2017-03-07 01:34:57');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.info', '{\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"162\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1439402367,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"199\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"singular\":1,\"created\":1439405266,\"namespace\":\"ProcessWire\\\\\"},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":104,\"namespace\":\"ProcessWire\\\\\"},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":100,\"singular\":true,\"namespace\":\"ProcessWire\\\\\"},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":100,\"singular\":1,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":104,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":105,\"singular\":1,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":101,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"163\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"singular\":true,\"created\":1439402390,\"namespace\":\"ProcessWire\\\\\"},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":103,\"autoload\":true,\"singular\":true,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"164\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"singular\":true,\"created\":1439402399,\"namespace\":\"ProcessWire\\\\\"},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"166\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":105,\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"singular\":true,\"created\":1439402405,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"167\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":105,\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1439402405,\"namespace\":\"ProcessWire\\\\\"},\"168\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"singular\":1,\"created\":1439402410,\"namespace\":\"ProcessWire\\\\\"},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":100,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":106,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"240\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"created\":1472063402,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":120,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":104,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":157,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1406298377,\"namespace\":\"ProcessWire\\\\\"},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":105,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"namespace\":\"ProcessWire\\\\\"},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":124,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":103,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"161\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1439402154,\"namespace\":\"ProcessWire\\\\\"},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":119,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":104,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":106,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"169\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"created\":1439402431,\"namespace\":\"ProcessWire\\\\\"},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"165\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1439402399,\"namespace\":\"ProcessWire\\\\\"},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":27,\"autoload\":\"template=admin\",\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1405941705,\"namespace\":\"ProcessWire\\\\\"},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"namespace\":\"ProcessWire\\\\\"},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":107,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"177\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"autoload\":true,\"singular\":true,\"created\":1439402507,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"201\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"autoload\":true,\"singular\":true,\"created\":1439405278,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":105,\"created\":1406298377,\"namespace\":\"ProcessWire\\\\\"},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"namespace\":\"ProcessWire\\\\\"},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":104,\"namespace\":\"ProcessWire\\\\\"},\"171\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":2,\"autoload\":true,\"singular\":true,\"created\":1439402447,\"namespace\":\"ProcessWire\\\\\"},\"172\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"autoload\":true,\"singular\":true,\"created\":1439402454,\"namespace\":\"ProcessWire\\\\\"},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":112,\"icon\":\"cube\",\"permission\":\"field-admin\",\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"173\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":101,\"permission\":\"page-view\",\"singular\":1,\"created\":1439402466,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"160\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":1,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1439402154,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":103,\"permission\":\"page-view\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":118,\"permission\":\"module-admin\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"174\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":103,\"permission\":\"page-clone\",\"autoload\":\"template=admin\",\"singular\":true,\"created\":1439402475,\"namespace\":\"ProcessWire\\\\\"},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":108,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":118,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":24,\"icon\":\"search\",\"permission\":\"page-lister\",\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":102,\"singular\":1,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":103,\"permission\":\"profile-edit\",\"singular\":1,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"159\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1439402145,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":103,\"icon\":\"gears\",\"permission\":\"role-admin\",\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"229\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":3,\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"singular\":1,\"created\":1443057481,\"namespace\":\"ProcessWire\\\\\"},\"228\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"installs\":[\"ProcessSessionDB\"],\"autoload\":true,\"singular\":true,\"created\":1443057481,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":102,\"autoload\":\"function\",\"singular\":true,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"176\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"singular\":true,\"created\":1439402485,\"namespace\":\"ProcessWire\\\\\"},\"175\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1439402485,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":15,\"singular\":true,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"namespace\":\"ProcessWire\\\\\"},\"207\":{\"name\":\"BatchChildEditor\",\"title\":\"Batch child editor\",\"version\":\"1.6.2\",\"icon\":\"child\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.5.24\"]},\"installs\":[\"ProcessChildrenCsvExport\"],\"autoload\":\"template=admin\",\"created\":1439406435,\"configurable\":3,\"namespace\":\"\\\\\"},\"208\":{\"name\":\"ProcessChildrenCsvExport\",\"title\":\"Process Children CSV Export\",\"version\":139,\"requiresVersions\":{\"BatchChildEditor\":[\">=\",0]},\"permission\":\"batch-child-editor\",\"singular\":true,\"created\":1439406435,\"namespace\":\"\\\\\"},\"211\":{\"name\":\"CronjobDatabaseBackup\",\"title\":\"Cronjob Database Backup\",\"version\":115,\"icon\":\"database\",\"requiresVersions\":{\"LazyCron\":[\">=\",0],\"ProcessWire\":[\">=\",\"2.4.15\"]},\"permission\":\"db-backup\",\"autoload\":true,\"singular\":true,\"created\":1439407365,\"configurable\":true,\"namespace\":\"\\\\\",\"license\":\"GNU-GPLv3\",\"hreflicense\":\"http:\\/\\/www.gnu.org\\/licenses\\/gpl-3.0.html\"},\"246\":{\"name\":\"EditorSkinLightwire\",\"title\":\"Lightwire Skin for CKEditor\",\"version\":\"1.0.3\",\"requiresVersions\":{\"InputfieldCKEditor\":[\">=\",0]},\"created\":1478893687,\"configurable\":true,\"namespace\":\"\\\\\"},\"183\":{\"name\":\"FieldtypeMultiplier\",\"title\":\"ProFields: Multiplier\",\"version\":10,\"installs\":[\"InputfieldMultiplier\"],\"singular\":1,\"created\":1439402804,\"namespace\":\"\\\\\"},\"184\":{\"name\":\"InputfieldMultiplier\",\"title\":\"ProFields: Multiplier\",\"version\":10,\"requiresVersions\":{\"FieldtypeMultiplier\":[\">=\",0]},\"created\":1439402804,\"namespace\":\"\\\\\"},\"185\":{\"name\":\"FieldtypeTable\",\"title\":\"ProFields: Table\",\"version\":14,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.8.27\"]},\"installs\":[\"InputfieldTable\"],\"singular\":1,\"created\":1439402813,\"namespace\":\"\\\\\"},\"186\":{\"name\":\"InputfieldTable\",\"title\":\"ProFields: Table\",\"version\":14,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.8.27\"]},\"created\":1439402813,\"namespace\":\"\\\\\"},\"187\":{\"name\":\"FieldtypeTextareas\",\"title\":\"ProFields: Textareas\",\"version\":6,\"installs\":[\"InputfieldTextareas\"],\"singular\":1,\"created\":1439402820,\"configurable\":true,\"namespace\":\"\\\\\"},\"188\":{\"name\":\"InputfieldTextareas\",\"title\":\"ProFields: Textareas\",\"version\":6,\"requiresVersions\":{\"FieldtypeTextareas\":[\">=\",0]},\"created\":1439402820,\"namespace\":\"\\\\\"},\"193\":{\"name\":\"FormBuilder\",\"title\":\"Form Builder\",\"version\":30,\"installs\":[\"ProcessFormBuilder\",\"InputfieldFormBuilderFile\"],\"autoload\":true,\"singular\":true,\"created\":1439403112,\"configurable\":true,\"namespace\":\"\\\\\"},\"195\":{\"name\":\"InputfieldFormBuilderFile\",\"title\":\"File (for FormBuilder)\",\"version\":1,\"requiresVersions\":{\"FormBuilder\":[\">=\",0]},\"created\":1439403112,\"namespace\":\"\\\\\"},\"194\":{\"name\":\"ProcessFormBuilder\",\"title\":\"Forms\",\"version\":30,\"icon\":\"building\",\"requiresVersions\":{\"FormBuilder\":[\">=\",0]},\"permission\":\"form-builder\",\"singular\":1,\"created\":1439403112,\"namespace\":\"\\\\\",\"useNavJSON\":true},\"179\":{\"name\":\"JqueryDataTables\",\"title\":\"jQuery DataTables Plugin\",\"version\":194,\"singular\":1,\"created\":1439402664,\"namespace\":\"\\\\\"},\"192\":{\"name\":\"MarkupSEO\",\"title\":\"SEO\",\"version\":\"0.8.7\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.4.0\"],\"PHP\":[\">=\",\"5.3.8\"]},\"autoload\":true,\"created\":1439402908,\"configurable\":true,\"namespace\":\"\\\\\"},\"200\":{\"name\":\"MarkupSitemapXML\",\"title\":\"Markup Sitemap XML\",\"version\":110,\"autoload\":true,\"singular\":true,\"created\":1439405278,\"namespace\":\"\\\\\"},\"180\":{\"name\":\"ModulesManager\",\"title\":\"Modules Manager\",\"version\":216,\"requiresVersions\":{\"JqueryDataTables\":[\">=\",0]},\"permission\":\"modules-manager\",\"singular\":true,\"created\":1439402674,\"configurable\":true,\"namespace\":\"\\\\\"},\"219\":{\"name\":\"PageReferencesTab\",\"title\":\"Page References Tab\",\"version\":30,\"autoload\":true,\"created\":1439488028,\"configurable\":true,\"namespace\":\"\\\\\"},\"189\":{\"name\":\"ProCache\",\"title\":\"ProCache\",\"version\":314,\"icon\":\"fighter-jet\",\"installs\":[\"ProcessProCache\"],\"autoload\":true,\"singular\":true,\"created\":1439402831,\"configurable\":true,\"namespace\":\"\\\\\"},\"190\":{\"name\":\"ProcessProCache\",\"title\":\"ProCache\",\"version\":314,\"icon\":\"fighter-jet\",\"requiresVersions\":{\"ProCache\":[\">=\",0]},\"singular\":1,\"created\":1439402831,\"namespace\":\"\\\\\"},\"242\":{\"name\":\"ProcessChangelog\",\"title\":\"Changelog\",\"version\":\"1.4.1\",\"icon\":\"code-fork\",\"installs\":[\"ProcessChangelogHooks\"],\"permission\":\"changelog\",\"singular\":true,\"created\":1472063442,\"configurable\":true,\"namespace\":\"\\\\\"},\"243\":{\"name\":\"ProcessChangelogHooks\",\"title\":\"Changelog Hooks\",\"version\":\"1.1.10\",\"requiresVersions\":{\"ProcessChangelog\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1472063442,\"configurable\":true,\"namespace\":\"\\\\\"},\"203\":{\"name\":\"ProcessDatabaseBackups\",\"title\":\"Database Backups\",\"version\":3,\"icon\":\"database\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.4.15\"]},\"permission\":\"db-backup\",\"singular\":1,\"created\":1439406341,\"namespace\":\"\\\\\",\"nav\":[{\"url\":\"backup\\/\",\"label\":\"New Backup\",\"icon\":\"plus-circle\"},{\"url\":\"upload\\/\",\"label\":\"Upload\",\"icon\":\"upload\"}]},\"214\":{\"name\":\"DiagnoseDatabase\",\"title\":\"Database Diagnostics\",\"version\":2,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1439407879,\"namespace\":\"\\\\\"},\"215\":{\"name\":\"DiagnoseFiles\",\"title\":\"Filesystem Diagnostics\",\"version\":1,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1439407893,\"namespace\":\"\\\\\"},\"218\":{\"name\":\"DiagnoseImagehandling\",\"title\":\"Image Handling\",\"version\":1,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1439408150,\"namespace\":\"\\\\\"},\"213\":{\"name\":\"DiagnosePhp\",\"title\":\"PHP Diagnostics\",\"version\":7,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1439407864,\"namespace\":\"\\\\\"},\"217\":{\"name\":\"DiagnoseWebserver\",\"title\":\"Webserver Diagnostics\",\"version\":1,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1439407905,\"namespace\":\"\\\\\"},\"212\":{\"name\":\"ProcessDiagnostics\",\"title\":\"Diagnostics Page\",\"version\":22,\"singular\":true,\"created\":1439407841,\"namespace\":\"\\\\\"},\"231\":{\"name\":\"ProcessPageCloneAdaptUrls\",\"title\":\"Adapt URLs after Cloning\",\"version\":\"0.0.4\",\"requiresVersions\":{\"ProcessPageClone\":[\">=\",0]},\"autoload\":true,\"created\":1458760223,\"namespace\":\"\\\\\",\"description\":\"Adjust links and img sources in CKEditor fields after page clone\"},\"202\":{\"name\":\"ProcessRedirects\",\"title\":\"Redirects\",\"version\":100,\"permission\":\"redirects-admin\",\"autoload\":true,\"singular\":true,\"created\":1439406057,\"namespace\":\"\\\\\"},\"210\":{\"name\":\"ProcessWireConfig\",\"title\":\"ProcessWire Config\",\"version\":4,\"icon\":\"gear\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.5.10\"]},\"permission\":\"config-edit\",\"singular\":1,\"created\":1439407110,\"configurable\":\"ProcessWireConfig.config.php\",\"namespace\":\"\\\\\"},\"244\":{\"name\":\"ProcessWireUpgrade\",\"title\":\"Upgrades\",\"version\":7,\"icon\":\"coffee\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.5.20\"]},\"installs\":[\"ProcessWireUpgradeCheck\"],\"singular\":1,\"created\":1475605604,\"namespace\":\"\\\\\"},\"182\":{\"name\":\"ProcessWireUpgradeCheck\",\"title\":\"Upgrades Checker\",\"version\":7,\"icon\":\"coffee\",\"autoload\":\"template=admin\",\"singular\":true,\"created\":1439402725,\"configurable\":\"ProcessWireUpgradeCheck.config.php\",\"namespace\":\"\\\\\"},\"238\":{\"name\":\"ProcessProDrafts\",\"title\":\"ProDrafts\",\"version\":2,\"icon\":\"paperclip\",\"requiresVersions\":{\"ProDrafts\":[\">=\",0]},\"permission\":\"page-edit\",\"singular\":1,\"created\":1459974821,\"namespace\":\"\\\\\",\"useNavJSON\":true},\"237\":{\"name\":\"ProDrafts\",\"title\":\"ProDrafts (Main)\",\"version\":4,\"icon\":\"paperclip\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"2.7.0\"]},\"installs\":[\"ProcessProDrafts\"],\"autoload\":true,\"singular\":true,\"created\":1459974821,\"configurable\":\"ProDrafts.config.php\",\"namespace\":\"\\\\\"},\"209\":{\"name\":\"ProtectedMode\",\"title\":\"Protected Mode\",\"version\":13,\"icon\":\"key\",\"autoload\":true,\"singular\":true,\"created\":1439406874,\"configurable\":true,\"namespace\":\"\\\\\"},\"198\":{\"name\":\"TextformatterAutoLinks\",\"title\":\"ProFields: Auto Links\",\"version\":2,\"singular\":1,\"created\":1439403378,\"configurable\":true,\"namespace\":\"\\\\\",\"url\":\"http:\\/\\/processwire.com\"}}', '2010-04-08 03:10:10');

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_admin_theme` (`pages_id`, `data`) VALUES('41', '162');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_canonical`;
CREATE TABLE `field_canonical` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_company`;
CREATE TABLE `field_company` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_email` (`pages_id`, `data`) VALUES('41', 'web@sfamarketing.com');

DROP TABLE IF EXISTS `field_files`;
CREATE TABLE `field_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(255) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_google_analytics`;
CREATE TABLE `field_google_analytics` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_headline`;
CREATE TABLE `field_headline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(255) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_live`;
CREATE TABLE `field_live` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(10) unsigned NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_live` (`pages_id`, `data`, `sort`) VALUES('1044', '1', '0');

DROP TABLE IF EXISTS `field_notifications`;
CREATE TABLE `field_notifications` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `title` tinytext NOT NULL,
  `src_id` int(10) unsigned NOT NULL,
  `flags` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `qty` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `src_id` (`src_id`),
  KEY `flags` (`flags`),
  KEY `created` (`created`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_notifications` (`pages_id`, `data`, `sort`, `title`, `src_id`, `flags`, `created`, `modified`, `qty`) VALUES('41', '{\"id\":\"noIDc2f73abafcaf652030d7d404c4244fb4\",\"html\":\"<p><b>Referer:<\\/b> unknown<br \\/><b>Useragent:<\\/b> Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/55.0.2883.95 Safari\\/537.36<br \\/><b>IP:<\\/b> 0.0.0.0<br \\/><b>Page:<\\/b> Unknown<br \\/><b>User:<\\/b> admin<\\/p>\",\"expires\":1483719006}', '1', '404 occurred: /wire/modules/AdminTheme/AdminThemeReno/styles/main.css.map', '27', '16388', '2017-01-06 11:09:36', '2017-01-06 11:09:39', '5');
INSERT INTO `field_notifications` (`pages_id`, `data`, `sort`, `title`, `src_id`, `flags`, `created`, `modified`, `qty`) VALUES('41', '{\"id\":\"noID318b2c70c18e8d0342d6213faa4c4bf7\",\"html\":\"<p><b>Referer:<\\/b> unknown<br \\/><b>Useragent:<\\/b> Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/55.0.2883.95 Safari\\/537.36<br \\/><b>IP:<\\/b> 0.0.0.0<br \\/><b>Page:<\\/b> Unknown<br \\/><b>User:<\\/b> admin<\\/p>\",\"expires\":1483719005}', '0', '404 occurred: /wire/templates-admin/styles/AdminTheme.css.map', '27', '16388', '2017-01-06 11:09:35', '2017-01-06 11:09:39', '5');

DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('41', 'rtV6lLnRvFteKFjBnsOOA3pGr8imajq', '$2y$11$gU/m1/zHhDLO76zMenjcPe');
INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('40', '', '');

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '32', '9');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '32', '10');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '34', '12');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '34', '9');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '35', '18');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '35', '13');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('37', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '36', '27');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '36', '16');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '50', '21');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '50', '14');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '51', '22');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '51', '15');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '52', '23');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '53', '28');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '53', '17');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '54', '17');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1006', '26');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1006', '12');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1011', '15');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1011', '11');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1013', '8');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1014', '7');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1017', '10');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1017', '7');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1018', '11');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1018', '8');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1023', '1');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1025', '5');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1025', '2');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1026', '6');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1026', '3');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1034', '4');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('1043', '1034', '1');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1038', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1041', '3');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1142', '13');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1143', '14');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1144', '16');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1145', '19');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1146', '20');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1147', '24');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1148', '25');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '1149', '29');

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES('6', '17');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('3', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('8', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('9', '14');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('10', '7');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('11', '47');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('16', '48');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('300', '104');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('21', '50');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('29', '66');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('23', '10');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('304', '138');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('31', '136');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('22', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('30', '68');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('303', '129');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('2', '87');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('302', '121');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('301', '109');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('28', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1007', '150');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1010', '159');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1012', '160');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1016', '174');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1019', '180');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1153', '244');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1021', '190');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1027', '194');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1032', '202');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1033', '203');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1151', '242');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1039', '208');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1040', '210');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1042', '212');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1150', '238');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1078', '229');

DROP TABLE IF EXISTS `field_redirect`;
CREATE TABLE `field_redirect` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('40', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '38', '2');

DROP TABLE IF EXISTS `field_seo_canonical`;
CREATE TABLE `field_seo_canonical` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_custom`;
CREATE TABLE `field_seo_custom` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_description`;
CREATE TABLE `field_seo_description` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_image`;
CREATE TABLE `field_seo_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_keywords`;
CREATE TABLE `field_seo_keywords` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_tab`;
CREATE TABLE `field_seo_tab` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_tab_end`;
CREATE TABLE `field_seo_tab_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_seo_title`;
CREATE TABLE `field_seo_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_sitemap_ignore`;
CREATE TABLE `field_sitemap_ignore` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(255)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_title` (`pages_id`, `data`) VALUES('11', 'Templates');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('16', 'Fields');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('22', 'Setup');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('3', 'Pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('6', 'Add Page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('8', 'Tree');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('9', 'Save Sort');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('10', 'Edit');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('21', 'Modules');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('29', 'Users');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('30', 'Roles');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('2', 'Admin');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('7', 'Trash');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('27', '404 Not Found');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('302', 'Insert Link');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('23', 'Login');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('304', 'Profile');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('301', 'Empty Trash');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('300', 'Search');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('303', 'Insert Image');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('28', 'Access');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('31', 'Permissions');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('32', 'Edit pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('34', 'Delete pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('35', 'Move pages (change parent)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('36', 'View pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('50', 'Sort child pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('51', 'Change templates on pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('52', 'Administer users');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('53', 'User can update profile/password');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('54', 'Lock or unlock a page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1', 'Home');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1006', 'Use Page Lister');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1007', 'Find');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1010', 'Recent');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1011', 'Can see recently edited pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1012', 'Logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1013', 'Can view system logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1014', 'Can manage system logs');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1015', 'Repeaters');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1016', 'Clone');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1017', 'Clone a page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1018', 'Clone a tree of pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1019', 'ModulesManager');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1153', 'Upgrades');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1021', 'ProCache');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1023', 'View Batcher Page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1024', 'Form Builder');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1025', 'Access Form Builder admin page');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1026', 'Add new or import Form Builder forms');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1027', 'Forms');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1145', 'Publish/unpublish pages or edit already published pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1144', 'Hide/unhide pages');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1143', 'Use the image editor to manipulate (crop, resize, etc.) images');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1032', 'Redirects');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1033', 'DB Backups');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1034', 'Manage database backups (recommended for superuser only)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1038', 'Use Batch Child Editor');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1039', 'Children CSV Export');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1040', 'Config');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1041', 'Edit ProcessWire config settings (superuser recommended)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1042', 'Diagnostics');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1044', 'Settings');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1142', 'Edit only pages user has created');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1151', 'Changelog');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1078', 'Sessions');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1079', 'About');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1080', 'Products');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1081', 'Services');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1082', 'Markets Served');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1083', 'News');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1084', 'Contact Us');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1085', 'Blog');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1086', 'Our Company');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1087', 'Careers');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1088', 'Product Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1089', 'Product Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1090', 'Product Category 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1091', 'Product Category 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1092', 'Product Category 5');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1093', 'Sub Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1094', 'Sub Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1095', 'Sub Category 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1096', 'Sub Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1097', 'Sub Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1098', 'Sub Category 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1099', 'Sub Category 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1100', 'Sub Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1101', 'Sub Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1102', 'Sub Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1103', 'Sub Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1104', 'Sub Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1105', 'Sub Category 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1106', 'Sub Category 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1107', 'Sub Category 5');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1108', 'Service Category 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1109', 'Service Category 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1110', 'Service Category 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1111', 'Market 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1112', 'Market 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1113', 'Market 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1114', 'Market 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1115', 'Market 5');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1116', 'Market 6');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1117', 'Market 7');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1118', 'Sales');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1119', 'Customer Service');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1120', 'Technical Support');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1121', 'Product 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1122', 'Product 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1123', 'Product 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1124', 'Product 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1125', 'Product 5');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1126', 'Product 6');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1127', 'Product 7');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1128', 'Product 8');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1129', 'Product 9');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1130', 'Product 10');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1131', 'Product 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1132', 'Product 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1133', 'Product 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1134', 'Product 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1135', 'Product 5');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1136', 'Product 1');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1137', 'Product 2');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1138', 'Product 3');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1139', 'Product 4');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1146', 'Change the name of published pages they are allowed to edit');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1147', 'Administer users in any role (except superuser)');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1148', 'Administer users in role: webmaster');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1149', 'Tracy Debugger Permissions');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1150', 'Drafts');
INSERT INTO `field_title` (`pages_id`, `data`) VALUES('1152', 'Access changelog');

DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES('2', 'admin');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('3', 'user');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('4', 'role');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('5', 'permission');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('1', 'home');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('97', 'form-builder');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('98', '404');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('99', 'default');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('100', 'settings');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('101', 'fileless');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fields_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '4', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('4', '5', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('5', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '97', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '100', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '98', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '100', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '109', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '111', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '105', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '106', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '104', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '103', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '102', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '101', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '109', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '101', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '99', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '111', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '105', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '106', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '110', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '107', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '105', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '106', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '104', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '103', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '102', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '104', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '103', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '102', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '101', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '99', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '110', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '107', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '108', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '108', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '1', '0', '{\"collapsed\":\"4\",\"columnWidth\":100}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '2', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '99', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '92', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '3', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '114', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '113', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '100', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '115', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '112', '3', '{\"columnWidth\":50}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '112', '3', '{\"columnWidth\":50}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '109', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '111', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '110', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '112', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '107', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '108', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '1', '0', NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(255) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(255) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('1', 'FieldtypePageTitle', 'title', '13', 'Title', '{\"required\":1,\"size\":0,\"maxlength\":255,\"collapsed\":0,\"tags\":\"Generic\",\"send_templates\":[44,2,45,43,1,5],\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('2', 'FieldtypeModule', 'process', '25', 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('3', 'FieldtypePassword', 'pass', '24', 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('5', 'FieldtypePage', 'permissions', '24', 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('4', 'FieldtypePage', 'roles', '24', 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('92', 'FieldtypeEmail', 'email', '9', 'E-Mail Address', '{\"size\":70,\"maxlength\":255}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('97', 'FieldtypeModule', 'admin_theme', '8', 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('98', 'FieldtypeNotifications', 'notifications', '24', 'Notifications', '{\"collapsed\":2}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('99', 'FieldtypeFieldsetTabOpen', 'seo_tab', '0', 'SEO', '{\"tags\":\"SEO\",\"collapsed\":0,\"send_templates\":[1]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('100', 'FieldtypeFieldsetClose', 'seo_tab_END', '0', 'Close an open fieldset', '{\"tags\":\"SEO\",\"send_templates\":[1]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('101', 'FieldtypeText', 'seo_title', '0', 'SEO Title', '{\"description\":\"A good length for a title is 60 characters.\",\"tags\":\"SEO\",\"collapsed\":0,\"size\":0,\"maxlength\":2048,\"send_templates\":[44,45,1],\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('102', 'FieldtypeText', 'seo_keywords', '0', 'Keywords', '{\"tags\":\"SEO\",\"collapsed\":4,\"size\":0,\"maxlength\":2048,\"send_templates\":[44,45,1],\"columnWidth\":50,\"description\":\"Deprecated field and currently optional.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('103', 'FieldtypeText', 'seo_description', '0', 'SEO Description', '{\"description\":\"A good length for a description is 160 characters.\",\"tags\":\"SEO\",\"collapsed\":0,\"size\":0,\"maxlength\":2048,\"send_templates\":[44,45,1],\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('104', 'FieldtypeText', 'seo_image', '0', 'Image', '{\"description\":\"Please enter the URL (including \\\"http:\\/\\/...\\\") to a preview image.\",\"tags\":\"SEO\",\"collapsed\":0,\"size\":0,\"maxlength\":2048,\"send_templates\":[44,45,1],\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('105', 'FieldtypeText', 'seo_canonical', '0', 'Canonical Link', '{\"notes\":\"The URL should include \\\"http:\\/\\/...\\\".\",\"tags\":\"SEO\",\"collapsed\":2,\"size\":0,\"maxlength\":2048,\"send_templates\":[44,45,1],\"textformatters\":[\"TextformatterEntities\"],\"description\":\"You must use the full url path including the domain name.\\nNOTE: This canonical setting is on a per page basis and will override any site settings canonical url that has been configured.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('106', 'FieldtypeTextarea', 'seo_custom', '0', 'Custom Meta Tags', '{\"description\":\"If you want to add other meta tags, you can do it here.\",\"notes\":\"Please use this schema: name := content. One tag per line. Special characters are only allowed in the content part and get converted to HTML.\",\"tags\":\"SEO\",\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"collapsed\":2,\"rows\":5,\"send_templates\":[44,45,1]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('107', 'FieldtypeTextarea', 'body', '0', 'Body', '{\"textformatters\":[\"TextformatterAutoLinks\"],\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"collapsed\":0,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, Underline, Strike, Subscript, Superscript, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":0,\"usePurifier\":0,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"contentsCss\":\"\\/site\\/templates\\/_assets\\/css\\/ckeditor.css\",\"stylesSet\":\"mystyles:\\/site\\/modules\\/InputfieldCKEditor\\/mystyles.js\",\"extraPlugins\":[\"codemirror\",\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"Generic\",\"minlength\":0,\"maxlength\":0,\"showCount\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('108', 'FieldtypeText', 'headline', '1', 'Headline', '{\"collapsed\":0,\"columnWidth\":50,\"size\":0,\"maxlength\":2048,\"tags\":\"Generic\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('109', 'FieldtypeCheckbox', 'sitemap_ignore', '1', 'Disable SEO Visibility', '{\"description\":\"Hide this page and its children from the XML sitemap and search engine crawlers\",\"collapsed\":2,\"tags\":\"SEO\",\"columnWidth\":50,\"send_templates\":[45,1]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('110', 'FieldtypeImage', 'images', '0', 'Page Images', '{\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":2,\"unzip\":1,\"overwrite\":1,\"descriptionRows\":1,\"adminThumbs\":1,\"defaultGrid\":0,\"maxReject\":0,\"tags\":\"Generic\",\"columnWidth\":50,\"fileSchema\":2}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('111', 'FieldtypeURL', 'redirect', '0', 'Page Redirect', '{\"description\":\"If set, the current page will automatically redirect to the selected page below.\",\"noRelative\":0,\"addRoot\":0,\"collapsed\":2,\"size\":0,\"maxlength\":1024,\"placeholder\":\"\\/about\\/ or http:\\/\\/mysite.com\",\"tags\":\"SEO\",\"columnWidth\":50,\"send_templates\":[1]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('112', 'FieldtypeFile', 'files', '0', 'Page Documents & Files', '{\"extensions\":\"pdf doc docx xls xlsx\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"inputfieldClass\":\"InputfieldFile\",\"collapsed\":2,\"descriptionRows\":1,\"unzip\":1,\"overwrite\":1,\"tags\":\"Generic\",\"fileSchema\":2}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('113', 'FieldtypeText', 'company', '1', 'Company Name', '{\"description\":\"Used throughout the site for copyright disclosures, author declarations, short page title adjustments and various other template uses.\",\"collapsed\":0,\"size\":0,\"maxlength\":2048,\"tags\":\"Settings\",\"send_templates\":[46],\"columnWidth\":50,\"placeholder\":\"ABC Corp Inc.\",\"notes\":\"RECOMMENDED: Do not leave empty!\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('114', 'FieldtypeOptions', 'live', '0', 'Website Status', '{\"description\":\"Indicates whether the website is in production mode or development mode. Production mode enables search engine crawling & Google Analytics tracking when account number has been entered in the field below.\",\"inputfieldClass\":\"InputfieldRadios\",\"collapsed\":0,\"columnWidth\":50,\"tags\":\"Settings\",\"required\":1,\"send_templates\":[46]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('115', 'FieldtypeURL', 'canonical', '1', 'Canonical URL', '{\"textformatters\":[\"TextformatterEntities\"],\"noRelative\":1,\"addRoot\":0,\"collapsed\":0,\"columnWidth\":50,\"size\":0,\"maxlength\":1024,\"tags\":\"Settings\",\"send_templates\":[46],\"description\":\"Sitewide canonical URL configuration\",\"notes\":\"WARNING: Any canonical urls configured in individual pages will override this setting.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('116', 'FieldtypeText', 'google_analytics', '0', 'Google Analytics ID', '{\"collapsed\":0,\"size\":0,\"maxlength\":2048,\"tags\":\"Settings\",\"description\":\"Google Analytics code will be embedded if this field is populated.\",\"notes\":\"How to find your code: https:\\/\\/support.google.com\\/analytics\\/answer\\/1008080. It should look like: UA-XXXXXXX-X.\",\"textformatters\":[\"TextformatterEntities\"],\"placeholder\":\"UA-XXXXXXX-X\",\"columnWidth\":50,\"pattern\":\"\\\\bUA-\\\\d{4,10}-\\\\d{1,4}\\\\b\",\"send_templates\":[46]}');

DROP TABLE IF EXISTS `fieldtype_options`;
CREATE TABLE `fieldtype_options` (
  `fields_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  `title` text,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fields_id`,`option_id`),
  UNIQUE KEY `title` (`title`(255),`fields_id`),
  KEY `value` (`value`,`fields_id`),
  KEY `sort` (`sort`,`fields_id`),
  FULLTEXT KEY `title_value` (`title`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`) VALUES('114', '1', 'Development', 'dev', '0');
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`) VALUES('114', '2', 'Production', 'live', '1');

DROP TABLE IF EXISTS `forms`;
CREATE TABLE `forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `forms_entries`;
CREATE TABLE `forms_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forms_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forms_id` (`forms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('1', 'FieldtypeTextarea', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('2', 'FieldtypeNumber', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('3', 'FieldtypeText', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('4', 'FieldtypePage', '3', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('30', 'InputfieldForm', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('6', 'FieldtypeFile', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('7', 'ProcessPageEdit', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('10', 'ProcessLogin', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('12', 'ProcessPageList', '0', '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50,\"useBookmarks\":1,\"speed\":200}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('121', 'ProcessPageEditLink', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('14', 'ProcessPageSort', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('15', 'InputfieldPageListSelect', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('117', 'JqueryUI', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('17', 'ProcessPageAdd', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('125', 'SessionLoginThrottle', '11', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('122', 'InputfieldPassword', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('25', 'InputfieldAsmSelect', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('116', 'JqueryCore', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('27', 'FieldtypeModule', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('28', 'FieldtypeDatetime', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('29', 'FieldtypeEmail', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('108', 'InputfieldURL', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('32', 'InputfieldSubmit', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('33', 'InputfieldWrapper', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('34', 'InputfieldText', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('35', 'InputfieldTextarea', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('36', 'InputfieldSelect', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('37', 'InputfieldCheckbox', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('38', 'InputfieldCheckboxes', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('39', 'InputfieldRadios', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('40', 'InputfieldHidden', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('41', 'InputfieldName', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('43', 'InputfieldSelectMultiple', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('45', 'JqueryWireTabs', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('46', 'ProcessPage', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('47', 'ProcessTemplate', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('48', 'ProcessField', '32', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('50', 'ProcessModule', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('114', 'PagePermissions', '3', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('97', 'FieldtypeCheckbox', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('115', 'PageRender', '3', '{\"clearCache\":1}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('55', 'InputfieldFile', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('56', 'InputfieldImage', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('57', 'FieldtypeImage', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('60', 'InputfieldPage', '0', '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('61', 'TextformatterEntities', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('66', 'ProcessUser', '0', '{\"showFields\":[\"name\",\"email\",\"roles\"],\"maxAjaxQty\":25}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('67', 'MarkupAdminDataTable', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('68', 'ProcessRole', '0', '{\"showFields\":[\"name\"]}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('76', 'ProcessList', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('78', 'InputfieldFieldset', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('79', 'InputfieldMarkup', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('80', 'InputfieldEmail', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('89', 'FieldtypeFloat', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('83', 'ProcessPageView', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('84', 'FieldtypeInteger', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('85', 'InputfieldInteger', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('86', 'InputfieldPageName', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('87', 'ProcessHome', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('90', 'InputfieldFloat', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('94', 'InputfieldDatetime', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('98', 'MarkupPagerNav', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('129', 'ProcessPageEditImageSelect', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('103', 'JqueryTableSorter', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('104', 'ProcessPageSearch', '1', '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('105', 'FieldtypeFieldsetOpen', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('106', 'FieldtypeFieldsetClose', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('107', 'FieldtypeFieldsetTabOpen', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('109', 'ProcessPageTrash', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('111', 'FieldtypePageTitle', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('112', 'InputfieldPageTitle', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('113', 'MarkupPageArray', '3', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('131', 'InputfieldButton', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('133', 'FieldtypePassword', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('134', 'ProcessPageType', '33', '{\"showFields\":[]}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('135', 'FieldtypeURL', '1', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('136', 'ProcessPermission', '1', '{\"showFields\":[\"name\",\"title\"]}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('137', 'InputfieldPageListSelectMultiple', '0', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('138', 'ProcessProfile', '1', '{\"profileFields\":[\"pass\",\"email\",\"admin_theme\"]}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('139', 'SystemUpdater', '1', '{\"systemVersion\":15,\"coreVersion\":\"3.0.42\"}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('148', 'AdminThemeDefault', '10', '{\"colors\":\"classic\"}', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('149', 'InputfieldSelector', '42', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('150', 'ProcessPageLister', '32', '', '0000-00-00 00:00:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('151', 'JqueryMagnific', '1', '', '2014-07-21 07:21:45');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('155', 'InputfieldCKEditor', '0', '', '2014-07-25 10:26:17');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('156', 'MarkupHTMLPurifier', '0', '', '2014-07-25 10:26:17');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('159', 'ProcessRecentPages', '1', '', '2015-08-12 13:55:45');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('160', 'ProcessLogger', '1', '', '2015-08-12 13:55:54');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('161', 'InputfieldIcon', '0', '', '2015-08-12 13:55:54');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('162', 'AdminThemeReno', '14', '{\"colors\":\"\",\"avatar_field_user\":\"\",\"userFields_user\":\"name\",\"notices\":\"fa-bell\",\"home\":\"fa-home\",\"signout\":\"fa-power-off\",\"page\":\"fa-file-text\",\"setup\":\"fa-wrench\",\"module\":\"fa-briefcase\",\"access\":\"fa-unlock\",\"-dups\":[\"\\/site\\/modules\\/AdminThemeReno\\/AdminThemeReno.module\",\"\\/wire\\/modules\\/AdminTheme\\/AdminThemeReno\\/AdminThemeReno.module\"],\"-dups-use\":\"\\/site\\/modules\\/AdminThemeReno\\/AdminThemeReno.module\"}', '2015-08-12 13:59:27');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('163', 'FieldtypeOptions', '1', '', '2015-08-12 13:59:50');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('164', 'FieldtypePageTable', '3', '', '2015-08-12 13:59:59');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('165', 'InputfieldPageTable', '0', '', '2015-08-12 13:59:59');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('166', 'FieldtypeRepeater', '3', '{\"repeatersRootPageID\":1015}', '2015-08-12 14:00:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('167', 'InputfieldRepeater', '0', '', '2015-08-12 14:00:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('168', 'FieldtypeSelector', '1', '', '2015-08-12 14:00:10');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('169', 'InputfieldPageAutocomplete', '0', '', '2015-08-12 14:00:31');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('170', 'JqueryFancybox', '1', '', '2015-08-12 14:00:36');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('171', 'PagePathHistory', '3', '', '2015-08-12 14:00:47');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('172', 'PagePaths', '3', '', '2015-08-12 14:00:54');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('173', 'ProcessForgotPassword', '1', '', '2015-08-12 14:01:06');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('174', 'ProcessPageClone', '11', '', '2015-08-12 14:01:15');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('175', 'SystemNotifications', '2', '{\"disabled\":\"0\",\"trackEdits\":\"1\",\"reverse\":\"0\",\"activeHooks\":[],\"systemUserName\":\"admin\",\"updateDelay\":5000,\"dateFormat\":\"rel\",\"iconMessage\":\"check-square-o\",\"iconWarning\":\"exclamation-circle\",\"iconError\":\"exclamation-triangle\",\"ghostDelay\":1000,\"ghostDelayError\":2000,\"ghostOpacity\":0.85,\"ghostLimit\":20,\"ghostFadeSpeed\":\"fast\",\"ghostPos\":\"2\",\"placement\":\"0\"}', '2015-08-12 14:01:25');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('176', 'FieldtypeNotifications', '1', '', '2015-08-12 14:01:25');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('177', 'LazyCron', '3', '', '2015-08-12 14:01:47');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('179', 'JqueryDataTables', '1', '', '2015-08-12 14:04:24');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('180', 'ModulesManager', '1', '', '2015-08-12 14:04:34');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('244', 'ProcessWireUpgrade', '1', '', '2016-10-04 14:26:44');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('182', 'ProcessWireUpgradeCheck', '11', '{\"useLoginHook\":\"1\"}', '2015-08-12 14:05:25');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('183', 'FieldtypeMultiplier', '1', '', '2015-08-12 14:06:44');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('184', 'InputfieldMultiplier', '0', '', '2015-08-12 14:06:44');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('185', 'FieldtypeTable', '1', '', '2015-08-12 14:06:53');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('186', 'InputfieldTable', '0', '', '2015-08-12 14:06:53');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('187', 'FieldtypeTextareas', '1', '', '2015-08-12 14:07:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('188', 'InputfieldTextareas', '0', '', '2015-08-12 14:07:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('189', 'ProCache', '3', '{\"licenseKey\":\"PWPC3.dev3364.2d19f901603cb129d1180add1c6f0dfd7295361d\"}', '2015-08-12 14:07:11');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('190', 'ProcessProCache', '1', '', '2015-08-12 14:07:11');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('192', 'MarkupSEO', '2', '{\"includeTemplates\":[\"home\",\"default\",404],\"author\":\"\",\"sitename\":\"\",\"useParents\":\"\",\"title\":\"\",\"titleSmart\":[],\"keywords\":\"\",\"keywordsSmart\":[],\"description\":\"\",\"descriptionSmart\":[],\"image\":\"\",\"imageSmart\":[],\"titleFormat\":\"{title} | {sitename}\",\"canonicalProtocol\":\"auto\",\"custom\":\"\",\"robots\":[\"noindex\",\"nofollow\",\"noarchive\",\"nosnippet\"],\"hardLimit\":\"\",\"titleLimit\":60,\"descriptionLimit\":160,\"includeGenerator\":\"\",\"includeOpenGraph\":1,\"includeTwitter\":1,\"twitterUsername\":\"\",\"method\":\"manual\",\"addWhitespace\":1,\"googleAnalytics\":\"UA-XXXXXXX-X\",\"piwikAnalyticsUrl\":\"\",\"piwikAnalyticsIDSite\":\"\",\"googleAnalyticsAnonymizeIP\":\"\"}', '2015-08-12 14:08:28');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('193', 'FormBuilder', '3', '{\"licenseKey\":\"PWFB3.dev3364.e6154245e8a0d80b1160ab3f4e74e8337258cf60\",\"inputfieldClasses\":[\"AsmSelect\",\"Checkbox\",\"Checkboxes\",\"Datetime\",\"Email\",\"Fieldset\",\"Float\",\"FormBuilderFile\",\"Integer\",\"Hidden\",\"Page\",\"Radios\",\"Select\",\"SelectMultiple\",\"Text\",\"Textarea\",\"URL\"],\"embedFields\":[],\"embedTag\":\"form-builder\",\"markup_list\":\"<div {attrs}>{out}\\n<\\/div>\",\"markup_item\":\"<div {attrs}>{out}\\n<\\/div>\",\"markup_item_label\":\"<label class=\'ui-widget-header\' for=\'{for}\'>{out}<\\/label>\",\"markup_item_content\":\"<div class=\'ui-widget-content\'>{out}<\\/div>\",\"markup_item_error\":\"<p><span class=\'ui-state-error\'>{out}<\\/span><\\/p>\",\"markup_item_description\":\"<p class=\'description\'>{out}<\\/p>\",\"markup_success\":\"<p class=\'ui-state-highlight\'>{out}<\\/p>\",\"markup_error\":\"<p class=\'ui-state-error\'>{out}<\\/p>\",\"classes_form\":\"\",\"classes_list\":\"Inputfields\",\"classes_list_clearfix\":\"ui-helper-clearfix\",\"classes_item\":\"Inputfield Inputfield_{name} ui-widget {class}\",\"classes_item_required\":\"InputfieldStateRequired\",\"classes_item_error\":\"InputfieldStateError ui-state-error\",\"classes_item_collapsed\":\"InputfieldStateCollapsed\",\"classes_item_column_width\":\"InputfieldColumnWidth\",\"classes_item_column_width_first\":\"InputfieldColumnWidthFirst\",\"akismetKey\":\"\",\"csvDelimiter\":\",\",\"filesPath\":\"{config.paths.cache}form-builder\\/\",\"useRoles\":\"\"}', '2015-08-12 14:11:52');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('194', 'ProcessFormBuilder', '1', '', '2015-08-12 14:11:52');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('195', 'InputfieldFormBuilderFile', '0', '', '2015-08-12 14:11:52');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('198', 'TextformatterAutoLinks', '1', '', '2015-08-12 14:16:18');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('199', 'FieldtypeCache', '1', '', '2015-08-12 14:47:46');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('200', 'MarkupSitemapXML', '3', '', '2015-08-12 14:47:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('201', 'MarkupCache', '3', '', '2015-08-12 14:47:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('202', 'ProcessRedirects', '3', '', '2015-08-12 15:00:57');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('203', 'ProcessDatabaseBackups', '1', '', '2015-08-12 15:05:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('240', 'FileCompilerTags', '0', '{\"runOrder\":0,\"openTag\":\"{\",\"closeTag\":\"}\"}', '2016-08-24 14:30:02');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('207', 'BatchChildEditor', '10', '{\"enabledTemplates\":[],\"enabledPages\":[],\"configurablePages\":[],\"editModes\":[\"edit\",\"add\",\"update\",\"replace\",\"export\"],\"defaultMode\":\"\",\"overwriteNames\":1,\"allowOverrideOverwriteNames\":1,\"disableContentProtection\":\"\",\"allowTemplateChanges\":1,\"trashOrDelete\":\"trash\",\"csvOptions\":[\"paste\"],\"csvOptionsCollapsed\":[],\"csvImportFieldSeparator\":\",\",\"csvImportFieldEnclosure\":\"\\\"\",\"ignoreFirstRow\":\"\",\"allowOverrideCsvImportSettings\":\"\",\"csvExportFieldSeparator\":\",\",\"csvExportFieldEnclosure\":\"\\\"\",\"csvExportExtension\":\"csv\",\"columnsFirstRow\":\"\",\"multipleValuesSeparator\":\"|\",\"allowOverrideCsvExportSettings\":\"\",\"position\":\"bottom\",\"loadOpen\":\"\",\"tabName\":\"Batch Child Editor\",\"editModeTitle\":\"Edit Child Pages\",\"editModeDescription\":\"You can edit the page titles, sort, delete, add new, or edit pages in a modal popup.\",\"editModeNotes\":\"\",\"addModeTitle\":\"Add Child Pages\",\"addModeDescription\":\"Editing this field will add all the child page titles listed here to the existing set of child pages.\",\"addModeNotes\":\"Each row is a separate page.\\n\\nYou can also use CSV formatted lines for populating all text\\/numeric fields on the page, eg:\\n\\\"Bolivia, Plurinational State of\\\",BO,\\\"BOLIVIA, PLURINATIONAL STATE OF\\\",BOL,68\",\"updateModeTitle\":\"Update Child Pages\",\"updateModeDescription\":\"Editing this field will update the field values of the pages represented here.\",\"updateModeNotes\":\"WARNING: If you use this option, the content of all fields in existing pages will be replaced.\\n\\nEach row is a separate page.\\n\\nYou can also use CSV formatted lines for populating all text\\/numeric fields on the page, eg:\\n\\\"Bolivia, Plurinational State of\\\",BO,\\\"BOLIVIA, PLURINATIONAL STATE OF\\\",BOL,68\",\"replaceModeTitle\":\"Replace Child Pages\",\"replaceModeDescription\":\"Editing this field will replace all the child page titles represented here.\",\"replaceModeNotes\":\"WARNING: If you use this option, all the existing child pages (and grandchildren) will be deleted and new ones created.\\n\\nEach row is a separate page.\\n\\nYou can also use CSV formatted lines for populating all text\\/numeric fields on the page, eg:\\n\\\"Bolivia, Plurinational State of\\\",BO,\\\"BOLIVIA, PLURINATIONAL STATE OF\\\",BOL,68\",\"exportModeTitle\":\"Export Child Pages\",\"exportModeDescription\":\"Creates a CSV file with one row for each child page.\",\"exportModeNotes\":\"\",\"allowAdminPages\":\"\",\"listerDefaultSort\":\"sort\",\"listerColumns\":[\"title\",\"path\",\"modified\",\"modified_users_id\"],\"defaultFilter\":\"\",\"listerConfigurable\":null,\"formatExport\":1,\"insertAfterField\":false,\"removeChildrenTab\":null,\"openMethod\":\"ajax\",\"listerModeTitle\":\"List Child Pages\",\"listerModeDescription\":\"View child pages in a Lister interface.\",\"listerModeNotes\":\"\",\"importMultipleValuesSeparator\":\"|\",\"exportMultipleValuesSeparator\":\"|\",\"newImageFirst\":\"\"}', '2015-08-12 15:07:15');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('208', 'ProcessChildrenCsvExport', '1', '', '2015-08-12 15:07:15');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('209', 'ProtectedMode', '3', '{\"protectedMode\":\"\",\"message\":\"This site is currently in maintenance mode. You must log in to view the site.\",\"logincss\":\".protected-mode-container {\\n    width: 400px;\\n    max-width: 100%;\\n    height: 150px;\\n    margin: auto;\\n    position: absolute;\\n    top: 0;\\n    left: 0;\\n    bottom: 0;\\n    right: 0;\\n}\\n\\np, legend {\\n    font-family: Arial, Helvetica, sans-serif;\\n    display: block;\\n    width: 100%;\\n    margin-bottom: 1rem;\\n    color: #6F6F6F;\\n}\\n\\nbutton {\\n    font-family: Arial, Helvetica, sans-serif;\\n    font-size: 100%;\\n    padding: 0.5em 1em;\\n    background-color: #006DD3;\\n    color:#fff;\\n    text-decoration: none;\\n    border: 0 rgba(0,0,0,0);\\n    border-radius: 2px;\\n}\\nbutton:hover,\\nbutton:focus {\\n    background-color: #007DD2;\\n}\\nbutton:focus {\\n    outline: 0;\\n}\\n\\ninput[type=\'text\'],\\ninput[type=\'password\'] {\\n    font-size: 100%;\\n    padding: 0.5rem;\\n    display: inline-block;\\n    border: 1px solid #ccc;\\n    box-shadow: inset 0 1px 3px #ddd;\\n    border-radius: 4px;\\n    -webkit-box-sizing: border-box;\\n    -moz-box-sizing: border-box;\\n    box-sizing: border-box;\\n}\"}', '2015-08-12 15:14:34');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('210', 'ProcessWireConfig', '1', '{\"customConfig\":\"site_title = text = Site Title = You can access this property with $config->site_title\",\"chmodConfig\":\"0644\"}', '2015-08-12 15:18:30');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('211', 'CronjobDatabaseBackup', '3', '{\"cycle\":\"every6Hours\",\"max\":30,\"deadline\":\"\",\"backup_name\":\"\",\"field_storage_path\":\"\",\"backup_all\":1,\"tables\":{\"ProcessRedirects\":\"ProcessRedirects\",\"caches\":\"caches\",\"field_admin_theme\":\"field_admin_theme\",\"field_body\":\"field_body\",\"field_email\":\"field_email\",\"field_file\":\"field_file\",\"field_headline\":\"field_headline\",\"field_images\":\"field_images\",\"field_notifications\":\"field_notifications\",\"field_pass\":\"field_pass\",\"field_permissions\":\"field_permissions\",\"field_process\":\"field_process\",\"field_redirect\":\"field_redirect\",\"field_roles\":\"field_roles\",\"field_seo_canonical\":\"field_seo_canonical\",\"field_seo_custom\":\"field_seo_custom\",\"field_seo_description\":\"field_seo_description\",\"field_seo_image\":\"field_seo_image\",\"field_seo_keywords\":\"field_seo_keywords\",\"field_seo_tab\":\"field_seo_tab\",\"field_seo_tab_end\":\"field_seo_tab_end\",\"field_seo_title\":\"field_seo_title\",\"field_sitemap_ignore\":\"field_sitemap_ignore\",\"field_title\":\"field_title\",\"fieldgroups\":\"fieldgroups\",\"fieldgroups_fields\":\"fieldgroups_fields\",\"fields\":\"fields\",\"fieldtype_options\":\"fieldtype_options\",\"forms\":\"forms\",\"forms_entries\":\"forms_entries\",\"hanna_code\":\"hanna_code\",\"modules\":\"modules\",\"page_path_history\":\"page_path_history\",\"pages\":\"pages\",\"pages_access\":\"pages_access\",\"pages_parents\":\"pages_parents\",\"pages_paths\":\"pages_paths\",\"pages_procache\":\"pages_procache\",\"pages_sortfields\":\"pages_sortfields\",\"process_changelog\":\"process_changelog\",\"process_forgot_password\":\"process_forgot_password\",\"session_login_throttle\":\"session_login_throttle\",\"templates\":\"templates\"},\"cleanup\":\"\",\"backup_fileinfo\":\"\"}', '2015-08-12 15:22:45');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('212', 'ProcessDiagnostics', '1', '', '2015-08-12 15:30:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('213', 'DiagnosePhp', '1', '', '2015-08-12 15:31:04');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('214', 'DiagnoseDatabase', '1', '', '2015-08-12 15:31:19');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('215', 'DiagnoseFiles', '1', '', '2015-08-12 15:31:33');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('217', 'DiagnoseWebserver', '1', '', '2015-08-12 15:31:45');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('218', 'DiagnoseImagehandling', '1', '', '2015-08-12 15:35:50');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('219', 'PageReferencesTab', '2', '{\"enabledTemplates\":[]}', '2015-08-13 13:47:08');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('231', 'ProcessPageCloneAdaptUrls', '2', '', '2016-03-23 15:10:23');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('243', 'ProcessChangelogHooks', '3', '{\"schema_version\":2}', '2016-08-24 14:30:42');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('228', 'SessionHandlerDB', '3', '', '2015-09-23 21:18:01');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('229', 'ProcessSessionDB', '1', '', '2015-09-23 21:18:01');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('242', 'ProcessChangelog', '1', '{\"date_format\":\"Y-m-d H:i:s\",\"row_limit\":\"25\",\"row_label\":\"name\",\"visible_filters\":[\"operation\",\"username\",\"when\",\"date_range\"],\"default_for_operation\":\"\",\"default_for_flags\":\"\",\"default_for_when\":\"\"}', '2016-08-24 14:30:42');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('237', 'ProDrafts', '3', '{\"licenseKey\":\"PWPD3.dev3364.b7b7563e8d906ca6f26bb27e706f4e69256c730e\",\"draftsMode\":\"1\",\"templateIDs\":[],\"confirmPublish\":\"0\",\"autosave\":\"0\",\"autosaveNotField\":[],\"autosaveNotRegex\":\"\\/^delete_\\/\\n\\/_delete$\\/\",\"autosaveInterval\":1000,\"autosaveDelay\":500,\"livePreview\":0,\"maxVersions\":\"0\"}', '2016-04-06 16:33:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('238', 'ProcessProDrafts', '1', '', '2016-04-06 16:33:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('246', 'EditorSkinLightwire', '32', '', '2016-11-11 14:48:07');

DROP TABLE IF EXISTS `page_path_history`;
CREATE TABLE `page_path_history` (
  `path` varchar(255) NOT NULL,
  `pages_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`path`),
  KEY `pages_id` (`pages_id`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM AUTO_INCREMENT=1163 DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1', '0', '1', 'home', '9', '2016-11-11 11:57:57', '41', '0000-00-00 00:00:00', '2', '2016-11-11 11:57:57', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('2', '1', '2', 'manage', '1035', '2015-08-12 13:55:45', '40', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('3', '2', '2', 'page', '21', '2011-03-29 21:37:06', '41', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('6', '3', '2', 'add', '21', '2015-08-12 13:56:01', '40', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('7', '1', '2', 'trash', '1039', '2011-08-14 22:04:52', '41', '2010-02-07 05:29:39', '2', '2010-02-07 05:29:39', '15');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('8', '3', '2', 'list', '21', '2016-03-18 16:47:25', '41', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('9', '3', '2', 'sort', '1047', '2011-03-29 21:37:06', '41', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('10', '3', '2', 'edit', '1045', '2016-03-07 22:38:23', '40', '0000-00-00 00:00:00', '2', '0000-00-00 00:00:00', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('11', '22', '2', 'template', '21', '2011-03-29 21:37:06', '41', '2010-02-01 11:04:54', '2', '2010-02-01 11:04:54', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('16', '22', '2', 'field', '21', '2011-03-29 21:37:06', '41', '2010-02-01 12:44:07', '2', '2010-02-01 12:44:07', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('21', '2', '2', 'module', '21', '2011-03-29 21:37:06', '41', '2010-02-02 10:02:24', '2', '2010-02-02 10:02:24', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('22', '2', '2', 'setup', '21', '2015-08-13 16:31:33', '41', '2010-02-09 12:16:59', '2', '2010-02-09 12:16:59', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('23', '2', '2', 'login', '1035', '2011-05-03 23:38:10', '41', '2010-02-17 09:59:39', '2', '2010-02-17 09:59:39', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('27', '1', '44', 'http404', '1035', '2015-08-13 15:53:14', '41', '2010-06-03 06:53:03', '3', '2010-06-03 06:53:03', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('28', '2', '2', 'access', '13', '2011-05-03 23:38:10', '41', '2011-03-19 19:14:20', '2', '2011-03-19 19:14:20', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('29', '28', '2', 'users', '29', '2011-04-05 00:39:08', '41', '2011-03-19 19:15:29', '2', '2011-03-19 19:15:29', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('30', '28', '2', 'roles', '29', '2011-04-05 00:38:39', '41', '2011-03-19 19:15:45', '2', '2011-03-19 19:15:45', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('31', '28', '2', 'permissions', '29', '2011-04-05 00:53:52', '41', '2011-03-19 19:16:00', '2', '2011-03-19 19:16:00', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('32', '31', '5', 'page-edit', '25', '2011-09-06 15:34:24', '41', '2011-03-19 19:17:03', '2', '2011-03-19 19:17:03', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('34', '31', '5', 'page-delete', '25', '2011-09-06 15:34:33', '41', '2011-03-19 19:17:23', '2', '2011-03-19 19:17:23', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('35', '31', '5', 'page-move', '25', '2011-09-06 15:34:48', '41', '2011-03-19 19:17:41', '2', '2011-03-19 19:17:41', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('36', '31', '5', 'page-view', '25', '2011-09-06 15:34:14', '41', '2011-03-19 19:17:57', '2', '2011-03-19 19:17:57', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('37', '30', '4', 'guest', '25', '2011-04-05 01:37:19', '41', '2011-03-19 19:18:41', '2', '2011-03-19 19:18:41', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('38', '30', '4', 'superuser', '25', '2016-04-01 16:20:38', '41', '2011-03-19 19:18:55', '2', '2011-03-19 19:18:55', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('41', '29', '3', 'admin', '1', '2015-08-12 14:59:38', '41', '2011-03-19 19:41:26', '2', '2011-03-19 19:41:26', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('40', '29', '3', 'guest', '25', '2011-08-17 14:26:09', '41', '2011-03-20 17:31:59', '2', '2011-03-20 17:31:59', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('50', '31', '5', 'page-sort', '25', '2011-09-06 15:34:58', '41', '2011-03-26 22:04:50', '41', '2011-03-26 22:04:50', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('51', '31', '5', 'page-template', '25', '2011-09-06 15:35:09', '41', '2011-03-26 22:25:31', '41', '2011-03-26 22:25:31', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('52', '31', '5', 'user-admin', '25', '2011-09-06 15:35:42', '41', '2011-03-30 00:06:47', '41', '2011-03-30 00:06:47', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('53', '31', '5', 'profile-edit', '1', '2011-08-16 22:32:48', '41', '2011-04-26 00:02:22', '41', '2011-04-26 00:02:22', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('54', '31', '5', 'page-lock', '1', '2011-08-15 17:48:12', '41', '2011-08-15 17:45:48', '41', '2011-08-15 17:45:48', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('300', '3', '2', 'search', '1045', '2011-03-29 21:37:06', '41', '2010-08-04 05:23:59', '2', '2010-08-04 05:23:59', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('301', '3', '2', 'trash', '1047', '2011-03-29 21:37:06', '41', '2010-09-28 05:39:30', '2', '2010-09-28 05:39:30', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('302', '3', '2', 'link', '1041', '2011-03-29 21:37:06', '41', '2010-10-01 05:03:56', '2', '2010-10-01 05:03:56', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('303', '3', '2', 'image', '1041', '2011-03-29 21:37:06', '41', '2010-10-13 03:56:48', '2', '2010-10-13 03:56:48', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('304', '2', '2', 'profile', '1025', '2011-05-03 23:38:10', '41', '2011-04-25 23:57:18', '41', '2011-04-25 23:57:18', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1006', '31', '5', 'page-lister', '1', '2014-07-20 09:00:38', '40', '2014-07-20 09:00:38', '40', '2014-07-20 09:00:38', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1007', '3', '2', 'lister', '1', '2014-07-20 09:00:38', '40', '2014-07-20 09:00:38', '40', '2014-07-20 09:00:38', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1010', '3', '2', 'recent-pages', '1', '2015-08-12 13:55:45', '40', '2015-08-12 13:55:45', '40', '2015-08-12 13:55:45', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1011', '31', '5', 'page-edit-recent', '1', '2015-08-12 13:55:45', '40', '2015-08-12 13:55:45', '40', '2015-08-12 13:55:45', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1012', '22', '2', 'logs', '1', '2015-08-12 15:12:49', '41', '2015-08-12 13:55:54', '40', '2015-08-12 13:55:54', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1013', '31', '5', 'logs-view', '1', '2015-08-12 13:55:54', '40', '2015-08-12 13:55:54', '40', '2015-08-12 13:55:54', '11');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1014', '31', '5', 'logs-edit', '1', '2015-08-12 13:55:54', '40', '2015-08-12 13:55:54', '40', '2015-08-12 13:55:54', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1015', '2', '2', 'repeaters', '1036', '2015-08-12 14:00:05', '41', '2015-08-12 14:00:05', '41', '2015-08-12 14:00:05', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1016', '3', '2', 'clone', '1024', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1017', '31', '5', 'page-clone', '1', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1018', '31', '5', 'page-clone-tree', '1', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '41', '2015-08-12 14:01:15', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1019', '22', '2', 'modulesmanager', '1', '2015-08-13 16:31:29', '41', '2015-08-12 14:04:34', '41', '2015-08-12 14:04:34', '13');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1153', '22', '2', 'upgrades', '1', '2016-10-04 14:26:44', '41', '2016-10-04 14:26:44', '41', '2016-10-04 14:26:44', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1021', '22', '2', 'procache', '1', '2015-08-13 16:30:40', '41', '2015-08-12 14:07:11', '41', '2015-08-12 14:07:11', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1023', '31', '5', 'batcher', '1', '2015-08-12 14:07:57', '41', '2015-08-12 14:07:57', '41', '2015-08-12 14:07:57', '15');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1024', '1', '43', 'form-builder', '1025', '2015-08-13 16:23:25', '41', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1025', '31', '5', 'form-builder', '1', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '16');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1026', '31', '5', 'form-builder-add', '1', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '17');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1027', '22', '2', 'form-builder', '1', '2015-08-12 15:12:01', '41', '2015-08-12 14:11:52', '41', '2015-08-12 14:11:52', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1147', '31', '5', 'user-admin-all', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '27');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1146', '31', '5', 'page-rename', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '26');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1145', '31', '5', 'page-publish', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '25');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1144', '31', '5', 'page-hide', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '24');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1032', '22', '2', 'redirects', '1', '2015-08-12 15:12:42', '41', '2015-08-12 15:00:57', '41', '2015-08-12 15:00:57', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1033', '22', '2', 'db-backups', '1', '2015-08-13 16:31:06', '41', '2015-08-12 15:05:41', '41', '2015-08-12 15:05:41', '12');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1034', '31', '5', 'db-backup', '1', '2015-08-12 15:05:41', '41', '2015-08-12 15:05:41', '41', '2015-08-12 15:05:41', '21');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1151', '22', '2', 'changelog', '1', '2016-08-24 14:30:42', '41', '2016-08-24 14:30:42', '41', '2016-08-24 14:30:42', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1152', '31', '5', 'changelog', '1', '2016-08-24 14:30:42', '41', '2016-08-24 14:30:42', '41', '2016-08-24 14:30:42', '29');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1038', '31', '5', 'batch-child-editor', '1', '2015-08-12 15:07:15', '41', '2015-08-12 15:07:15', '41', '2015-08-12 15:07:15', '23');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1039', '22', '2', 'children-csv-export', '1024', '2015-08-12 15:07:15', '41', '2015-08-12 15:07:15', '41', '2015-08-12 15:07:15', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1040', '22', '2', 'config', '1', '2015-08-13 16:31:01', '41', '2015-08-12 15:18:30', '41', '2015-08-12 15:18:30', '15');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1041', '31', '5', 'config-edit', '1', '2015-08-12 15:18:30', '41', '2015-08-12 15:18:30', '41', '2015-08-12 15:18:30', '24');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1042', '22', '2', 'processdiagnostics', '1', '2015-08-13 16:30:58', '41', '2015-08-12 15:30:41', '41', '2015-08-12 15:30:41', '14');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1043', '30', '4', 'webmaster', '1', '2015-08-12 15:52:20', '41', '2015-08-12 15:51:20', '41', '2015-08-12 15:51:20', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1044', '1', '46', 'settings', '1025', '2016-03-23 15:33:11', '41', '2015-08-13 10:22:57', '41', '2015-08-13 10:22:57', '11');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1078', '22', '2', 'sessions-db', '1', '2015-09-23 21:18:01', '41', '2015-09-23 21:18:01', '41', '2015-09-23 21:18:01', '17');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1079', '1', '45', 'about', '1', '2016-03-23 15:07:31', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1080', '1', '45', 'products', '1', '2016-03-23 15:07:32', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1081', '1', '45', 'services', '1', '2016-03-23 15:07:38', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1082', '1', '45', 'markets-served', '1', '2016-03-23 15:07:44', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1083', '1', '45', 'news', '1', '2016-03-23 15:07:51', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1084', '1', '45', 'contact-us', '1', '2016-03-23 15:07:55', '41', '2015-09-30 21:45:44', '41', '2015-09-30 21:45:44', '10');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1085', '1079', '45', 'blog', '1', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1086', '1079', '45', 'our-company', '1', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1087', '1079', '45', 'careers', '1', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '41', '2015-09-30 21:47:13', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1088', '1080', '45', 'product-category-1', '1', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1089', '1080', '45', 'product-category-2', '1', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1090', '1080', '45', 'product-category-3', '1', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1091', '1080', '45', 'product-category-4', '1', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1092', '1080', '45', 'product-category-5', '1', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '41', '2015-09-30 21:48:48', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1093', '1088', '45', 'sub-category-1', '1', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1094', '1088', '45', 'sub-category-2', '1', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1095', '1088', '45', 'sub-category-3', '1', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '41', '2015-09-30 21:49:13', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1096', '1089', '45', 'sub-category-1', '1', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1097', '1089', '45', 'sub-category-2', '1', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1098', '1089', '45', 'sub-category-3', '1', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1099', '1089', '45', 'sub-category-4', '1', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '41', '2015-09-30 21:49:39', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1100', '1090', '45', 'sub-category-1', '1', '2015-09-30 21:49:49', '41', '2015-09-30 21:49:49', '41', '2015-09-30 21:49:49', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1101', '1091', '45', 'sub-category-1', '1', '2015-09-30 21:50:04', '41', '2015-09-30 21:50:04', '41', '2015-09-30 21:50:04', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1102', '1091', '45', 'sub-category-2', '1', '2015-09-30 21:50:04', '41', '2015-09-30 21:50:04', '41', '2015-09-30 21:50:04', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1103', '1092', '45', 'sub-category-1', '1', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1104', '1092', '45', 'sub-category-2', '1', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1105', '1092', '45', 'sub-category-3', '1', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1106', '1092', '45', 'sub-category-4', '1', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1107', '1092', '45', 'sub-category-5', '1', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '41', '2015-09-30 21:50:25', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1108', '1081', '45', 'service-category-1', '1', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1109', '1081', '45', 'service-category-2', '1', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1110', '1081', '45', 'service-category-3', '1', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '41', '2015-09-30 21:50:51', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1111', '1082', '45', 'market-1', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1112', '1082', '45', 'market-2', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1113', '1082', '45', 'market-3', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1114', '1082', '45', 'market-4', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1115', '1082', '45', 'market-5', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1116', '1082', '45', 'market-6', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1117', '1082', '45', 'market-7', '1', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '41', '2015-09-30 21:51:39', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1118', '1084', '45', 'sales', '1', '2015-09-30 21:52:20', '41', '2015-09-30 21:52:20', '41', '2015-09-30 21:52:20', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1119', '1084', '45', 'customer-service', '1', '2015-09-30 21:52:20', '41', '2015-09-30 21:52:20', '41', '2015-09-30 21:52:20', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1120', '1084', '45', 'technical-support', '1', '2017-02-06 14:49:17', '41', '2015-09-30 21:52:20', '41', '2017-02-06 14:35:19', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1121', '1093', '45', 'product-1', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1122', '1093', '45', 'product-2', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1123', '1093', '45', 'product-3', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1124', '1093', '45', 'product-4', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1125', '1093', '45', 'product-5', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1126', '1093', '45', 'product-6', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '5');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1127', '1093', '45', 'product-7', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '6');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1128', '1093', '45', 'product-8', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '7');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1129', '1093', '45', 'product-9', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '8');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1130', '1093', '45', 'product-10', '1', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '41', '2015-09-30 21:53:06', '9');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1131', '1094', '45', 'product-1', '1', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1132', '1094', '45', 'product-2', '1', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1133', '1094', '45', 'product-3', '1', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1134', '1094', '45', 'product-4', '1', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1135', '1094', '45', 'product-5', '1', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '41', '2015-09-30 21:53:28', '4');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1136', '1095', '45', 'product-1', '1', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1137', '1095', '45', 'product-2', '1', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1138', '1095', '45', 'product-3', '1', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '2');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1139', '1095', '45', 'product-4', '1', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '41', '2015-09-30 21:53:54', '3');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1143', '31', '5', 'page-edit-images', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '23');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1142', '31', '5', 'page-edit-created', '1', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '41', '2016-04-01 16:19:37', '22');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1148', '31', '5', 'user-admin-webmaster', '1', '2016-04-01 16:19:38', '41', '2016-04-01 16:19:38', '41', '2016-04-01 16:19:38', '28');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1149', '31', '5', 'tracy-debugger', '1', '2016-04-01 16:19:55', '41', '2016-04-01 16:19:47', '41', '2016-04-01 16:19:55', '29');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`) VALUES('1150', '3', '2', 'prodrafts', '1', '2016-04-06 16:33:41', '41', '2016-04-06 16:33:41', '41', '2016-04-06 16:33:41', '11');

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('37', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('38', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1043', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('32', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('34', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('35', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('36', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('50', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('51', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('52', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('53', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('54', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1006', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1011', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1013', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1014', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1017', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1018', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1023', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1025', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1026', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1144', '2', '2016-04-01 16:19:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1143', '2', '2016-04-01 16:19:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1142', '2', '2016-04-01 16:19:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1034', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1152', '2', '2016-08-24 14:30:42');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1038', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1041', '2', '2015-08-13 10:22:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1079', '1', '2016-03-23 15:07:31');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1080', '1', '2016-03-23 15:07:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1081', '1', '2016-03-23 15:07:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1082', '1', '2016-03-23 15:07:44');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1083', '1', '2016-03-23 15:07:51');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1084', '1', '2016-03-23 15:07:55');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1149', '2', '2016-04-01 16:19:47');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1148', '2', '2016-04-01 16:19:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1147', '2', '2016-04-01 16:19:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1146', '2', '2016-04-01 16:19:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1145', '2', '2016-04-01 16:19:37');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int(10) unsigned NOT NULL,
  `parents_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('2', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1079', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1080', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1081', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1082', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1084', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1088', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1088', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1090', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1090', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1091', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1091', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1092', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1092', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1093', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1093', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1093', '1088');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '1088');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1095', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1095', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1095', '1088');

DROP TABLE IF EXISTS `pages_paths`;
CREATE TABLE `pages_paths` (
  `pages_id` int(10) unsigned NOT NULL,
  `path` text CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`pages_id`),
  UNIQUE KEY `path` (`path`(500)),
  FULLTEXT KEY `path_fulltext` (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1', '');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('2', 'manage');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('3', 'manage/page');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('6', 'manage/page/add');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('8', 'manage/page/list');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('9', 'manage/page/sort');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('10', 'manage/page/edit');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('300', 'manage/page/search');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('301', 'manage/page/trash');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('302', 'manage/page/link');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('303', 'manage/page/image');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1007', 'manage/page/lister');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1010', 'manage/page/recent-pages');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('21', 'manage/module');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('22', 'manage/setup');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('11', 'manage/setup/template');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('16', 'manage/setup/field');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1012', 'manage/setup/logs');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('23', 'manage/login');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('28', 'manage/access');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('29', 'manage/access/users');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('40', 'manage/access/users/guest');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('41', 'manage/access/users/admin');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('30', 'manage/access/roles');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('37', 'manage/access/roles/guest');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('38', 'manage/access/roles/superuser');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('31', 'manage/access/permissions');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('32', 'manage/access/permissions/page-edit');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('34', 'manage/access/permissions/page-delete');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('35', 'manage/access/permissions/page-move');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('36', 'manage/access/permissions/page-view');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('50', 'manage/access/permissions/page-sort');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('51', 'manage/access/permissions/page-template');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('52', 'manage/access/permissions/user-admin');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('53', 'manage/access/permissions/profile-edit');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('54', 'manage/access/permissions/page-lock');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1006', 'manage/access/permissions/page-lister');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1011', 'manage/access/permissions/page-edit-recent');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1013', 'manage/access/permissions/logs-view');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1014', 'manage/access/permissions/logs-edit');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('304', 'manage/profile');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1015', 'manage/repeaters');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('7', 'trash');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('27', 'http404');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1016', 'manage/page/clone');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1017', 'manage/access/permissions/page-clone');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1018', 'manage/access/permissions/page-clone-tree');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1019', 'manage/setup/modulesmanager');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1153', 'manage/setup/upgrades');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1021', 'manage/setup/procache');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1023', 'manage/access/permissions/batcher');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1024', 'form-builder');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1025', 'manage/access/permissions/form-builder');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1026', 'manage/access/permissions/form-builder-add');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1027', 'manage/setup/form-builder');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1146', 'manage/access/permissions/page-rename');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1145', 'manage/access/permissions/page-publish');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1144', 'manage/access/permissions/page-hide');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1032', 'manage/setup/redirects');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1033', 'manage/setup/db-backups');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1034', 'manage/access/permissions/db-backup');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1038', 'manage/access/permissions/batch-child-editor');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1039', 'manage/setup/children-csv-export');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1040', 'manage/setup/config');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1041', 'manage/access/permissions/config-edit');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1042', 'manage/setup/processdiagnostics');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1043', 'manage/access/roles/webmaster');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1044', 'settings');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1151', 'manage/setup/changelog');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1152', 'manage/access/permissions/changelog');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1078', 'manage/setup/sessions-db');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1079', 'about');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1080', 'products');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1081', 'services');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1082', 'markets-served');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1083', 'news');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1084', 'contact-us');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1085', 'about/blog');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1086', 'about/our-company');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1087', 'about/careers');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1088', 'products/product-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1089', 'products/product-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1090', 'products/product-category-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1091', 'products/product-category-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1092', 'products/product-category-5');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1093', 'products/product-category-1/sub-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1094', 'products/product-category-1/sub-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1095', 'products/product-category-1/sub-category-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1096', 'products/product-category-2/sub-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1097', 'products/product-category-2/sub-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1098', 'products/product-category-2/sub-category-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1099', 'products/product-category-2/sub-category-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1100', 'products/product-category-3/sub-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1101', 'products/product-category-4/sub-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1102', 'products/product-category-4/sub-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1103', 'products/product-category-5/sub-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1104', 'products/product-category-5/sub-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1105', 'products/product-category-5/sub-category-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1106', 'products/product-category-5/sub-category-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1107', 'products/product-category-5/sub-category-5');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1108', 'services/service-category-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1109', 'services/service-category-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1110', 'services/service-category-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1111', 'markets-served/market-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1112', 'markets-served/market-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1113', 'markets-served/market-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1114', 'markets-served/market-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1115', 'markets-served/market-5');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1116', 'markets-served/market-6');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1117', 'markets-served/market-7');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1118', 'contact-us/sales');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1119', 'contact-us/customer-service');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1120', 'contact-us/technical-support');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1121', 'products/product-category-1/sub-category-1/product-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1122', 'products/product-category-1/sub-category-1/product-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1123', 'products/product-category-1/sub-category-1/product-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1124', 'products/product-category-1/sub-category-1/product-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1125', 'products/product-category-1/sub-category-1/product-5');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1126', 'products/product-category-1/sub-category-1/product-6');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1127', 'products/product-category-1/sub-category-1/product-7');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1128', 'products/product-category-1/sub-category-1/product-8');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1129', 'products/product-category-1/sub-category-1/product-9');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1130', 'products/product-category-1/sub-category-1/product-10');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1131', 'products/product-category-1/sub-category-2/product-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1132', 'products/product-category-1/sub-category-2/product-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1133', 'products/product-category-1/sub-category-2/product-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1134', 'products/product-category-1/sub-category-2/product-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1135', 'products/product-category-1/sub-category-2/product-5');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1136', 'products/product-category-1/sub-category-3/product-1');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1137', 'products/product-category-1/sub-category-3/product-2');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1138', 'products/product-category-1/sub-category-3/product-3');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1139', 'products/product-category-1/sub-category-3/product-4');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1150', 'manage/page/prodrafts');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1149', 'manage/access/permissions/tracy-debugger');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1148', 'manage/access/permissions/user-admin-webmaster');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1147', 'manage/access/permissions/user-admin-all');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1143', 'manage/access/permissions/page-edit-images');
INSERT INTO `pages_paths` (`pages_id`, `path`) VALUES('1142', 'manage/access/permissions/page-edit-created');

DROP TABLE IF EXISTS `pages_procache`;
CREATE TABLE `pages_procache` (
  `path` varchar(500) CHARACTER SET ascii NOT NULL,
  `pages_id` int(10) unsigned NOT NULL,
  `templates_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`path`),
  KEY `created` (`created`,`templates_id`),
  KEY `pages_id` (`pages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `process_changelog`;
CREATE TABLE `process_changelog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(128) DEFAULT NULL,
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(10) unsigned NOT NULL DEFAULT '0',
  `operation` varchar(128) NOT NULL,
  `data` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `process_forgot_password`;
CREATE TABLE `process_forgot_password` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `token` char(32) NOT NULL,
  `ts` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `token` (`token`),
  KEY `ts` (`ts`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;


DROP TABLE IF EXISTS `prodrafts`;
CREATE TABLE `prodrafts` (
  `pages_id` int(10) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_users_id` int(10) unsigned NOT NULL,
  `modified_users_id` int(10) unsigned NOT NULL,
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(44) DEFAULT NULL,
  `data` longtext,
  `meta` text,
  PRIMARY KEY (`pages_id`,`version`),
  UNIQUE KEY `name` (`name`),
  KEY `created` (`created`),
  KEY `modified` (`modified`),
  KEY `created_users_id` (`created_users_id`),
  KEY `modified_users_id` (`modified_users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES('admin', '1', '1488825293');

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` char(32) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `ua` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `pages_id` (`pages_id`),
  KEY `user_id` (`user_id`),
  KEY `ts` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sessions` (`id`, `user_id`, `pages_id`, `data`, `ts`, `ip`, `ua`) VALUES('8b7be58c88f600e1ae448fe1ed078f48', '40', '27', 'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1488825284;}}', '2017-03-06 13:34:44', '0', '');
INSERT INTO `sessions` (`id`, `user_id`, `pages_id`, `data`, `ts`, `ip`, `ua`) VALUES('8d2b42f1057616f530c5ff3cfcdb80e8', '41', '1033', 'Session|a:13:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1488836331;}s:15:\"ProcessPageView\";a:1:{s:18:\"loginRequestPageID\";i:2;}s:12:\"ProcessLogin\";a:1:{s:17:\"beforeLoginChecks\";i:1;}s:5:\"_user\";a:4:{s:2:\"id\";i:41;s:2:\"ts\";i:1488836331;s:9:\"challenge\";s:32:\"BIXrw.AUL2rwprg8veeWo37UbOSVrQ2c\";s:11:\"fingerprint\";s:32:\"9bc134e93ca434cc48bb120cdc5e1606\";}s:27:\"ProcessWireUpgrade_branches\";a:6:{s:3:\"dev\";a:5:{s:4:\"name\";s:3:\"dev\";s:5:\"title\";s:3:\"Dev\";s:6:\"zipURL\";s:58:\"https://github.com/processwire/processwire/archive/dev.zip\";s:7:\"version\";s:6:\"3.0.54\";s:10:\"versionURL\";s:87:\"https://raw.githubusercontent.com/processwire/processwire/dev/wire/core/ProcessWire.php\";}s:6:\"master\";a:5:{s:4:\"name\";s:6:\"master\";s:5:\"title\";s:6:\"Master\";s:6:\"zipURL\";s:61:\"https://github.com/processwire/processwire/archive/master.zip\";s:7:\"version\";s:6:\"3.0.42\";s:10:\"versionURL\";s:90:\"https://raw.githubusercontent.com/processwire/processwire/master/wire/core/ProcessWire.php\";}s:13:\"legacy-master\";a:5:{s:4:\"name\";s:13:\"legacy-master\";s:5:\"title\";s:13:\"Legacy/Master\";s:6:\"zipURL\";s:68:\"https://github.com/processwire/processwire-legacy/archive/master.zip\";s:7:\"version\";s:6:\"2.8.35\";s:10:\"versionURL\";s:97:\"https://raw.githubusercontent.com/processwire/processwire-legacy/master/wire/core/ProcessWire.php\";}s:8:\"prev-dev\";a:5:{s:4:\"name\";s:8:\"prev-dev\";s:5:\"title\";s:8:\"Prev/Dev\";s:6:\"zipURL\";s:63:\"https://github.com/ryancramerdesign/ProcessWire/archive/dev.zip\";s:7:\"version\";s:5:\"2.7.3\";s:10:\"versionURL\";s:92:\"https://raw.githubusercontent.com/ryancramerdesign/ProcessWire/dev/wire/core/ProcessWire.php\";}s:10:\"prev-devns\";a:5:{s:4:\"name\";s:10:\"prev-devns\";s:5:\"title\";s:10:\"Prev/Devns\";s:6:\"zipURL\";s:65:\"https://github.com/ryancramerdesign/ProcessWire/archive/devns.zip\";s:7:\"version\";s:6:\"3.0.33\";s:10:\"versionURL\";s:94:\"https://raw.githubusercontent.com/ryancramerdesign/ProcessWire/devns/wire/core/ProcessWire.php\";}s:11:\"prev-master\";a:5:{s:4:\"name\";s:11:\"prev-master\";s:5:\"title\";s:11:\"Prev/Master\";s:6:\"zipURL\";s:66:\"https://github.com/ryancramerdesign/ProcessWire/archive/master.zip\";s:7:\"version\";s:5:\"2.7.3\";s:10:\"versionURL\";s:95:\"https://raw.githubusercontent.com/ryancramerdesign/ProcessWire/master/wire/core/ProcessWire.php\";}}s:5:\"hidpi\";b:0;s:5:\"touch\";b:0;s:11:\"SessionCSRF\";a:2:{s:4:\"name\";s:25:\"TOKEN857698256X1488825298\";s:25:\"TOKEN857698256X1488825298\";s:32:\"l5OHpn1ApPKM7S4vQT5iMWZHrs4QnEpt\";}s:14:\"ProcessPageAdd\";a:2:{s:3:\"nav\";a:6:{s:3:\"url\";s:17:\"/manage/page/add/\";s:5:\"label\";s:5:\"Pages\";s:4:\"icon\";s:11:\"plus-circle\";s:3:\"add\";N;s:4:\"list\";a:0:{}s:8:\"modified\";i:1488825298;}s:10:\"numAddable\";i:0;}s:15:\"ProcessPageList\";a:1:{s:12:\"enabledPages\";s:5:\"title\";}s:18:\"InputfieldSelector\";a:1:{s:18:\"id21_defaultFilter\";a:17:{s:5:\"valid\";b:1;s:9:\"initValue\";s:0:\"\";s:23:\"allowSystemCustomFields\";b:0;s:23:\"allowSystemNativeFields\";b:1;s:20:\"allowSystemTemplates\";b:0;s:17:\"allowSubselectors\";b:1;s:14:\"allowSubfields\";b:1;s:19:\"allowSubfieldGroups\";b:1;s:14:\"allowModifiers\";b:1;s:14:\"allowAddRemove\";b:1;s:15:\"fieldsWhitelist\";N;s:10:\"dateFormat\";s:5:\"Y-m-d\";s:15:\"datePlaceholder\";s:10:\"yyyy-mm-dd\";s:10:\"timeFormat\";s:3:\"H:i\";s:15:\"timePlaceholder\";s:5:\"hh:mm\";s:14:\"previewColumns\";a:0:{}s:11:\"limitFields\";a:0:{}}}s:18:\"ModulesUninstalled\";a:25:{i:0;s:20:\"CommentFilterAkismet\";i:1;s:17:\"FieldtypeComments\";i:2;s:26:\"FieldtypePageTitleLanguage\";i:3;s:21:\"FieldtypeTextLanguage\";i:4;s:25:\"FieldtypeTextareaLanguage\";i:5;s:23:\"ImageSizerEngineIMagick\";i:6;s:23:\"InputfieldCommentsAdmin\";i:7;s:15:\"LanguageSupport\";i:8;s:21:\"LanguageSupportFields\";i:9;s:24:\"LanguageSupportPageNames\";i:10;s:12:\"LanguageTabs\";i:11;s:16:\"MarkupPageFields\";i:12;s:9:\"MarkupRSS\";i:13;s:26:\"ModulesManagerNotification\";i:14;s:13:\"PageFrontEdit\";i:15;s:19:\"ProcessChangelogRSS\";i:16;s:22:\"ProcessCommentsManager\";i:17;s:15:\"ProcessLanguage\";i:18;s:25:\"ProcessLanguageTranslator\";i:19;s:26:\"TextformatterMarkdownExtra\";i:20;s:22:\"TextformatterNewlineBR\";i:21;s:22:\"TextformatterNewlineUL\";i:22;s:22:\"TextformatterPstripper\";i:23;s:24:\"TextformatterSmartypants\";i:24;s:22:\"TextformatterStripTags\";}s:21:\"ProcessSessionDB_mins\";i:5;}', '2017-03-06 16:39:07', '0', '');

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('2', 'admin', '2', '8', '0', '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1472063223,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('3', 'user', '3', '8', '0', '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('4', 'role', '4', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('5', 'permission', '5', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('1', 'home', '1', '0', '0', '{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"label\":\"Home Template\",\"tags\":\"Generic\",\"modified\":1486497486,\"ns\":\"\\\\\",\"roles\":[37,1043]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('43', 'form-builder', '97', '8', '0', '{\"noParents\":1,\"urlSegments\":1,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1472063225,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('44', '404', '98', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"tags\":\"Generic\",\"modified\":1485808397,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('45', 'default', '99', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"label\":\"Default Template\",\"tags\":\"Generic\",\"modified\":1486413424,\"ns\":\"\\\\\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('46', 'settings', '100', '0', '0', '{\"useRoles\":1,\"editRoles\":[1043],\"noChildren\":1,\"noParents\":-1,\"slashUrls\":1,\"pageLabelField\":\"title\",\"noChangeTemplate\":1,\"label\":\"Site Settings\",\"tags\":\"Settings\",\"modified\":1439493422,\"tabContent\":\"Website Settings\",\"roles\":[1043]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('47', 'fileless', '101', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1486407850}');

# --- /WireDatabaseBackup {"numTables":48,"numCreateTables":48,"numInserts":961,"numSeconds":0}